<?php

$di->get('router')->setUriSource(Phalcon\Mvc\Router::URI_SOURCE_SERVER_REQUEST_URI);

//customer side routes
$di->get('router')->mount( new \Justashop\Frontend\FrontendRoutes() );


//administration side routes
$di->get('router')->mount( new \Justashop\Backend\BackendRoutes() );

