<?php

$loader = new Phalcon\Loader();

$justashopDir = APP_PATH . 'Justashop' . DS;

$loader->registerNamespaces([
    'Justashop' => $justashopDir . DS,
] )->register();

unset( $justashopDir );

$di = new Phalcon\DI\FactoryDefault();

\Justashop\Helpers\DI::registerAdditionalServices($di);
\Justashop\Helpers\DI::configureServices($di);



include APP_PATH . 'routes.php';
