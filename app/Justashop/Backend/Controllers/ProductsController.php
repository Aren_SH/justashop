<?php

namespace Justashop\Backend\Controllers;


use Justashop\Backend\Form\ProductsForm;
use Justashop\Model\Product;


class ProductsController extends EditController{

    protected $_template = 'controlPanel';

    public function onConstruct(){
        $this->_model     = new Product();
        $this->_modelForm = ProductsForm::Factory();
    }
} 