<?php

namespace Justashop\Backend\Controllers;


use Justashop\Backend\Form\CategoriesForm;
use Justashop\Model\Category;


class CategoriesController extends EditController{

    protected $_template = 'controlPanel';

    public function onConstruct(){
        $this->_model     = new Category();
        $this->_modelForm = CategoriesForm::Factory();
    }
} 