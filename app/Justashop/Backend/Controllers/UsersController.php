<?php

namespace Justashop\Backend\Controllers;


use Justashop\Backend\Form\UserForm;
use Justashop\Components\Account\Model\User;

class UsersController extends EditController {

    protected $_template = 'controlPanel';

    protected $_canAdd = false;

    /**
     * @var array
     */
    protected $_columns  = [ 'id' => 'id', 'login' => 'login', 'email' => 'email'];

    public function onConstruct(){
        $this->_model     = new User();
        $this->_modelForm = UserForm::Factory();
    }
} 