<?php

namespace Justashop\Backend\Controllers;


use Justashop\Components\Account;
use Justashop\Helpers\DI;
use Justashop\Mvc\TemplateController;

abstract class BaseController extends TemplateController {

    /** @var string */
    protected $_template = 'controlPanel';

    /** @var Account\Model\User */
    protected $_user     = null;

   public function beforeExecuteRoute($dispatcher){
       /** @var Account $accountManager */
       $accountManager = $this->getDI()->getShared(DI::ACCOUNT);

       $user = $accountManager->getCurrentUser();
       if (!$user){
           $url = $this->url->get([
                   'for'    => 'backend.auth',
                   'action' => 'login',
               ]);

           $this->response->redirect( $url );
           return;
       }

       $this->_user = $user;
   }

    public function initialize(){
        parent::initialize();
        $this->view->setVar('user', $this->_user);
    }
} 