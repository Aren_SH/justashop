<?php

namespace Justashop\Backend\Controllers;


use Justashop\Backend\Form\PageForm;
use Justashop\Model\Page;


class PagesController extends EditController{

    protected $_template = 'controlPanel';

    public function onConstruct(){
        $this->_model     = new Page();
        $this->_modelForm = PageForm::Factory();
    }
} 