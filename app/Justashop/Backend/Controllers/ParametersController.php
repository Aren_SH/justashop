<?php

namespace Justashop\Backend\Controllers;


use Justashop\Backend\Form\ParameterForm;
use Justashop\Backend\Form\PaymentTypeForm;
use Justashop\Components\Parameters\Model\Parameter;
use Justashop\Model\Payment;


class ParametersController extends EditController{

    /**
     * @var array
     */
    protected $_columns  = [ 'id' => 'id', 'key' => 'key', 'value' => 'value'];

    public function onConstruct(){
        $this->_model     = new Parameter();
        $this->_modelForm = ParameterForm::Factory();
    }
} 