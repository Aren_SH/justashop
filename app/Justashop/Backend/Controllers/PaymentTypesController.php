<?php

namespace Justashop\Backend\Controllers;


use Justashop\Backend\Form\PaymentTypeForm;
use Justashop\Model\Payment;


class PaymentTypesController extends EditController{

    protected $_template = 'controlPanel';

    public function onConstruct(){
        $this->_model     = new Payment\Type();
        $this->_modelForm = PaymentTypeForm::Factory();
    }
} 