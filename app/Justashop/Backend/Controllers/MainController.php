<?php

namespace Justashop\Backend\Controllers;

use Justashop\Model\Order;
use Justashop\Model\Product;

class MainController extends BaseController {

    public function indexAction(){

        $orders   = Order::findWithRelations(['limit' => 10, 'order' => ' "createdAt" DESC'])->toArray();
        $products = Product::findWithRelations(['limit' => 10, 'order' => ' "createdAt" DESC'])->toArray();

        $this->view->setVar('orders',   $orders);
        $this->view->setVar('products', $products);


    }
} 