<?php

namespace Justashop\Backend\Controllers;


use Justashop\Backend\Form\OrderForm;
use Justashop\Model\Order;

class OrdersController extends EditController {

    protected $_template = 'controlPanel';

    protected $_canAdd = false;

    /**
     * @var array
     */
    protected $_columns  = [ 'id' => 'id', 'user_name' => 'user_name', 'amount' => 'amount'];

    public function onConstruct(){
        $this->_model     = new Order();
        $this->_modelForm = OrderForm::Factory();
    }
} 