<?php

namespace Justashop\Backend\Controllers;


use Justashop\Components\Former\FormRenderer;
use Justashop\Components\BreadCrumbs;
use Justashop\Helpers\DI;
use Justashop\Components\Messages\Translator;
use Justashop\Helpers\Text;

class EditController extends BaseController {

    /**
     * @var string
     */
    protected $_viewPath = 'edit';

    /**
     * @var \Justashop\Mvc\Model
     */
    protected $_model;

    /**
     * @var \Justashop\Components\Former\Model\ModelFormInterface
     */
    protected $_modelForm;

    /**
     * @var array
     */
    protected $_errorMessages = [];

    /**
     * @var array
     */
    protected $_columns  = [ 'id' => 'id', 'title' => 'title'];

    /**
     * @var int
     */
    protected $_pageSize = 20;

    /** @var bool */
    protected $_canAdd  = true;

    /** @var bool */
    protected $_canEdit = true;

    /** @var bool */
    protected $_canDelete = true;


    public function initialize(){
        parent::initialize();
        $this->view->setVar('controllerName', $this->_getControllerName());

    }


    public function indexAction(){
        $builder    = $this->_getBuilder();
        $pageNumber = $this->request->get('page', 'int', 1);

        /** @var \Phalcon\Paginator\AdapterInterface $paginator */
        $paginator = new \Phalcon\Paginator\Adapter\QueryBuilder([
            'builder' => $builder,
            'limit'   => $this->_pageSize,
            'page'    => $pageNumber
        ]);


        $page = $paginator->getPaginate();

        $this->view->pick('edit/index');
        $this->view->setVar( 'page',    $page );
        $this->view->setVar( 'columns', $this->_columns );

        $this->view->setVar( 'canAdd',    $this->_canAdd );
        $this->view->setVar( 'canEdit',   $this->_canEdit );
        $this->view->setVar( 'canDelete', $this->_canDelete );

        $this->_setCrumbs( 'list' );
    }


    public function addAction(){
        $this->_modelForm->setModel( $this->_model );

        $model = $this->_model;

        if( $this->request->isPost() && $this->_save( $model ) ) {
            $this->_redirectToIndexPage();
            return;
        }

        $this->view->pick('edit/edit');

        /** @var FormRenderer $form */
        $form = $this->getDI()->getShared('formBuilder')->renderer( $this->_modelForm );
        $this->view->setVar( 'form', $form );

        $this->view->setVar( 'errorMessages', $this->_errorMessages );

        $this->_setCrumbs( 'edit' );
    }


    public function editAction(){
        $id = (int) $this->dispatcher->getParam( 'id' );
        if( !$id ){
            $this->_redirectToIndexPage();
            return;
        }

        $model = $this->_model->findFirst( $id );
        $this->_modelForm->setModel( $model );


        if( $this->request->isPost() && $this->_save( $model ) ) {
            if( $this->request->getPost('submit') != 'apply'){
                $this->_redirectToIndexPage();
                return;
            }
        }

        $this->view->pick('edit/edit');

        $form = $this->getDI()->getShared('formBuilder')->renderer( $this->_modelForm );
        $this->view->setVar( 'form', $form );

        $this->view->setVar( 'errorMessages', $this->_errorMessages );

        $this->_setCrumbs( 'edit' );

    }


    public function deleteAction(){
        $id = (int) $this->dispatcher->getParam( 'id' );
        if( !$id ){
            $this->_redirectToIndexPage();
            return;
        }

        $model = $this->_model->findFirst( $id );
        if( $model ){
            $model->delete();
        }

        $this->_redirectToIndexPage();
        return;
    }


    /**
     * @param \Phalcon\Mvc\Model $model
     *
     * @return bool
     */
    protected function _save( $model ){
        $saved = false;
        $this->_modelForm->handleRequest();
        if( $this->_modelForm->validate() ) {
            $saved = $this->_modelForm->save();
        }

        $this->_errorMessages = !$saved ? $model->getMessages() : [];

        return $saved;
    }


    /**
     * @return \Phalcon\Mvc\Model\Query\BuilderInterface
     */
    protected function _getBuilder(){
        $mManager = $this->getDI()->getShared('modelsManager');
        if( !$mManager ) return null;

        /** @var \Phalcon\Mvc\Model\Manager $mManager */
        return $mManager->createBuilder()
            ->from( get_class( $this->_model ) )
            ->columns( join( ',' , array_keys( $this->_columns ) ) )
            ->orderBy( 'id DESC' );
    }


    /**
     * Redirect to current Controllers index page
     */
    protected function _redirectToIndexPage(){
        $this->view->disable();
        $url = $this->url->get([
            'for'        => 'backend.indexPage',
            'controller' => \Phalcon\Text::uncamelize( $this->_getControllerName() ),
        ]);
        $this->response->redirect( $url, true );
    }


    /**
     * @return string
     */
    protected function _getControllerName(){
        $name = get_class( $this );
        $nameParts = explode('\\', $name );
        $name = str_replace( 'Controller', '', array_pop( $nameParts ) );

        return $name;
    }

    protected function _setCrumbs( $page ){
        /** @var BreadCrumbs $crumbs */
        $crumbs = $this->getDI()->get(DI::BREADCRUMBS);

        /** @var Translator $t */
        $t = $this->getDI()->getShared( DI::TRANSLATOR );

        $crumbs->add( $t->_('controlPanel.menu', 'main'), $this->url->get(['for' => 'backend.mainPage',]));

        $messagesPath = 'objects.' . lcfirst($this->_getControllerName());
        if( $page != 'list' ){
            $indexUrl = $this->url->get([
                'for'        => 'backend.indexPage',
                'controller' => Text::controllerAlias( $this->_getControllerName() ),
            ]);

            $crumbs->add( $t->_( $messagesPath, 'page-title' ), $indexUrl );
        }

        $crumbs->add( $t->_($messagesPath, 'page-title-' . $page), null );

        $this->view->setVar( 'breadCrumbs', $crumbs);

    }
} 