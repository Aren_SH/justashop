<?php

namespace Justashop\Backend\Controllers;


use Justashop\Backend\Form\DeliveryTypeForm;
use Justashop\Model\Delivery;


class DeliveryTypesController extends EditController{

    protected $_template = 'controlPanel';

    public function onConstruct(){
        $this->_model     = new Delivery\Type();
        $this->_modelForm = DeliveryTypeForm::Factory();
    }
} 