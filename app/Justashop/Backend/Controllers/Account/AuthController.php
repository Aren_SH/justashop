<?php

namespace Justashop\Backend\Controllers\Account;

use Justashop\Components\Account;
use Justashop\Components\Messages\Translator;
use Justashop\Helpers\DI;
use Justashop\Mvc\TemplateController;

class AuthController extends TemplateController {

    protected $_template = 'controlPanel';

    const ADMIN_ROLE = 1;


    public function loginAction(){
        /** @var Translator $t */
        $t              = $this->getDI()->getShared(DI::TRANSLATOR);
        /** @var Account $accountManager */
        $accountManager = $this->getDI()->getShared(DI::ACCOUNT);
        $mainPageUrl    = $this->url->get(['for' => 'backend.mainPage']);

        $user = $accountManager->getCurrentUser();

        if( $user ){
            $this->response->redirect($mainPageUrl);
        }

        if( $this->request->isPost() ){
            $login = $this->request->getPost('login');
            $pass  = $this->request->getPost('password');
            $user = $accountManager->getUserByCredentials( $login, $pass );
            if( $user && $user->hasRole(self::ADMIN_ROLE) ){
                $accountManager->logIn($user);
                $this->response->redirect($mainPageUrl);
            } else {
                $this->view->setVar('error', $t->_('errors.user', 'Invalid login or password' ));
            }
        }
    }

    public function logoutAction(){
        $this->view->disable();
        /** @var Account $accountManager */
        $accountManager = $this->getDI()->getShared(DI::ACCOUNT);
        $mainPageUrl    = $this->url->get(['for' => 'backend.mainPage']);

        $user = $accountManager->getCurrentUser();
        if($user){
            $accountManager->logOut($user);
        }

        $this->response->redirect($mainPageUrl);

    }
} 