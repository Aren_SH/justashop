<?php

namespace Justashop\Backend\Controllers\Internal;


use Justashop\Components\Image\Manager;
use Justashop\Components\Image\Model\Image;
use Phalcon\Mvc\Controller;

class UploadImageController extends Controller{


    public function indexAction(){
        /** @var Manager $imageManager */
        $imageManager = $this->getDI()->getShared('imageManager');

        $profile = $this->dispatcher->getParam('profile');
        if( !$profile) $profile = 'default';

        $result = $imageManager->uploadFromRequest($profile);

        $this->view->disable();
        $this->response->setJsonContent( $this->_prepareImages($result, $profile) );

        return $this->response;
    }

    /**
     * @param Image[] $images
     */
    private function _prepareImages($images, $profile){
        if(!$images ) return [];

        $result = [];
        foreach($images as $image){
            $result[] = $this->_getImageData($image, $profile);
        }

        return $result;
    }

    /**
     * @param Image $image
     */
    private function _getImageData($image, $profile){
        /** @var Manager $imageManager */
        $imageManager = $this->getDI()->getShared('imageManager');

        return [
            'id'    => $image->id,
            'hash'  => $image->hash,
            'file'  => $image->file,
            'url'   => $imageManager->getFileUrl( $image->file, $profile, 'm'),
        ];
    }
} 