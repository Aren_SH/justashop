<?php
namespace Justashop\Backend\Form;


use Justashop\Helpers\DI;

use Justashop\Helpers\Text;
use Justashop\Model\Page;
use \Phalcon\Validation;
use \Phalcon\Validation\Validator;

/**
 * Class PageForm
 *
 * @property Page _model
 * @package  \Justashop\Backend\Form
 */
class PageForm extends BaseForm {


    protected $_withMetaTags = true;

    /**
     * @return Validation
     */
    public function _initValidation(){
        $validator = new Validation();

        $t = $this->getDI()->get(DI::TRANSLATOR);

        $validator->add('title', new Validator\PresenceOf([
            'message' => $t->_('objects.page.validation', 'title required')
        ]));
        $validator->add('title', new Validator\StringLength([
            'max'            => 120,
            'messageMaximum' => $t->_('objects.page.validation', 'title too long')
        ]));

        $validator->add('alias', new \Justashop\Validation\Validator\Unique([
            'model'      => Page::getClass(),
            'field'      => 'alias',
            'primaryKey' => $this->_model->id,
            'message'    => $t->_('objects.pages.validation', 'alias already exists'),
        ]));

        $validator->add('content', new Validator\PresenceOf([
            'message' => $t->_('objects.pages.validation', 'content required')
        ]));


        return $validator;
    }


    /**
     * @return mixed[]
     */
    protected function _initFields(){
        $t = $this->getDI()->get(DI::TRANSLATOR);

        /** @var \Justashop\Components\Former\Builder $former */
        $former = $this->getDI()->getShared('formBuilder');

        $this->addField('title', $former
                ->field('input', 'title')
                ->setLabel($t->_('objects.pages.fields', 'title'))
                ->setErrorText($this->getMessageText('title'))
                ->setHelpText($t->_('objects.pages.help', 'title', ''))
                ->setValue($this->_model->title)
                ->setRequired(true)
            )
            ->addField('alias', $former
                    ->field('input', 'alias')
                    ->setLabel($t->_('objects.pages.fields', 'alias'))
                    ->setErrorText($this->getMessageText('alias'))
                    ->setHelpText($t->_('objects.pages.help', 'alias', ''))
                    ->setValue($this->_model->alias)
            )
            ->addField('content', $former
                    ->field('textarea', 'content')
                    ->setLabel($t->_('objects.pages.fields', 'content'))
                    ->setErrorText($this->getMessageText('value'))
                    ->setHelpText($t->_('objects.pages.help', 'content', ''))
                    ->setValue($this->_model->content)
                    ->addClass('editable')
            );
    }


    protected function _prepareRequestData( $data ){

        if( empty( $data['alias'] ) && !empty($data['title']) ){
            $data['alias'] = Text::slug_format($data['title']);
        }

        return $data;
    }
}