<?php
namespace Justashop\Backend\Form;


use Justashop\Helpers\DI;
use \Justashop\Model\Category;

use Justashop\Model\Product;
use Phalcon\Db\Column;
use Phalcon\Mvc\Model\Query;
use \Phalcon\Validation;
use \Phalcon\Validation\Validator;


class ProductsForm extends BaseForm{


    protected $_requestImages = [];

    protected $_withMetaTags = true;

    /**
     * @return Validation
     */
    public function _initValidation(){
        $validator = new Validation();

        $validator->add('title', new Validator\StringLength([
            'max'            => 500,
            'messageMaximum' => 'Product title too long'
        ]));


        return $validator;
    }


    /**
     * @return mixed[]
     */
    protected function _initFields(){
        $t = $this->_model->getDI()->get(DI::TRANSLATOR);

        /** @var \Justashop\Components\Former\Builder $former */
        $former = $this->_model->getDI()->getShared('formBuilder');

        $this->addField('title', $former
                    ->field('input', 'title')
                    ->setLabel($t->_('objects.products.fields', 'title'))
                    ->setErrorText($this->getMessageText('title'))
                    ->setHelpText('Title is very important')
                    ->setValue($this->_model->title)
                    ->setRequired(true)
            )
            ->addField('sku', $former
                    ->field('input', 'sku')
                    ->setLabel($t->_('objects.products.fields', 'sku'))
                    ->setErrorText($this->getMessageText('sku'))
                    ->setHelpText('sku is very important too')
                    ->setValue($this->_model->sku)
                    ->setRequired(true)
            )
            ->addField('price', $former
                    ->field('input', 'price')
                    ->setLabel($t->_('objects.products.fields', 'price'))
                    ->setErrorText($this->getMessageText('price'))
                    ->setValue($this->_model->price)
                    ->setRequired(true)
            )
            ->addField('category_id', $former
                    ->field('select', 'category_id')
                    ->setLabel($t->_('objects.products.fields', 'category'))
                    ->setErrorText($this->getMessageText('category_id'))
                    ->setSelected($this->_model->category_id)
                    ->setOptions($this->_getCategoryOptions())
                    ->setRequired(true)
            )
            ->addField('description', $former
                    ->field('textarea', 'description')
                    ->setLabel($t->_('objects.products.fields', 'description'))
                    ->setErrorText($this->getMessageText('description'))
                    ->setValue($this->_model->description)->setRequired(false)
                    ->addClass('editable')
            )
            ->addField('images', $former
                    ->field('image', 'images')
                    ->setLabel($t->_('objects.products.fields', 'images'))
                    ->setErrorText($this->getMessageText('images'))
                    ->setSelected($this->_getSelectedImages())
                    ->setRequired(false)
            );
    }


    /**
     * @return string[]
     */
    protected function _getCategoryOptions(){
        $categories = Category::find();
        $result     = [];
        foreach($categories as $category){
            $result[$category->id] = $category->title;
        }

        return $result;
    }


    protected function _getSelectedImages(){
        if($this->request->isPost()){
            return $this->_requestImages;
        }

        $images = [];
        $id     = $this->_model ? $this->_model->id : null;
        if(!$id){
            return $images;
        }

        /** @var Product\Image[] $dbImages */
        $dbImages = Product\Image::findWithRelations([
            'conditions' => 'product_id = :id:',
            'bind'       => ['id' => $id],
            'bindTypes'  => ['id' => Column::BIND_PARAM_INT]
        ]);

        foreach($dbImages as $image){
            $images[] = [
                'id'          => $image->image_id,
                'hash'        => $image->image->hash,
                'title'       => $image->title,
                'description' => $image->description,
                'is_main'     => $image->is_main,
                'url'         => $image->getUrl('m')
            ];
        }

        return $images;
    }


    protected function _postSave(){
        $this->_saveImages();
    }


    protected function _prepareRequestData($data){
        if($data && isset($data['images']))
            unset($data['images']);

        return $data;
    }


    protected function _handleRequestData(){
        $data = parent::_handleRequestData();

        $this->_handleRequestImages();

        return $data;
    }


    protected function _handleRequestImages(){
        $data      = $this->request->getPost();
        $rImages   = array_key_exists('images', $data) ? $data['images'] : [];
        $mainImage = $this->request->getPost('images_main', 'int', 0);

        foreach($rImages as $rImage){
            $id = (int)filter_var($rImage['id'], FILTER_SANITIZE_NUMBER_INT);

            $this->_requestImages[$id] = [
                'id'          => (int)filter_var($rImage['id'], FILTER_SANITIZE_NUMBER_INT),
                'title'       => filter_var($rImage['title'], FILTER_SANITIZE_SPECIAL_CHARS),
                'description' => filter_var($rImage['description'], FILTER_SANITIZE_SPECIAL_CHARS),
                'url'         => filter_var($rImage['url'], FILTER_SANITIZE_URL),
                'hash'        => $rImage['hash'],
                'is_main'     => $rImage['id'] == $mainImage,
            ];
        }
    }


    protected function _saveImages(){
        $existedImages = $this->_getExistedImages();
        $existedIds    = array_keys($existedImages);

        /** @var Product\Image[] $ImagesToSave */
        $ImagesToSave = [];
        $requestIds   = [];
        foreach($this->_requestImages as $img){
            $requestIds[] = $img['id'];
            if(!empty($existedImages[$img['id']])){
                $image = $existedImages[$img['id']];
            }
            else{
                $image = new Product\Image();
            }

            if($image->id && !$this->_imageHasChanges($image, $img)){
                continue;
            }

            $image->product_id  = $this->_model->id;
            $image->image_id    = $img['id'];
            $image->title       = $img['title'];
            $image->description = $img['description'];
            $image->is_main     = $img['is_main'];

            $ImagesToSave[] = $image;
        }


        $this->_deleteImages(array_diff($existedIds, $requestIds));

        foreach($ImagesToSave as $image){
            $image->save();
        }
    }


    protected function _getExistedImages(){
        $result = [];

        if(!$this->_model->id)
            return $result;

        $images = Product\Image::findByProductId($this->_model->id);

        foreach($images as $image){
            $result[$image->image_id] = $image;
        }

        return $result;
    }


    /**
     * @param  Product\Image $image
     * @param  array         $data
     *
     * @return bool
     */
    protected function _imageHasChanges($image, $data){
        if(!$image->product_id || !$image->image_id)
            return true;

        foreach(['description', 'title', 'is_main'] as $field){
            if($image->{$field} != $data[$field])
                return true;
        }

        return false;
    }


    /**
     * @param  int[] $ids
     * @return bool
     */
    protected function _deleteImages($ids){
        if(!$ids)
            return false;

        $ids = array_filter($ids, function ($n){
            return $n && is_numeric($n);
        });
        if(!$ids)
            return false;

        $query = sprintf('DELETE FROM %s WHERE image_id IN (%s) AND product_id = %s',
            Product\Image::getClass(),
            join(',', $ids),
            $this->_model->id
        );

        $query = new Query($query);
        $query->setDI($this->getDI());

        return $query->execute();
    }
}