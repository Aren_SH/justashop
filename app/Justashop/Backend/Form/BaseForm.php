<?php
namespace Justashop\Backend\Form;

use Justashop\Components\Former\Form;

use Justashop\Helpers\DI;
use Justashop\Model\MetaTag;
use Phalcon\Db\Column;
use \Phalcon\Mvc\Model;
use Phalcon\Mvc\ModelInterface;
use \Phalcon\Validation;
use \Phalcon\Validation\Validator;


abstract class BaseForm extends Form{

    /**
     * @var Model
     */
    protected $_model;

    /** @var MetaTag */
    protected $_metaTagModel;

    protected $_withMetaTags = false;



    protected function _init(){
        $t = $this->getDI()->get(DI::TRANSLATOR);
        $this->collection('default')->setTitle($t->_('controlPanel.menu', 'default'));
    }


    /**
     * @param  ModelInterface $model
     * @return void
     */
    public function setModel($model){
        $this->_model = $model;

        if($this->_withMetaTags){
            $this->_loadMetaTag();
        }
    }


    public function getModel(){
        return $this->_model;
    }


    /**
     * @return \mixed[]
     */
    public function getFilters(){
    }


    public function _initAdditionalFields(){
        if( $this->_withMetaTags ){
            $this->_initMetaTagFields();
        }
    }

    /**
     * @return bool
     */
    public function save(){
        $this->_preSave();

        $result = $this->_model->save();

        if($result){
            $this->_postSave();
        }
        else{
            $this->_messages->appendMessages($this->_model->getMessages());
        }

        return $result;
    }


    protected function _preSave(){
        if( $this->_withMetaTags && $this->_saveMetaTagModel()){
            $this->_model->meta_tag_id = $this->_metaTagModel->id;
        }
    }


    protected function _postSave(){

    }


    protected function _initMetaTagFields(){
        $t = $this->_model->getDI()->get(DI::TRANSLATOR);

        /** @var \Justashop\Components\Former\Builder $former */
        $former = $this->_model->getDI()->getShared('formBuilder');

        $this->collection('metaTags')
            ->addField('metaTagId', $former->field('hidden', 'metaTag[id]')
                    ->setValue($this->_model->meta_tag_id)
            )
            ->addField('metaTagTitle',
                $former->field('input', 'metaTag[title]')
                    ->setLabel($t->_('objects.metaTags.fields', 'title'))
                    ->setErrorText($this->getMessageText('metaTagTitle'))
                    ->setHelpText($t->_('objects.metaTags.help', 'title'))
                    ->setValue($this->_metaTagModel->title)
            )
            ->addField('metaTagDescription',
                $former->field('input', 'metaTag[description]')
                    ->setLabel($t->_('objects.metaTags.fields', 'description'))
                    ->setErrorText($this->getMessageText('metaTagDescription'))
                    ->setHelpText($t->_('objects.metaTags.help', 'description'))
                    ->setValue($this->_metaTagModel->description)
            )
            ->addField('metaTagKeywords',
                $former->field('input', 'metaTag[keywords]')
                    ->setLabel($t->_('objects.metaTags.fields', 'keywords'))
                    ->setErrorText($this->getMessageText('metaTagKeywords'))
                    ->setHelpText($t->_('objects.metaTags.help', 'keywords'))
                    ->setValue($this->_metaTagModel->keywords)
            )
            ->addField('metaTagRobots',
                $former->field('input', 'metaTag[robots]')
                    ->setLabel($t->_('objects.metaTags.fields', 'robots'))
                    ->setErrorText($this->getMessageText('metaTagRobots'))
                    ->setHelpText($t->_('objects.metaTags.help', 'robots'))
                    ->setValue($this->_metaTagModel->robots)
            )
            ->setTitle($t->_('controlPanel.menu', 'metaTags'));
    }


    protected function _loadMetaTag(){
        if(!$this->_model->meta_tag_id){
            $this->_metaTagModel = new MetaTag();
        }
        else{
            $this->_metaTagModel = MetaTag::findById($this->_model->meta_tag_id);
        }
    }


    /**
     * @return bool
     */
    protected function _saveMetaTagModel(){
        $hasData = $this->_metaTagModel->title  || $this->_metaTagModel->description
                || $this->_metaTagModel->robots || $this->_metaTagModel->keywords;

        if( !$this->_metaTagModel->id && !$hasData ) return false;

        return $this->_metaTagModel->save();
    }


    /**
     * @param  string $field
     * @param  \mixed $default
     * @return \mixed
     */
    public function getRequestData($field = null, $default = null){

        if(!$field){
            return $this->_requestData;
        }

        if($this->_requestData && array_key_exists($field, $this->_requestData)){
            return $field;
        }

        return $default;
    }


    protected function _handleRequestData(){
        $data = parent::_handleRequestData();

        $this->_assignRequestDataToModel($data);

        if($this->_withMetaTags){
            $this->_assignDataToMetaTagModel($data);
        }

        return $data;
    }


    protected function _assignRequestDataToModel($data){
        $data = array_filter($data);
        $meta = $this->_model->getModelsMetaData();
        $map  = $meta->getDataTypes($this->_model);

        $result = [];
        foreach($map as $col => $type){
            if(!array_key_exists($col, $data)){
                $result[$col] = $this->_model->{$col};
                continue;
            };

            switch($type){
                case Column::TYPE_DECIMAL:
                case Column::TYPE_INTEGER:
                    $result[$col] = (int)$data[$col];
                    break;

                case Column::TYPE_DOUBLE:
                case Column::TYPE_FLOAT:
                    $result[$col] = floatval($data[$col]);
                    break;

                case Column::TYPE_BOOLEAN:
                    $result[$col] = (bool)$data[$col];
                    break;
                default:
                    $result[$col] = $data[$col];
            }
        }

        $this->_model->assign($result);
    }


    protected function _assignDataToMetaTagModel($data){

        $metaTagData = !empty($data['metaTag']) ? $data['metaTag'] : null;

        if(!$metaTagData){
            return;
        }

        $this->_metaTagModel->assign($metaTagData);
    }
}