<?php


namespace Justashop\Backend\Form;


use Justashop\Components\Account\Model\Role;
use Justashop\Components\Account\Model\User;
use Justashop\Components\Account\Model\UserRole;
use Justashop\Components\Account;
use Justashop\Helpers\DI;
use Phalcon\Db\Column;
use Phalcon\Mvc\Model\Query\Builder;
use Phalcon\Mvc\Model\Query;
use Phalcon\Validation;
use Phalcon\Validation\Validator;


/**
 * Class UserForm
 *
 * @property User $_model
 *
 * @package Justashop\Backend\Form
 */
class UserForm extends BaseForm{

    /**
     * @return Validation
     */
    public function _initValidation(){
        $t = $this->_model->getDI()->getShared(DI::TRANSLATOR);

        $validator = new Validation();

        $validator->add('email', new Validator\Email([
            'message' => $t->_('objects.users.validation', 'email is not valid'),
        ]));
        $validator->add('email', new \Justashop\Validation\Validator\Unique([
            'model'      => User::getClass(),
            'field'      => 'email',
            'primaryKey' => $this->_model->id,
            'message'    => $t->_('objects.users.validation', 'email already exists'),
        ]));


        $validator->add('login', new Validator\PresenceOf([
            'messageMinimum' => 'Login required',
        ]));
        $validator->add('login', new Validator\StringLength([
            'max'            => 256,
            'min'            => 5,
            'messageMaximum' => $t->_('objects.users.validation', 'login to long'),
            'messageMinimum' => $t->_('objects.users.validation', 'login to short'),
        ]));

        $validator->add('password', new Validator\StringLength([
            'max'            => 256,
            'messageMaximum' => $t->_('objects.users.validation', 'password to long'),
            'messageMinimum' => $t->_('objects.users.validation', 'password to short'),
        ]));


        return $validator;
    }


    /**
     * @return mixed[]
     */
    protected function _initFields(){
        $t = $this->_model->getDI()->getShared(DI::TRANSLATOR);

        /** @var \Justashop\Components\Former\Builder $former */
        $former = $this->_model->getDI()->getShared('formBuilder');

        $this->addField('login', $former
                    ->field('input', 'login')
                    ->setLabel($t->_('objects.users.fields', 'login'))
                    ->setErrorText($this->getMessageText('login'))
                    ->setHelpText($t->_('objects.users.help', 'login', ''))
                    ->setValue($this->_model->login)
                    ->setRequired(true)
            )
            ->addField('password', $former
                    ->field('input', 'password')
                    ->setLabel($t->_('objects.users.fields', 'password'))
                    ->setErrorText($this->getMessageText('password'))
                    ->setHelpText($t->_('objects.users.help', 'password', ''))
                    ->setValue('')
            )
            ->addField('email', $former
                    ->field('input', 'email')
                    ->setLabel($t->_('objects.users.fields', 'email'))
                    ->setErrorText($this->getMessageText('email'))
                    ->setHelpText($t->_('objects.users.help', 'email', ''))
                    ->setValue($this->_model->email)
                    ->setRequired(true)
            )
            ->addField('status', $former
                    ->field('select', 'status')
                    ->setLabel($t->_('objects.users.fields', 'status'))
                    ->setErrorText($this->getMessageText('status'))
                    ->setHelpText($t->_('objects.users.help', 'status', ''))
                    ->setSelected($this->_model->status)
                    ->setOptions($this->getStatusOptions())
                    ->setRequired(true)
            )
            ->addField('roles', $former
                    ->field('select', 'roles[]')
                    ->setLabel($t->_('objects.users.fields', 'roles'))
                    ->setErrorText($this->getMessageText('roles'))
                    ->setHelpText($t->_('objects.users.help', 'roles', ''))
                    ->setSelected($this->getSelectedRoles())
                    ->setOptions($this->getRolesOptions())
                    ->setMultiple(true)
                    ->setRequired(true)
            );
    }


    protected function getStatusOptions(){
        $t = $this->_model->getDI()->get(DI::TRANSLATOR);

        $result = [];
        foreach([User::STATUS_DISABLED, User::STATUS_ACTIVE, User::STATUS_NOT_VERIFIED] as $statusId){
            $result[$statusId] = $t->_('objects.users.statuses', 'status' . $statusId);
        }

        return $result;
    }


    protected function getSelectedRoles(){

        if($this->isRequestHandled()){
            $data = $this->getRequestData();

            return !empty($data['roles']) ? $data['roles'] : null;
        }

        if(!$this->_model->id){
            return [];
        }
        $roles = UserRole::find([
            'conditions' => 'user_id = :id:',
            'bind'       => ['id' => $this->_model->id],
            'bindTypes'  => ['id' => Column::BIND_PARAM_INT],
        ]);
        $ids   = [];
        foreach($roles as $role){
            $ids[] = $role->role_id;
        }

        return $ids;
    }


    protected function getRolesOptions(){
        $roles = Role::find();

        $result = [];
        foreach($roles as $role){
            $result[$role->id] = $role->name;
        }

        return $result;
    }

    protected function _preSave(){
        /** @var Account $account */
        $account = $this->getDI()->getShared(DI::ACCOUNT);

        $data = $this->getRequestData();
        if(!empty($data['password'])){
            $this->_model->password = $account->getHasher()->passwordHash($data['password']);
        }
    }


    protected function _postSave(){
        $data = $this->getRequestData();

        $roleIds = !empty($data['roles']) ? $data['roles'] : [];

        $roleIds = array_map('intval', array_filter($roleIds));


        $query = new Query(sprintf("DELETE FROM %s WHERE user_id = :id:", UserRole::getClass()));

        $query->setBindParams(['id' => $this->_model->id]);
        $query->setBindTypes(['id' => Column::BIND_PARAM_INT]);

        $query->setDI($this->_model->getDI());
        $query->execute();

        if(!$roleIds){
            return;
        }

        $usersRoles = [];
        foreach($roleIds as $id){
            $ur           = new UserRole();
            $ur->user_id  = $this->_model->id;
            $ur->role_id  = $id;
            $usersRoles[] = $ur;
        }

        foreach($usersRoles as $role){
            $role->save();
        }
    }
}