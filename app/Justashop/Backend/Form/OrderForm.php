<?php

namespace Justashop\Backend\Form;


use Justashop\Helpers\DI;

use Justashop\Helpers\Text;
use Justashop\Model\Address;
use Justashop\Model\Order;
use Justashop\Model\Payment;
use Justashop\Model\Delivery;
use \Phalcon\Validation;
use \Phalcon\Validation\Validator;

/**
 * Class PaymentTypeForm
 *
 * @property \Justashop\Model\Order _model
 * @package  \Justashop\Backend\Form
 */
class OrderForm extends BaseForm{

    /**
     * @param Order $model
     */
    public function setModel($model){
        if($model->id){
            $model->address      = Address::findById($model->address_id);
            $model->paymentType  = Payment\Type::findById($model->payment_type_id);
            $model->deliveryType = Delivery\Type::findById($model->delivery_type_id);
        }

        $this->_model = $model;
    }


    /**
     * @return Validation
     */
    public function _initValidation(){
        $t = $this->_model->getDI()->get(DI::TRANSLATOR);

        $validator = new Validation();

        $validator->add('status', new Validator\InclusionIn([
            'domain'  => Order::$statuses,
            'message' => $t->_('objects.orders.validation', 'status not in domain')
        ]));


        return $validator;
    }


    /**
     * @return mixed[]
     */
    protected function _initFields(){
        $t = $this->_model->getDI()->get(DI::TRANSLATOR);

        /** @var \Justashop\Components\Former\Builder $former */
        $former = $this->_model->getDI()->getShared('formBuilder');

        $this->addField('url', $former
                ->field('span', 'url')
                ->setLabel($t->_('objects.orders.fields', 'url'))
                ->setValue($this->_model->GetUrl())
            )
            ->addField('user_name', $former
                    ->field('span', 'user_name')
                    ->setLabel($t->_('objects.orders.fields', 'user_name'))
                    ->setValue($this->_model->user_name)
            )
            ->addField('user_email', $former
                    ->field('span', 'user_email')
                    ->setLabel($t->_('objects.orders.fields', 'user_email'))
                    ->setValue($this->_model->user_email)
            )
            ->addField('user_phone', $former
                    ->field('span', 'user_phone')
                    ->setLabel($t->_('objects.orders.fields', 'user_phone'))
                    ->setValue($this->_model->user_phone)
            )
            ->addField('address', $former
                    ->field('span', 'user_email')
                    ->setLabel($t->_('objects.orders.fields', 'address'))
                    ->setValue($this->_model->address->asString())
            )
            ->addField('payment_type', $former
                    ->field('span', 'payment_type')
                    ->setLabel($t->_('objects.orders.fields', 'paymentType'))
                    ->setValue($this->_model->paymentType ? $this->_model->paymentType->title : '-')
            )
            ->addField('delivery_type', $former
                    ->field('span', 'delivery_type')
                    ->setLabel($t->_('objects.orders.fields', 'deliveryType'))
                    ->setValue($this->_model->deliveryType->title)
            )
            ->addField('amount', $former
                    ->field('span', 'amount')
                    ->setLabel($t->_('objects.orders.fields', 'amount'))
                    ->setValue(Text::money_format($this->_model->amount))
            )
            ->addField('status', $former
                    ->field('select', 'status')
                    ->setLabel($t->_('objects.orders.fields', 'status'))
                    ->setSelected($this->_model->status)
                    ->setOptions($this->_getAvailableStatuses())
            );
    }


    protected function _getAvailableStatuses(){
        $t = $this->_model->getDI()->get(DI::TRANSLATOR);

        $result = [];
        foreach(Order::$statuses as $statusId){
            $result[$statusId] = $t->_('objects.orders.statuses', 'status' . $statusId);
        }

        return $result;
    }


    protected function _assignRequestDataToModel($data){
        $status = !empty($data['status']) ? (int)$data['status'] : null;
        if($status){
            $this->_model->status = $status;
        }
    }
} 