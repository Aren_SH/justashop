<?php
namespace Justashop\Backend\Form;


use Justashop\Helpers\DI;
use \Justashop\Model\Category;

use \Phalcon\Validation;
use \Phalcon\Validation\Validator;


class CategoriesForm extends BaseForm {

    protected $_withMetaTags = true;


    /**
     * @return Validation
     */
    public function _initValidation(){
        $validator = new Validation();

        $validator->add('title', new Validator\StringLength([
            'max'            => 500,
            'messageMaximum' => 'Product title too long'
        ]));


        return $validator;
    }


    /**
     * @return mixed[]
     */
    protected function _initFields(){
        $t = $this->_model->getDI()->get(DI::TRANSLATOR);

        /** @var \Justashop\Components\Former\Builder $former */
        $former = $this->_model->getDI()->getShared('formBuilder');

        $this->addField('title', $former
                ->field('input', 'title')
                ->setLabel($t->_('objects.categories.fields', 'title'))
                ->setErrorText($this->getMessageText('title'))
                ->setHelpText('Title is very important')
                ->setValue($this->_model->title)
                ->setRequired(true)
            )
            ->addField('parent_id', $former
                    ->field('select', 'parent_id')
                    ->setLabel($t->_('objects.categories.fields', 'parent'))
                    ->setErrorText($this->getMessageText('category_id'))
                    ->setSelected($this->_model->parent_id)
                    ->setOptions($this->_getCategoryOptions())
            )
            ->addField('description', $former
                    ->field('textarea', 'description')
                    ->setLabel($t->_('objects.categories.fields', 'description'))
                    ->setErrorText($this->getMessageText('description'))
                    ->setValue($this->_model->description)
                    ->addClass('editable')
                    ->setRequired(false)
            );
    }


    /**
     * @return string[]
     */
    protected function _getCategoryOptions(){
        $condition  = $this->_model->id ? 'id != ' . $this->_model->id : '';
        $categories = Category::find($condition);
        $result     = [];
        foreach($categories as $category){
            $result[$category->id] = $category->title;
        }

        return $result;
    }
}