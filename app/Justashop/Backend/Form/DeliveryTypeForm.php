<?php
namespace Justashop\Backend\Form;


use Justashop\Helpers\DI;
use \Justashop\Model\Delivery\Type;

use \Phalcon\Validation;
use \Phalcon\Validation\Validator;

/**
 * Class DeliveryTypeForm
 *
 * @property \Justashop\Model\Delivery\Type _model
 * @package  \Justashop\Backend\Form
 */
class DeliveryTypeForm extends BaseForm{


    /**
     * @return Validation
     */
    public function _initValidation(){
        $validator = new Validation();

        $t = $this->_model->getDI()->get(DI::TRANSLATOR);

        $validator->add('title', new Validator\PresenceOf([
            'message' => $t->_('objects.deliveryTypes.validation', 'title-required')
        ]));
        $validator->add('title', new Validator\StringLength([
            'max'            => 120,
            'messageMaximum' => $t->_('objects.deliveryTypes.validation', 'title-too-long')
        ]));


        $validator->add('description', new Validator\StringLength([
            'max'            => 500,
            'messageMaximum' => $t->_('objects.deliveryTypes.validation', 'description-too-long')
        ]));


        return $validator;
    }


    /**
     * @return mixed[]
     */
    protected function _initFields(){
        $t = $this->_model->getDI()->get(DI::TRANSLATOR);

        /** @var \Justashop\Components\Former\Builder $former */
        $former = $this->_model->getDI()->getShared('formBuilder');

        $this->addField('title', $former
                ->field('input', 'title')
                ->setLabel($t->_('objects.deliveryTypes.fields', 'title'))
                ->setErrorText($this->getMessageText('title'))
                ->setHelpText($t->_('objects.deliveryTypes.help', 'title', ''))
                ->setValue($this->_model->title)
                ->setRequired(true)
            )
            ->addField('description', $former
                    ->field('textarea', 'description')
                    ->setLabel($t->_('objects.deliveryTypes.fields', 'description'))
                    ->setErrorText($this->getMessageText('description'))
                    ->setValue($this->_model->description)
                    ->setRequired(false)
            )
            ->addField('price', $former
                    ->field('input', 'price')
                    ->setLabel($t->_('objects.deliveryTypes.fields', 'price'))
                    ->setErrorText($this->getMessageText('price'))
                    ->setHelpText($t->_('objects.deliveryTypes.help', 'price', ''))
                    ->setValue($this->_model->price)
                    ->setRequired(true)
            );
    }
}