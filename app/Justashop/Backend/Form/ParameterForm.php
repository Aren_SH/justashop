<?php
namespace Justashop\Backend\Form;


use Justashop\Helpers\DI;
use \Justashop\Components\Parameters\Model\Parameter;

use \Phalcon\Validation;
use \Phalcon\Validation\Validator;

/**
 * Class PaymentTypeForm
 *
 * @property Parameter _model
 * @package  \Justashop\Backend\Form
 */
class ParameterForm extends BaseForm{


    /**
     * @return Validation
     */
    public function _initValidation(){
        $validator = new Validation();

        $t = $this->getDI()->get(DI::TRANSLATOR);

        $validator->add('key', new Validator\PresenceOf([
            'message' => $t->_('objects.parameters.validation', 'key required')
        ]));
        $validator->add('key', new Validator\StringLength([
            'max'            => 120,
            'messageMaximum' => $t->_('objects.parameters.validation', 'key too long')
        ]));


        $validator->add('value', new Validator\PresenceOf([
            'message' => $t->_('objects.parameters.validation', 'value required')
        ]));

        $validator->add('description', new Validator\StringLength([
            'max'            => 500,
            'messageMaximum' => $t->_('objects.parameters.validation', 'description too long')
        ]));


        return $validator;
    }


    /**
     * @return mixed[]
     */
    protected function _initFields(){
        $t = $this->getDI()->get(DI::TRANSLATOR);

        /** @var \Justashop\Components\Former\Builder $former */
        $former = $this->getDI()->getShared('formBuilder');

        $this->addField('key', $former
                ->field('input', 'key')
                ->setLabel($t->_('objects.parameters.fields', 'key'))
                ->setErrorText($this->getMessageText('key'))
                ->setHelpText($t->_('objects.parameters.help', 'key', ''))
                ->setValue($this->_model->key)
                ->setRequired(true)
            )
            ->addField('value', $former
                    ->field('textarea', 'value')
                    ->setLabel($t->_('objects.parameters.fields', 'value'))
                    ->setErrorText($this->getMessageText('value'))
                    ->setHelpText($t->_('objects.parameters.help', 'value', ''))
                    ->setValue($this->_model->value)
                    ->setRequired(true)
            )
            ->addField('description', $former
                    ->field('textarea', 'description')
                    ->setLabel($t->_('objects.parameters.fields', 'description'))
                    ->setErrorText($this->getMessageText('description'))
                    ->setValue($this->_model->description)
                    ->setRequired(false)
            );
    }
}