<?php
namespace Justashop\Backend\Form;


use Justashop\Helpers\DI;
use \Justashop\Model\Payment\Type;

use \Phalcon\Validation;
use \Phalcon\Validation\Validator;

/**
 * Class PaymentTypeForm
 *
 * @property \Justashop\Model\Payment\Type _model
 * @package  \Justashop\Backend\Form
 */
class PaymentTypeForm extends BaseForm{


    /**
     * @return Validation
     */
    public function _initValidation(){
        $validator = new Validation();

        $t = $this->_model->getDI()->get(DI::TRANSLATOR);

        $validator->add('title', new Validator\PresenceOf([
            'message' => $t->_('objects.paymentTypes.validation', 'title-required')
        ]));
        $validator->add('title', new Validator\StringLength([
            'max'            => 120,
            'messageMaximum' => $t->_('objects.paymentTypes.validation', 'title-too-long')
        ]));


        $validator->add('description', new Validator\StringLength([
            'max'            => 500,
            'messageMaximum' => $t->_('objects.paymentTypes.validation', 'description-too-long')
        ]));


        $validator->add('adapter', new Validator\PresenceOf([
            'max'     => 500,
            'message' => $t->_('objects.paymentTypes.validation', 'adapter-required')
        ]));


        return $validator;
    }


    /**
     * @return mixed[]
     */
    protected function _initFields(){
        $t = $this->_model->getDI()->get(DI::TRANSLATOR);

        /** @var \Justashop\Components\Former\Builder $former */
        $former = $this->_model->getDI()->getShared('formBuilder');

        $this->addField('title', $former
                    ->field('input', 'title')
                    ->setLabel($t->_('objects.paymentTypes.fields', 'title'))
                    ->setErrorText($this->getMessageText('title'))
                    ->setHelpText($t->_('objects.paymentTypes.help', 'title', ''))
                    ->setValue($this->_model->title)
                    ->setRequired(true)
            )
            ->addField('description', $former
                    ->field('textarea', 'description')
                    ->setLabel($t->_('objects.paymentTypes.fields', 'description'))
                    ->setErrorText($this->getMessageText('description'))
                    ->setValue($this->_model->description)
                    ->setRequired(false)
            )
            ->addField('discount', $former
                    ->field('input', 'discount')
                    ->setLabel($t->_('objects.paymentTypes.fields', 'discount'))
                    ->setErrorText($this->getMessageText('discount'))
                    ->setHelpText($t->_('objects.paymentTypes.help', 'discount', ''))
                    ->setValue($this->_model->discount)
                    ->setRequired(true)
            )
            ->addField('adapter', $former
                    ->field('select', 'adapter')
                    ->setLabel($t->_('objects.paymentTypes.fields', 'adapter'))
                    ->setErrorText($this->getMessageText('adapter'))
                    ->setHelpText($t->_('objects.paymentTypes.help', 'adapter', ''))
                    ->setOptions($this->_getAdapterOptionsList())
                    ->setSelected($this->_model->adapter)
                    ->setRequired(true)
            );
    }

    /**
     * @return string[]
     */
    private function _getAdapterOptionsList(){
        $t = $this->_model->getDI()->get(DI::TRANSLATOR);

        $list = [Type::ADAPTER_TYPE_CACHE => $t->_('objects.paymentTypes.adapters', Type::ADAPTER_TYPE_CACHE)];

        $paymentManager = $this->getDI()->getShared('paymentManager');

        foreach($paymentManager->getAll() as $name => $system){
            $list[$name] = $t->_('objects.paymentTypes.adapters', $name);
        }

        return $list;
    }
}