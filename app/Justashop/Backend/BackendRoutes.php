<?php


namespace Justashop\Backend;

use Phalcon\Mvc\Router\Group as RouteGroup;

const BackendRoutePrefix = '/control-panel';

/**
 * Group Contains all routes for Backend part
 *
 * @package Justashop\Backend
 */
class BackendRoutes extends RouteGroup {

    public function initialize(){

        $this->setPaths( [
            'namespace'  => '\Justashop\Backend\Controllers',
            'controller' => 'main',
            'action'     => 'index',
        ] );

        $this->setPrefix( BackendRoutePrefix );

        $this->add('/',[
            'controller' => 'main',
            'action'     => 'index',
        ])->setName('backend.mainPage');

        /** Base routes for controllers */

        $this->add( '/:controller', [
            'controller' => 1,
            'action'     => 'index',
        ] )->setName('backend.indexPage');

        $this->add( '/:controller/add/?', [
            'controller' => 1,
            'action'     => 'add',
        ] )->setName('backend.addPage');

        $this->add( '/:controller/edit/:int/?', [
            'controller' => 1,
            'action'     => 'edit',
            'id'         => 2,
        ] )->setName('backend.editPage');

        $this->add( '/:controller/delete/:int/?', [
            'controller' => 1,
            'action'     => 'delete',
            'id'         => 2,
        ] )->setName('backend.deletePage');

        /** routes for authorization */

        $this->add( '/auth/:action', [
            'controller' => 'Account\Auth',
            'action'     => 1
        ] )->setName('backend.auth');


        /** helper routes */

        $this->add( '/int/upload\-image/[^\/]+?/?', [
            'controller' => 'Internal\UploadImage',
            'action'     => 'index',
            'profile'    => 'default',
        ] )->setName('backend.internal.uploadImage');

    }

} 