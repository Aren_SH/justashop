<?php

namespace Justashop\Validation\Validator;

use Phalcon\Mvc\Model;
use Phalcon\Validation\Validator,
    Phalcon\Validation\ValidatorInterface,
    Phalcon\Validation\Message;

class Unique extends Validator implements ValidatorInterface
{

    /**
     * Executes the validation
     *
     * @param  \Phalcon\Validation $validator
     * @param  string $attribute
     * @throws \Exception
     *
     * @return boolean
     */
    public function validate($validator, $attribute)
    {
        $value = $validator->getValue($attribute);
        $model = $this->getOption('model');
        $field = $this->getOption('field') ?: $attribute;

        $pk      = $this->getOption('primaryKey');
        $pkField = $this->getOption('primaryKeyField');


        if( !is_object( $model ) && !class_exists($model)){
            throw new \Exception(sprintf("Class '%s' not exists", $model) );
        }

       if( !is_object($model) ){
           $model = new $model();
       }
        $conditions = sprintf('%s = :val:', $field);
        if( $pk ){
            $conditions .= sprintf( ' AND %s!=%s', $pkField ?: 'id', $pk);
        }

        $count = $model->count([
            'conditions' => $conditions,
            'bind'       => ['val' => $value],
        ]);

        if( $count > 0 ){
            $message = $this->getOption('message') ?: sprintf('%s is not unique', $field);
            $validator->appendMessage( new Message($message, $attribute, 'unique' ) );
        };

        return $count  == 0;
    }

}