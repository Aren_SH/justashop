<?php

namespace Justashop\Model\Delivery;


use Justashop\Mvc\Model;

class Type extends Model {

    const DEFAULT_TYPE_ID = 1;

    public $id;

    public $title;

    public $description;

    public $price;

    public $status;

    /** @var string */
    protected static $_table = 'delivery_types';

    protected static $_view  = 'delivery_types';
} 