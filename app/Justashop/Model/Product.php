<?php

namespace Justashop\Model;

use \Justashop\Model\Product\Image;
use \Justashop\Mvc\Model;
use \Justashop\Mvc\Model\Resultset;

use \Phalcon\Mvc\Model\Query;
use \Phalcon\Mvc\Model\Behavior\Timestampable;
use \Phalcon\Mvc\ModelInterface;

class Product extends Model {

    /** @var int */
    public $id;

    /** @var string */
    public $sku;

    /** @var string */
    public $title;

    /** @var string */
    public $description;

    /** @var int */
    public $category_id;

    /** @var float */
    public $price;

    /** @var int timestamp */
    public $createdAt;

    /** @var int timestamp */
    public $updatedAt;

    /** @var bool */
    public $status;

    /** @var int */
    public $popularity = 0;

    /** @var int */
    public $meta_tag_id;

    /** @var Product\Image */
    public $mainImage;

    /** @var string */
    protected static $_table = 'products';

    protected static $_view  = 'products_view';

    protected static $_classMap;




    /**
     * @param  int $id
     *
     * @return ModelInterface|Resultset\Multiple
     */
    public static function findByCategoryId( $id ){
        if( !is_array( $id ) ) $id = [$id];

        $ids = array_filter($id, 'is_int');
        if( !$ids ) return null;

        if( count( $ids ) == 1) {
            $params = [
                'conditions' => 'category_id = :id:',
                'bind'       => ['id' => reset($id) ],
                'bindTypes'  => ['id' => Model\Query\Simple::BIND_PARAM_INT]
            ];
        } else {
            $params = [
                'conditions' => 'category_id IN (:id:)',
                'bind'       => ['id' => $id ],
                'bindTypes'  => ['id' => Model\Query\Simple::BIND_PARAM_INT_LIST]
            ];
        }

        return self::findWithRelations($params);
    }


    /**
     * @param  int   $limit
     * @param  int[] $maskIds
     * @return Resultset\Multiple
     */
    public static function findPopular($limit = 5, $maskIds = []){
        $params = [
            'limit' => $limit,
            'order' => 'popularity DESC NULLS LAST, "createdAt" DESC NULLS LAST'
        ];

        if( $maskIds ){
            $params['conditions'] = 'id NOT IN (:id:)';
            $params['bind']       = ['id' => $maskIds ];
            $params['bindTypes']  = ['id' => Model\Query\Simple::BIND_PARAM_INT_LIST];
        }

        return self::findWithRelations($params);
    }


    /**
     * @param  int   $limit
     * @param  int[] $maskIds
     * @return Resultset\Multiple
     */
    public static function findNew($limit = 5, $maskIds = []){
        $params = [
            'limit' => $limit,
            'order' => '"createdAt" DESC NULLS LAST'
        ];

        if( $maskIds ){
            $params['conditions'] = 'id NOT IN (:id:)';
            $params['bind']       = ['id' => $maskIds ];
            $params['bindTypes']  = ['id' => Model\Query\Simple::BIND_PARAM_INT_LIST];
        }

        return self::findWithRelations($params);
    }


    /**
     * @return string
     */
    public function getUrl(){
        return $this->getDI()->getShared('url')->get([
            'for'   => 'frontend.product',
            'id'    => $this->id,
            'alias' => \Justashop\Helpers\Text::slug_format($this->title)
        ]);
    }


    /**
     * @param  string $action
     * @return string
     */
    public function getControlPanelUrl( $action = 'edit' ){
        return $this->getDI()->getShared('url')->get([
            'for'        => sprintf( 'backend.%sPage', $action ),
            'id'         => $this->id,
            'controller' => 'products'
        ]);
    }


    /**
     * @param  string $size
     * @return string
     */
    public function getImageUrl( $size = 'm' ){
        if( !$this->mainImage ) return null;

        return $this->mainImage->getUrl( $size );
    }


    public static function getClass(){
        return __CLASS__;
    }


    protected function _setBehaviours(){
        $this->addBehavior( new Timestampable([
                'beforeValidationOnCreate' => [
                    'field'  => 'createdAt',
                    'format' => 'Y-m-d H:i:s'
                ]
            ]
        ) );

        $this->addBehavior( new Timestampable([
                'beforeValidationOnCreate' => [
                    'field'  => 'updatedAt',
                    'format' => 'Y-m-d H:i:s'
                ],
                'beforeValidationOnUpdate' => [
                    'field'  => 'updatedAt',
                    'format' => 'Y-m-d H:i:s'
                ]
            ]
        ) );

    }

    protected function _setRelations(){
        $this->belongsTo('category_id', Category::getClass(), 'id',
            ['alias' => 'category']
        );

        $this->hasMany('id', Product\Image::getClass(), 'product_id',
            ['alias' => 'images']
        );
    }


    /**
     * @return []
     */
    public static function getClassMap(){
        if(!static::$_classMap ){
            static::$_classMap = [
                'default'    => static::getClass(),
                'categories' => [
                    'class' => Category::getClass(),
                    'field' => 'category',
                ],
                'product_image' => [
                    'class'    => Image::getClass(),
                    'field'    => 'mainImage',
                    'classMap' => Image::getClassMap()
                ],
            ];
        }

        return static::$_classMap;
    }

} 