<?php

namespace Justashop\Model;

use \Justashop\Mvc\Model;
use Phalcon\Mvc\Model\Behavior\Timestampable;

class Category extends Model{

    /** @var int */
    public $id;

    /** @var string */
    public $title;

    /** @var string */
    public $description;

    /** @var int */
    public $parent_id;

    /** @var int timestamp */
    public $createdAt;

    /** @var bool */
    public $status;

    /** @var int */
    public $meta_tag_id;

    /** @var string */
    protected static $_table = 'categories';


    public static function getClass(){
        return __CLASS__;
    }



    /**
     * @return string
     */
    public function getUrl(){
        return $this->getDI()->getShared('url')->get([
            'for'   => 'frontend.categories',
            'id'    => $this->id,
            'alias' => \Justashop\Helpers\Text::slug_format($this->title)
        ]);
    }


    protected function _setRelations(){
        $this->hasMany('id', Product::getClass(), 'category_id',
            ['alias' => 'products']
        );
    }


    protected function _setBehaviours(){

        $this->addBehavior( new Timestampable([
                'beforeValidationOnCreate' => [
                    'field'  => 'createdAt',
                    'format' => 'Y-m-d H:i:s'
                ]
            ]
        ) );

    }
} 