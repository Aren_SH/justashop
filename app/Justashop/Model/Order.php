<?php

namespace Justashop\Model;


use \Justashop\Mvc\Model;
use \Phalcon\Mvc\Model\Query;
use \Phalcon\Mvc\Model\Behavior\Timestampable;

class Order extends Model {

    const STATUS_CREATED             = 1;

    const STATUS_CANCELED            = 2;

    const STATUS_NOT_PAID            = 3;

    const STATUS_PAYMENT_IN_PROGRESS = 4;

    const STATUS_PAID                = 5;

    const STATUS_IN_PROGRESS         = 6;

    const STATUS_NOT_SHIPPED         = 7;

    const STATUS_SHIPPED             = 8;

    const STATUS_COMPLETED           = 9;

    /** @var int */
    public $id;

    /** @var int */
    public $user_id;

    /** @var int */
    public $address_id;

    /** @var int */
    public $user_name;

    /** @var string */
    public $user_email;

    /** @var string */
    public $user_phone;

    /** @var string */
    public $hash;

    /** @var int */
    public $payment_type_id;

    /** @var int */
    public $delivery_type_id;

    /** @var int */
    public $amount;

    /** @var int timestamp */
    public $createdAt;

    /** @var int */
    public $status;

    /** @var Address */
    public $address;

    /** @var Delivery\Type */
    public $deliveryType;

    /** @var Payment\Type */
    public $paymentType;

    public static $statuses = [
        self::STATUS_CREATED,
        self::STATUS_CANCELED,
        self::STATUS_NOT_PAID,
        self::STATUS_PAID,
        self::STATUS_IN_PROGRESS,
        self::STATUS_NOT_SHIPPED,
        self::STATUS_SHIPPED,
        self::STATUS_COMPLETED,
    ];


    /** @var string */
    protected static $_table = 'orders';

    protected static $_view  = 'orders_view';

    protected static $_source;

    protected static $_classMap;


    /**
     * @return string
     */
    public function getUrl( $action = 'show'){
        return $this->getDI()->getShared('url')->get([
            'for'    => 'frontend.order',
            'action' => $action,
            'id'     => $this->id,
            'hash'   => $this->hash
        ]);
    }


    /**
     * @param  string $action
     * @return string
     */
    public function getControlPanelUrl( $action = 'edit' ){
        return $this->getDI()->getShared('url')->get([
            'for'        => sprintf( 'backend.%sPage', $action ),
            'id'         => $this->id,
            'controller' => 'orders'
        ]);
    }




    public function getFinalAmount(){
        return $this->amount * ( $this->payment_type_id && $this->paymentType ? $this->paymentType->discount : 1 );
    }


    public function beforeSave(){
        if( !$this->hash ) {
            $this->hash = hash('sha256', $this->address_id . $this->phone . $this->email );
        }
    }


    protected function _setBehaviours(){
        $this->addBehavior( new Timestampable([
                'beforeValidationOnCreate' => [
                    'field'  => 'createdAt',
                    'format' => 'Y-m-d H:i:s'
                ]
            ]
        ) );

    }


    /**
     * @return []
     */
    public static function getClassMap(){
        if(!static::$_classMap ){
            static::$_classMap = [
                'default'    => static::getClass(),
                'address' => [
                    'class' => Address::getClass(),
                    'field' => 'address',
                ],
                'delivery_type' => [
                    'class' => Delivery\Type::getClass(),
                    'field' => 'deliveryType',
                ],
                'payment_type' => [
                    'class' => Payment\Type::getClass(),
                    'field' => 'paymentType',
                ],
            ];
        }

        return static::$_classMap;
    }

} 