<?php

namespace Justashop\Model\Payment;


use Justashop\Mvc\Model;

class Type extends Model {

    const DEFAULT_TYPE_ID    = 1;

    const ADAPTER_TYPE_CACHE = '__cache';

    /** @var int */
    public $id;

    /** @var string */
    public $title;

    /** @var string */
    public $description;

    /** @var float */
    public $discount;

    /** @var string */
    public $adapter;

    /** @var int */
    public $status;

    /** @var string */
    protected static $_table = 'payment_types';

    protected static $_view  = 'payment_types';
} 