<?php

namespace Justashop\Model\Product;

use Justashop\Model\Product;
use Justashop\Mvc\Model;
use Phalcon\Db\Column;

class Image extends Model {

    /** @var int */
    public $id;

    /** @var string */
    public $product_id;

    /** @var string */
    public $title;

    /** @var string */
    public $description;

    public $image_id;

    /** @var bool */
    public $status;

    /** @var bool */
    public $is_main;

    /** @var string */
    protected static $_table = 'product_images';

    protected static $_view  = 'product_images_view';

    protected static $_classMap;


    public static function getClass(){
        return __CLASS__;
    }


    public function getUrl($size = null){
        if(!$this->image) return null;

        return $this->image->getUrl($size);
    }


    /**
     * @return []
     */
    public static function getClassMap(){
        if(!static::$_classMap ){
            static::$_classMap = [
                'default'    => static::getClass(),
                'image' => [
                    'class' => '\\Justashop\\Components\\Image\\Model\\Image',
                    'field' => 'image',
                ]
            ];
        }

        return static::$_classMap;
    }


    protected function _setRelations(){
        $this->belongsTo('product_id', Product::getClass(), 'id',
            ['alias' => 'product']
        );

        $this->belongsTo('image_id', '\\Justashop\\Components\\Image\\Model\\Image', 'id',
            ['alias' => 'image']
        );
    }


    /**
     * @param  int  $id
     *
     * @return \Phalcon\Mvc\Model\ResultsetInterface
     */
    public static function findByProductId($id){
        if(!is_array($id)){
            $conditions = 'product_id = :id:';
            $bind       = ['id' => $id];
            $bindTypes  = ['id' => Column::BIND_PARAM_INT];
        } else {
            $bind      = ['id' => $id];
            $bindTypes = ['id' => Model\Query\Simple::BIND_PARAM_INT_LIST];

            $conditions = sprintf('product_id IN (:id:)' );
        }


        return self::findWithRelations([
            'conditions' => $conditions,
            'bind'       =>  $bind,
            'bindTypes'  => $bindTypes
        ]);
    }


    /**
     * @param $id
     * @return Image
     */
    public static function findMainByProductId($id){
        $conditions = 'product_id = :id: AND is_main = true';
        $bind       = ['id' => $id];
        $bindTypes  = ['id' => Column::BIND_PARAM_INT];

        return self::findFirst([
            'conditions' => $conditions,
            'bind'       =>  $bind,
            'bindTypes'  => $bindTypes
        ]);
    }
} 