<?php


namespace Justashop\Model\Order;


use Justashop\Components\Former\Model\ModelFormInterface;
use Justashop\Mvc\Model;
use Justashop\Mvc\Model\Resultset;

class Product extends Model {

    public $id;

    public $order_id;

    public $product_id;

    public $price;

    public $quantity;

    protected static $_table = 'order_products';
    protected static $_view  = 'order_products';
    protected static $_source;




    /**
     * @param  int $id
     *
     * @return ModelFormInterface|Resultset\Multiple
     */
    public static function findByOrderId( $id ){
        if( !is_array( $id ) ) $id = [$id];

        $ids = array_filter($id, 'is_int');
        if( !$ids ) return null;

        if( count( $ids ) == 1) {
            $params = [
                'conditions' => 'order_id = :id:',
                'bind'       => ['id' => reset($id) ],
                'bindTypes'  => ['id' => Model\Query\Simple::BIND_PARAM_INT]
            ];
        } else {
            $params = [
                'conditions' => 'order_id IN (:id:)',
                'bind'       => ['id' => $id ],
                'bindTypes'  => ['id' => Model\Query\Simple::BIND_PARAM_INT_LIST]
            ];
        }

        return self::findWithRelations($params);
    }
}