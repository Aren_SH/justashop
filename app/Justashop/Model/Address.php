<?php
namespace Justashop\Model;


use \Justashop\Mvc\Model;
use \Phalcon\Db\Column;
use \Phalcon\Mvc\Model\Query;
use \Phalcon\Mvc\Model\Behavior\Timestampable;

class Address extends Model {

    /** @var int */
    public $id;

    /** @var int */
    public $user_id;

    /** @var int */
    public $country;

    /** @var string */
    public $zip_code;

    /** @var string */
    public $address;

    /** @var int */
    public $createdAt;

    /** @var int */
    public $status;

    /** @var string */
    protected static $_table = 'addresses';

    protected static $_view  = 'addresses_view';

    protected static $_classMap;



    public function asString(){
        return sprintf('%s, %s', $this->zip_code, $this->address );
    }

    /**
     * @param  int $id
     *
     * @return Order
     */
    public static function findById($id){
        return self::findFirst([
            'conditions' => 'id = :id:',
            'bind'       => ['id' => $id ],
            'bindTypes'  => ['id' => Column::BIND_PARAM_INT]
        ]);
    }


    protected function _setBehaviours(){
        $this->addBehavior( new Timestampable([
                'beforeValidationOnCreate' => [
                    'field'  => 'createdAt',
                    'format' => 'Y-m-d H:i:s'
                ]
            ]
        ) );

    }

    protected function _setRelations(){
    }


    /**
     * @return []
     */
    public static function getClassMap(){
        if(!static::$_classMap ){
            static::$_classMap = [
                'default'    => static::getClass(),
            ];
        }

        return static::$_classMap;
    }

} 