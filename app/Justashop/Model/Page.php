<?php

namespace Justashop\Model;

use Justashop\Mvc\Model;
use Phalcon\Mvc\Model\Behavior\Timestampable;


class Page extends Model {

    /** @var int */
    public $id;

    /** @var string */
    public $title;

    /** @var string */
    public $alias;

    /** @var string */
    public $content;

    /** @var string */
    public $createdAt;

    /** @var int */
    public $status;

    /** @var int */
    public $meta_tag_id;


    protected static $_table = 'pages';

    protected static $_view  = 'pages';



    public static function findByAlias( $alias ){
        if( !$alias ) return null;

        return self::findFirst([
            'conditions' => 'alias = :alias:',
            'bind'       => ['alias' => $alias ],
            'bindTypes'  => ['alias' => Model\Query\Simple::BIND_PARAM_STR],
        ]);
    }


    protected function _setBehaviours(){
        $this->addBehavior( new Timestampable([
                'beforeValidationOnCreate' => [
                    'field'  => 'createdAt',
                    'format' => 'Y-m-d H:i:s'
                ]
            ]
        ) );
    }
}