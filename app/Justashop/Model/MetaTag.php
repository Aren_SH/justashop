<?php

namespace Justashop\Model;


use Justashop\Mvc\Model;

class MetaTag extends Model {

    public $title;

    public $description;

    public $keywords;

    public $robots;

    public $props;

    /** @var string */
    protected static $_table = 'meta_tags';

    protected static $_view  = 'meta_tags';

} 