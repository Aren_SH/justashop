<?php

namespace Justashop\Components;

use Justashop\Components\Common\ListMixin;
use Phalcon\Mvc\User\Component;

class Menu extends Component implements \Iterator {
    use ListMixin;
}