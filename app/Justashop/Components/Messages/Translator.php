<?php

namespace Justashop\Components\Messages;

use Justashop\Components;

class Translator extends Components\Messages {

    /**
     * @var string ru_RU, en_US etc.
     */
    protected $_language;


    /**
     * @param  string $path
     * @param  string $text
     * @param  string $default
     * @return string
     */
    public function _($path, $text, $default = null){

        $path = sprintf('%s%s%s', $this->getLanguage(), $this->getDelimiter(), $path );

        return $this->get($path, $text, $default);
    }


    /**
     * @param  string $text
     * @param  int    $num
     * @param  string $default
     * @return string
     */
    public function declension($text, $num, $default = null){
        $cases = array (2, 0, 1, 1, 1, 2);
        $case  = ($num % 100 > 4 && $num % 100 < 20) ? 2 : $cases[min($num % 10, 5)];

        return $this->_('declensions.' . $text, $case, $default ?: $text);
    }


    /**
     * @param string $lng
     */
    public function setLanguage( $lng ){
        $this->_language = $lng;
    }

    /**
     * @return string
     */
    public function getLanguage(){
        return $this->_language;
    }
} 