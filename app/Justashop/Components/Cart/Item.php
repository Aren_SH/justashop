<?php

namespace Justashop\Components\Cart;


class Item {

    /** @var mixed */
    protected $_id;

    /** @var float */
    protected $_price;

    /** @var int  */
    protected $_quantity;

    /** @var float */
    protected $_discount = 1;

    /** @var string */
    protected $_title;

    /** @var string */
    protected $_thumbnail;

    protected $_url;

    /**
     * @param     $id
     * @param     $price
     * @param int $count
     */
    public function __construct($id, $price, $qty = 1){
        $this->_id       = $id;
        $this->_price    = (float)$price;
        $this->_quantity = (int)$qty;
    }

    public function getId(){
        return $this->_id;
    }


    /**
     * @param string $title
     */
    public function setTitle($title){
        $this->_title = $title;
    }


    /**
     * @return string
     */
    public function getTitle(){
        return $this->_title ?: $this->_id;
    }

    /**
     * @param string $url
     */
    public function setUrl($url){
        $this->_url = $url;
    }


    /**
     * @return string
     */
    public function getUrl(){
        return $this->_url;
    }


    /**
     * @param string $imageUrl
     */
    public function setThumbnail($imageUrl){
        $this->_thumbnail = $imageUrl;
    }


    /**
     * @return string
     */
    public function getThumbnail(){
        return $this->_thumbnail;
    }


    /**
     * @return float
     */
    public function getPrice(){
        return $this->_price;
    }



    /**
     * @return float
     */
    public function getTotalPrice(){
        return $this->_price * $this->_quantity * $this->_discount;
    }


    /**
     * @param float $price
     */
    public function setPrice($price){
        $this->_price = $price;
    }


    /**
     * @return int
     */
    public function getQuantity(){
        return $this->_quantity;
    }


    /**
     * @param int $count
     */
    public function setQuantity($qty){
        $this->_quantity = $qty;
    }


    /**
     * @param float $discount
     */
    public function setDiscount($discount){
        $this->_discount = $discount && $discount > 0 && $discount < 1 ? $discount : 1;
    }


    /**
     * @return float
     */
    public function getDiscount(){
        return $this->_discount;
    }
} 