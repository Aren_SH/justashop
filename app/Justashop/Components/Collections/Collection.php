<?php

namespace Justashop\Components\Collections;

class Collection implements \Iterator {

    /** @var \mixed[] */
    protected $_entries;


    public function __construct($entries = []){
        $this->_entries = $entries;
    }


    /**
     * @return \mixed[]
     */
    public function asArray(){
        return $this->_entries;
    }


    /**
     * @return int
     */
    public function count(){
        return count($this->_entries);
    }

    /**
     * @param  \mixed $key
     *
     * @return bool
     */
    public function hasKey($key){
        return array_key_exists($key, $this->_entries);
    }


    /**
     * @param  mixed $key
     * @param  mixed $default
     *
     * @return mixed
     */
    public function get($key, $default = null){
        return array_key_exists($key, $this->_entries) ? $this->_entries[$key] : $default;
    }


    /**
     * @param mixed $key
     */
    public function remove($key){
        if($this->hasKey($key)){
            unset($this->_entries[$key]);
        }
    }


    /**
     * @param  mixed $key
     * @param  mixed $val
     */
    public function set($key, $val){
        $this->_entries[$key] = $val;
    }


    /**
     * @param mixed $item
     */
    public function append($item){
        $this->_entries[] = $item;
    }


    /**
     * @param  callable [$callback,...]
     * @return Collection
     */
    public function filter(){
        $callbacks    = func_get_args();
        $hasCallbacks = count($callbacks) > 0;

        $result = [];
        foreach($this->_entries as &$entry){
            $pass = true;
            if($hasCallbacks) {
                foreach ($callbacks as $callback) {
                    if (!$callback($entry)){
                        $pass = false;
                        break;
                    };
                }
            } else {
                $pass = !empty($entry);
            }

            if($pass){
                $result[] = $entry;
            }
        }

        return static($result);
    }


    /**
     * @param  callable [$callback,...]
     * @throws \Exception
     *
     * @return Collection self
     */
    public function map(){
        $callbacks = func_get_args();

        if(count($callbacks) < 1){
            throw new \Exception("ArgumentError: No callback given");
        }

        foreach($this->_entries as &$entry){
            foreach ($callbacks as $callback) {
                $callback($entry);
            }
        }

        return $this;
    }


    /**
     * @param  callable [$callback,...]
     * @throws \Exception
     *
     * @return Collection self
     */
    public function modify(){
        $callbacks = func_get_args();


        if(count($callbacks) < 1){
            throw new \Exception("ArgumentError: No callback given");
        }

        foreach ($callbacks as $callback) {
            $callback($this->_entries);
        }

        return $this;
    }


    /**
     * @return mixed
     */
    public function current(){
        return current($this->_entries);
    }


    public function next(){
        next($this->_entries);
    }


    /**
     * @return mixed
     */
    public function key(){
        return key($this->_entries);
    }


    /**
     * @return bool
     */
    public function valid(){
        return current($this->_entries) !== false;
    }


    public function rewind(){
        reset($this->_entries);
    }
} 