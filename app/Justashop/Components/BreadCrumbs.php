<?php


namespace Justashop\Components;

use Justashop\Components\Common\ListMixin;
use Phalcon\Mvc\User\Component;

class BreadCrumbs extends Component implements \Iterator {
    use ListMixin;
}