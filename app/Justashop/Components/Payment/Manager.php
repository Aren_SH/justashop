<?php
namespace Justashop\Components\Payment;


class Manager {

    const TYPE_CARD         = 'card';
    const TYPE_YANDEX_MONEY = 'yandex';
    const TYPE_QIWI_WALLET  = 'qiwi';
    const TYPE_PAYPAL       = 'paypal';



    public $_systems = [];


    /**
     * @param string           $name
     * @param AdapterInterface $system
     */
    public function register($name, $system){
        $this->_systems[$name] = $system;
    }


    /**
     * @param  string           $name
     * @return AdapterInterface
     */
    public function get($name){
        return array_key_exists($name, $this->_systems) ? $this->_systems[$name] : null;
    }

    /**
     * @return AdapterInterface[]
     */
    public function getAll(){
        return $this->_systems;
    }

    /**
     * @param $name
     */
    public function remove($name){
        if(array_key_exists($name, $this->_systems)){
            unset($this->_systems[$name]);
        }
    }

} 