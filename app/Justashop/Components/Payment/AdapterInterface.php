<?php

namespace Justashop\Components\Payment;


interface AdapterInterface {


    /**
     * @param  Data  $data
     * @return array
     */
    public function getRequestData($data);


    /**
     * @return string
     */
    public function getRequestUrl();


    /**
     * @return string
     */
    public function getTitle();


    /**
     * @return string
     */
    public function getDescription();



    /**
     * @param  Data $response
     * @return bool
     */
    public function isResponseValid($response);


    /**
     * @param  Data $data
     * @return string
     */
    public function responseSuccess($data);


    /**
     * @param  array $data
     * @return string
     */
    public function responseFail($data);
} 