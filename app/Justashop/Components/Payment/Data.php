<?php

namespace Justashop\Components\Payment;

/**
 * Payment data
 * Wrapper class for convenience
 *
 * @package Justashop\Components\Payment
 */
class Data implements \ArrayAccess {

    /** @var int */
    public $amount;

    /** @var int */
    public $orderId;

    /** @var string */
    public $email;

    /** @var string */
    public $description;

    /** @var string[] */
    protected $_additionalParams = [];


    /**
     * @param  string $name
     * @return mixed
     */
    public function __get($name){
        return array_key_exists($name, $this->_additionalParams) ? $this->_additionalParams[$name] : null;
    }

    /**
     * @param string $name
     * @param mixed  $value
     */
    public function __set($name, $value){
        $this->_additionalParams[$name] = $value;
    }

    /**
     * @param string $offset
     * @param mixed $value
     */
    public function offsetSet($offset, $value){
        $this->{$offset} = $value;
    }

    /**
     * @param  string $offset
     * @return mixed
     */
    public function offsetGet($offset){
        return $this->{$offset};
    }

    /**
     * @param string $offset
     */
    public function offsetUnset($offset){
        $this->{$offset} = null;
    }

    /**
     * @param  string $offset
     * @return bool
     */
    public function offsetExists($offset){
        return $this->{$offset} != null;
    }

} 