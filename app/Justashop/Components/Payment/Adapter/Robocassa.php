<?php

namespace Justashop\Components\Payment\Adapter;


use Justashop\Components\Payment\AdapterInterface;
use Justashop\Components\Payment\Data;
use Phalcon\Config;

class Robocassa implements AdapterInterface {

    const DEFAULT_LANG     = 'en';

    const DEFAULT_ENCODING = 'utf-8';

    /**
     * @var bool
     */
    private $_isDeferred = false;


    /**
     * @var string
     */
    private $_title;


    /**
     * @var string
     */
    private $_description;

    /**
     * @var string
     */
    private $_requestUrl;


    /**
     * sMerchantLogin
     *
     * @var string
     */
    private $_login;


    /**
     * password1
     *
     * @var string
     */
    private $_requestPassword;


    /**
     * password2
     *
     * @var string
     */
    private $_responsePassword;


    /**
     * sCulture
     *
     * @var string
     */
    private $_lang;


    /**
     * sEncoding
     *
     * @var string
     */
    private $_encoding;


    /**
     * all shp_* parameters
     *
     * @var array
     */
    private $_additionalParams = [];


    private $_signDelimiter = ':';


    private $_paramsPrefix = 'Shp_';

    /**
     * @param  Config     $config
     * @throws \Exception
     */
    public function __construct($config){
        foreach(['login', 'password1', 'password2', 'url'] as $param){
            if( empty($config[$param]) ){
                throw new \Exception(sprintf("Parameter %s required", $param));
            }
        }

        $this->_login            = $config->get('login');
        $this->_requestPassword  = $config->get('password1');
        $this->_responsePassword = $config->get('password2');
        $this->_requestUrl       = $config->get('url');
        $this->_lang             = $config->get('lang', self::DEFAULT_LANG);
        $this->_encoding         = $config->get('encoding', self::DEFAULT_ENCODING);

        $this->_title            = $config->get('title', 'Robocassa');
        $this->_description      = $config->get('title', 'Robocassa payment provider');

    }


    /**
     * @return bool
     */
    public function isDeferred(){
        return $this->_isDeferred;
    }


    public function getRequestUrl(){
        return $this->_requestUrl;
    }


    /**
     * @param  Data $data
     * @return array
     */
    public function getRequestData($data){
        $result = [
            'MrchLogin'      => $this->_login,
            'Culture'        => $this->_lang,
            'Encoding'       => $this->_encoding,
            'OutSum'         => $data->amount,
            'InvId'          => $data->orderId,
            'Desc'           => $data->description,
            'Email'          => $data->email,
            'SignatureValue' => $this->_getRequestSignature($data),
        ];

        foreach( $this->_additionalParams as $param ){
            if( !$data->{$param} ) continue;

            $result[$this->_paramsPrefix . $param] = $data->{$param};
        }

        return $result;

    }


    /**
     * @param  array $response
     * @return Data
     */
    public function getPaymentDataFromResponse($response){
        $data = new Data();

        $data->amount  = empty($response['OutSum']) ? 0 : floatval($response['OutSum']);
        $data->orderId = empty($response['InvId'])  ? 0 : intval($response['InvId']);

        foreach($this->_additionalParams as $param){
            if( empty($response[$this->_paramsPrefix . $param])) continue;
            $data->{$param} = $response[$param];
        }

        return $data;
    }


    /**
     * @param  array $response
     * @return bool
     */
    public function isResponseValid($response){
        $signature = $this->_getResponseSignature($response);

        return !empty($response['SignatureValue']) && $response['SignatureValue'] == $signature;
    }


    /**
     * @param  Data  $data
     * @return string
     */
    public function responseSuccess($data){
        return 'OK' . $data->orderId;
    }


    /**
     * @param  Data $data
     * @return string
     */
    public function responseFail($data){
        return 'validation failed';
    }


    /**
     * @param  Data   $data
     * @return string
     */
    protected function _getRequestSignature($data){
        $main = join($this->_signDelimiter, [$this->_login, $data->amount, $data->orderId, $this->_requestPassword]);
        $add  = $this->_getAdditionalParamsString($data);

        return md5( $main . $add );
    }


    /**
     * @param  array  $data
     * @return string
     */
    protected function _getResponseSignature($data){
        $main = join($this->_signDelimiter, [$data['OutSum'], $data['InvId'], $this->_responsePassword]);
        $add  = $this->_getAdditionalParamsString($data);

        return md5( $main . $add );
    }


    /**
     * @param  Data   $data
     * @return string
     */
    protected function _getAdditionalParamsString($data){
        if( !$this->_additionalParams ){
            return '';
        }

        $params = [];
        foreach( $this->_additionalParams as $param ){
            $value = isset($data[$param]) ?: '';
            if( !$value && isset($data[$this->_paramsPrefix . $param]) ){
                $value = isset($data[$this->_paramsPrefix . $param]);
            };

            $params[] = sprintf('%s%s=%s', $this->_paramsPrefix, $param, urlencode($value));
        }

        ksort( $params );

        return join($this->_signDelimiter, $params);
    }


    public function getTitle(){
        return $this->_title;
    }


    public function getDescription(){
        return $this->_description;
    }
} 