<?php

namespace Justashop\Components\Account\Authentication;

use Justashop\Components\Account\Model\ActionToken;
use Justashop\Components\Account\Model\User;

use Phalcon\Db\Column;
use Phalcon\Mvc\User\Component;

class Manager extends Component{

    /** @var int */
    protected $_tokenLifeTime = 172800;

    /** @var string */
    protected $_tokenStorageKey = 'auth-token';

    /** @var \Justashop\Components\Account\Hasher */
    protected $_hasher;


    /**
     * Create new Auth class.
     * 'hashSalt' must be specified within $parameters array
     * 'hashAlgo' may be specified to replace default hashing algorithm (sha256)
     *
     * @param  array $parameters
     * @throws \Exception
     */
    public function __construct($parameters = array()){

        if(!empty($parameters['tokenLifeTime'])){
            $this->_tokenLifeTime = (int)$parameters['tokenLifeTime'];
        }
    }


    /**
     * @param $hasher
     */
    public function setHasher($hasher){
        $this->_hasher = $hasher;
    }


    public function getHasher(){
        return $this->_hasher;
    }


    /**
     * @return User
     */
    public function getCurrentUser(){
        $token = $this->_getStoredToken();

        if(!$token){
            return null;
        }

        return $this->getUserByToken($token);
    }


    public function getUserById($id){
        return User::findById($id);
    }


    /**
     * @param  string $str authentication token
     * @return User
     */
    public function getUserByToken($str){
        return ActionToken::findUser($str, ActionToken::ACTION_AUTHENTICATE);
    }


    /**
     * @param  string $login
     * @param  string $pass
     * @return User
     */
    public function getUserByCredentials($login, $pass){
        $user = User::findFirst([
            'conditions' => 'login = :login: AND password = :password:',
            'bind'       => ['login' => $login, 'password' => $this->_hasher->passwordHash($pass)],
            'bindTypes'  => ['login' => Column::BIND_PARAM_STR, 'password' => Column::BIND_PARAM_STR]
        ]);

        return $user ? : null;
    }


    /**
     * @param  User $user
     * @return bool
     */
    public function remember($user){

        $this->_removeTokens($user->id);

        $token = $this->_createToken($user->id);
        if($token){
            $this->_storeToken($token);

            return true;
        }
        else{
            return false;
        }
    }


    /**
     * @param  User $user
     * @return bool
     */
    public function forget($user){
        return $this->_removeTokens($user->id);
    }


    /**
     * @return string
     */
    protected function _getStoredToken(){
        /** @var \Phalcon\Session\AdapterInterface $session */
        $session = $this->getDI()->getShared('session');
        if(!$session->isStarted())
            $session->start();

        $sessionToken = $session->get($this->_tokenStorageKey);

        if(!$sessionToken)
            return null;

        /** @var \Phalcon\Http\Response\Cookies $cookies */
        $cookies     = $this->getDI()->getShared('cookies');
        $cookieToken = $cookies->get($this->_tokenStorageKey)->getValue();

        if(!$cookieToken)
            return null;

        if($cookieToken != $sessionToken){
            return null;
        }

        return $sessionToken;
    }


    /**
     * @param string $str
     */
    protected function _storeToken($str){
        /** @var \Phalcon\Session\AdapterInterface $session */
        $session = $this->getDI()->getShared('session');
        /** @var \Phalcon\Http\Response\Cookies $cookies */
        $cookies = $this->getDI()->getShared('cookies');
        if(!$session->isStarted()){
            $session->start();
        }
        $session->set($this->_tokenStorageKey, $str);
        $cookies->set($this->_tokenStorageKey, $str, time() + $this->_tokenLifeTime, '/', false, null, true);
    }


    /**
     * Add auth token for user
     *
     * @param  int $userId
     * @return ActionToken
     */
    protected function _createToken($userId){

        $token            = new ActionToken();
        $token->token     = $this->_hasher->tokenHash($userId);
        $token->user_id   = $userId;
        $token->expiredAt = date('Y-m-d H:i:s', time() + $this->_tokenLifeTime);
        $token->action    = ActionToken::ACTION_AUTHENTICATE;

        return $token->save() ? $token->token : null;
    }


    /**
     * Remove All user auth tokens
     *
     * @param  string $userId
     * @return bool
     */
    protected function _removeTokens($userId){
        return ActionToken::deleteByUserId($userId, ActionToken::ACTION_AUTHENTICATE);
    }
} 