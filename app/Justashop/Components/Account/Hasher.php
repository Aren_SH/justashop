<?php

namespace Justashop\Components\Account;


class Hasher {

    /** @var string */
    protected $_hashSalt;

    /** @var string  */
    protected $_hashALgo        = 'sha256';


    /**
     * Create new Auth class.
     * 'hashSalt' must be specified within $parameters array
     * 'hashAlgo' may be specified to replace default hashing algorithm (sha256)
     *
     * @param  array $parameters
     * @throws \Exception
     */
    public function __construct( $parameters = [] ){

        if( empty( $parameters['hashSalt'] ) ){
            throw new \Exception( "Salt parameter not exists in parameters array" );
        } else {
            $this->_hashSalt = $parameters['hashSalt'];
        }

        $existedAlgos = hash_algos();
        if( !empty( $parameters['hashAlgo'] ) && in_array( $parameters['hashAlgo'], $existedAlgos ) ){
            $this->_hashALgo = $parameters['hashAlgo'];
        }
    }

    /**
     * @param  string $str
     * @return string
     */
    public function passwordHash( $str ){
        return $str ? hash( $this->_hashALgo, $this->_addSalt( $str ) ) : null;
    }


    /**
     * Return random 64-char length string
     *
     * @param  string $str
     * @return string
     */
    public function tokenHash( $str ){
        return hash('sha256', time() . $str . md5( rand() . $str ));
    }

    /**
     * Return random 8-char length string
     *
     * @param  string $str
     * @return string
     */
    public function shortTokenHash( $str ){
        return hash('crc32', time() . $str .md5( rand() ) );
    }


    /**
     * @param  string $str
     * @return string
     */
    protected function _addSalt( $str ){
        return $str . $this->_hashSalt;
    }
} 