<?php

namespace Justashop\Components\Account\Registration;


use Justashop\Components\Account\Model\User;
use Phalcon\Validation;
use Phalcon\Validation\Validator;

class DefaultValidation extends Validation {


    public function __construct() {
        $this->add( 'email', new Validator\Email([
            'message' => 'Email is not valid',
        ]) );
        $this->add( 'email', new \Justashop\Validation\Validator\Unique([
            'model'   => User::getClass(),
            'field'   => 'email',
            'message' => 'Email already exists',
        ]) );


        $this->add( 'login', new Validator\PresenceOf([
            'messageMinimum' => 'Login required',
        ]) );
        $this->add( 'login', new Validator\StringLength([
            'max'            => 256,
            'min'            => 5,
            'messageMaximum' => 'Login to long',
            'messageMinimum' => 'Login to short',
        ]) );
        $this->add( 'password', new Validator\PresenceOf([
            'messageMinimum' => 'Password required',
        ]) );
        $this->add( 'password', new Validator\StringLength([
            'max'            => 256,
            'min'            => 5,
            'messageMaximum' => 'Password to long',
            'messageMinimum' => 'Password to short',
        ]) );
        $this->add( 'password', new Validator\Confirmation([
            'message' => 'Password doesn\'t match confirmation',
            'with'    => 'passwordConfirm'
        ]) );
    }


} 