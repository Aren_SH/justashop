<?php

namespace Justashop\Components\Account\Registration;

use Justashop\Components\Account\Model\ActionToken;
use Justashop\Components\Account\Model\User;
use Phalcon\Mvc\User\Component;
use Phalcon\Validation\Message;
use Phalcon\Validation;
use Phalcon\Validation\Validator;


class Manager extends Component{

    /** @var \Justashop\Components\Account\Hasher */
    protected $_hasher;

    /** @var  Validation */
    protected $_validation;

    /** @var Message\Group */
    protected $_errorMessages;

    /** @var int */
    protected $_activationTokenLifeTime = 3600;

    /** @var int */
    protected $_passwordRecoveryTokenLifeTime = 900;


    public function getHasher(){
        return $this->_hasher;
    }


    /**
     * @param $hasher
     */
    public function setHasher($hasher){
        $this->_hasher = $hasher;
    }


    public function getValidation(){
        return $this->_validation;
    }


    /**
     * @param Validation $validation
     */
    public function setValidation($validation){
        $this->_validation = $validation;
    }


    /**
     * @param array $data
     *
     * @return User
     */
    public function createUser($data){

        $data = $this->_prepareData($data);

        if($this->_validate($data)){
            return $this->_create($data);
        }

        return null;
    }


    /**
     * @param  string $token
     * @return User
     */
    public function activateUser($token){

        $token = $this->_loadToken($token, ActionToken::ACTION_ACTIVATE);

        if($token){
            $user = $token->getRelated('User');
            if( $user ){
                $user->activate();
            }

            $token->delete();

            return $user;
        }

        return null;
    }


    /**
     * @param  User  $user
     * @param  string $password
     * @param  string $token
     * @return bool
     */
    public function recoverPassword($user, $password, $token){
        $token = $this->_loadToken($token, ActionToken::ACTION_RECOVER_PASS);

        if($token && $token->user_id == $user->id){
            $user->password = $this->_hasher->passwordHash($password);
            if($user->save()){
                $token->delete();

                return true;
            };
        }

        return false;
    }


    /**
     * @param  User $user
     * @return string
     */
    public function generateActivationToken($user){
        if(!$user || !$user->id){
            return null;
        }

        return $this->_generateToken($user->id, ActionToken::ACTION_ACTIVATE, $this->_activationTokenLifeTime);
    }


    /**
     * @param  User $user
     * @return string
     */
    public function generatePasswordRecoveryToken($user){
        if(!$user || !$user->id){
            return null;
        }

        return $this->_generateToken($user->id, ActionToken::ACTION_RECOVER_PASS, $this->_passwordRecoveryTokenLifeTime);
    }


    /**
     * @return Message\Group
     */
    public function getMessages(){
        return $this->_errorMessages;
    }


    /**
     * @param  array $data
     * @return mixed
     */
    protected function _prepareData($data){
        if(!array_key_exists('login', $data) || !$data['login']){
            $data['login'] = $data['email'];
        }

        $data['status'] = User::STATUS_NOT_VERIFIED;

        return $data;
    }


    /**
     * @param $data
     * @return bool
     */
    protected function _validate($data){
        $this->_errorMessages = $this->_validation->validate($data);

        return count($this->_errorMessages) == 0;
    }


    /**
     * @param  array $data
     * @return User
     */
    protected function _create($data){
        $user = new User();
        $data['password'] = $this->_hasher->passwordHash( $data['password']);
        if($user->create($data)){
            return $user;
        }

        $this->_errorMessages->appendMessages($user->getMessages());

        return null;
    }

    /**
     * @param  int $userId
     * @param  int $action
     * @param  int $lifetime
     * @return string
     */
    protected function _generateToken($userId, $action, $lifetime){
        $token            = new ActionToken();
        $token->user_id   = $userId;
        $token->expiredAt = date('Y-m-d H:i:s', time() + $lifetime);
        $token->action    = $action;
        $token->token     = $this->_hasher->shortTokenHash( $userId );

        if($token->save()){
            return $token->token;
        }

        return null;
    }


    /**
     * @param string $token
     * @param  int   $action
     * @return ActionToken
     */
    protected function _loadToken($token, $action){

        /** @var ActionToken $token */
        return ActionToken::findActive($token, $action);
    }
} 