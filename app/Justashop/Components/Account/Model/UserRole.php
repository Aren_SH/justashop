<?php

namespace Justashop\Components\Account\Model;

use Phalcon\Mvc\Model;

class UserRole extends Model{

    /** @var int */
    public $role_id;

    public $user_id;


    /**
     * @return string
     */
    public function getSource(){
        return 'users_roles';
    }


    public static function getClass(){
        return __CLASS__;
    }
} 