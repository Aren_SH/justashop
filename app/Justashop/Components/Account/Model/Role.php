<?php

namespace Justashop\Components\Account\Model;

use Phalcon\Mvc\Model;

class Role extends Model{

    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var string */
    public $description;


    /**
     * @return string
     */
    public function getSource(){
        return 'roles';
    }


    public static function getClass(){
        return __CLASS__;
    }
} 