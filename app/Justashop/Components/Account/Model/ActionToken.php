<?php

namespace Justashop\Components\Account\Model;

use Phalcon\Db\Column;
use Phalcon\Mvc\Model;

class ActionToken extends Model {

    const ACTION_ACTIVATE     = 1;

    const ACTION_AUTHENTICATE = 2;

    const ACTION_RECOVER_PASS = 3;


    /** @var int */
    public $user_id;

    /** @var string */
    public $token;

    /** @var int */
    public $expiredAt;

    /** @var int */
    public $action;


    protected static $_table = 'action_tokens';


    public static function getClass(){
        return __CLASS__;
    }


    /**
     * @return string
     */
    public function getSource(){
        return self::$_table;
    }

    public function initialize(){
        $this->belongsTo('user_id', User::getClass(), 'id',
            ['alias' => 'user']
        );
    }


    public static function findUser($token, $action = null){
        $conditions = ["token = :token:", "expiredAt > :expiredAt:"];
        $bindings   = ['token' => $token, 'expiredAt' => date('Y-m-d H:i:s', time())];
        $bindTypes  = ['token' => Column::BIND_PARAM_STR, 'expiredAt' => Column::BIND_PARAM_STR];

        if($action){
            $conditions[]        = "action = :action:";
            $bindings['action']  = $action;
            $bindTypes['action'] = Column::BIND_PARAM_INT;
        }


        $builder = new Model\Query\Builder();
        $builder
            ->columns('users.*')
            ->addFrom(__CLASS__, 'tokens')
            ->addFrom(User::getClass(), 'users')
            ->where(join(' AND ', $conditions))
            ->andWhere('tokens.user_id = users.id');

        /** @var Model\Resultset\Simple $result */
        $result = $builder->getQuery()->setBindParams($bindings)->setBindTypes($bindTypes)->execute();

        $user = null;
        if( $result->count() > 0){
            $user = $result->getFirst();
        }

        return $user;
    }

    /**
     * @param  string $token
     * @param  int    $action
     *
     * @return ActionToken
     */
    public static function findActive($token, $action = null){
        $conditions = ["token = :token:", "expiredAt > :expiredAt:"];
        $bindings   = ['token' => $token, 'expiredAt' => date('Y-m-d H:i:s', time())];
        $bindTypes  = ['token' => Column::BIND_PARAM_STR, 'expiredAt' => Column::BIND_PARAM_STR];

        if($action){
            $conditions[]        = "action = :action:";
            $bindings['action']  = $action;
            $bindTypes['action'] = Column::BIND_PARAM_INT;
        }

        return self::findFirst([
            'conditions' => join(' AND ', $conditions),
            'bind'       => $bindings,
            'bindTypes'  => $bindTypes
        ]);
    }


    /**
     * @param  int $userId
     * @param  int $action
     * @return mixed
     */
    public static function deleteByUserId($userId, $action = null){
        $conditions = ["user_id = :user_id:"];
        $bindings   = ['user_id' => (int)$userId];
        $bindTypes  = ['user_id' => Column::BIND_PARAM_INT];

        if($action){
            $conditions[]        = "action = :action:";
            $bindings['action']  = (int)$action;
            $bindTypes['action'] = Column::BIND_PARAM_INT;
        }


        $queryString = sprintf( 'DELETE FROM %s WHERE %s', __CLASS__, join(' AND ', $conditions));

        $query = new Model\Query( $queryString);
        $query->setDI(\Phalcon\DI\FactoryDefault::getDefault());

        return $query->setBindParams($bindings)->setBindTypes($bindTypes)->execute();

    }
}