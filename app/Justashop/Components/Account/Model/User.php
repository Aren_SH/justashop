<?php

namespace Justashop\Components\Account\Model;


use Phalcon\Db\Column;
use Phalcon\Mvc\Model;

class User extends Model{


    const STATUS_DISABLED = 0;

    const STATUS_ACTIVE   = 1;

    const STATUS_NOT_VERIFIED = 2;

    /** @var int */
    public $id;

    /** @var string */
    public $login;

    /** @var string */
    public $password;

    /** @var string */
    public $email;

    /** @var string */
    public $createdAt;

    /** @var int */
    public $status;


    public static function findById($id){
        $user = self::findFirst([
            'conditions' => 'id = :id:',
            'bind'       => ['id' => $id ],
            'bindTypes'  => ['id' => Column::BIND_PARAM_INT],
        ]);

        return $user ? $user : null;
    }


    public static function getClass(){
        return __CLASS__;
    }


    public function isActive(){
        return $this->status === self::STATUS_ACTIVE;
    }


    public function isNotVerified(){
        return $this->status === self::STATUS_NOT_VERIFIED;
    }

    /**
     * Set status to active.
     *
     * @param  bool $save
     * @return bool
     */
    public function activate($save = true){

        $this->status = self::STATUS_ACTIVE;
        if($save){
            return $this->save();
        }

        return true;
    }


    public function hasRole($roleId){
        foreach( $this->roles as $userRole){
            if( $userRole->id == $roleId ){
                return true;
            }
        }

        return false;
    }


    /**
     * @return string
     */
    public function getSource(){
        return 'users';
    }


    public function initialize(){
        $this->_setBehaviors();
        $this->_setRelations();
    }


    protected function _setRelations(){
        $this->hasManyToMany('id', UserRole::getClass(), 'user_id', 'role_id', Role::getClass(), 'id', ['alias' => 'roles']);
    }


    protected function _setBehaviors(){

        $this->addBehavior(new Model\Behavior\Timestampable([
                'beforeValidationOnCreate' => [
                    'field'  => 'createdAt',
                    'format' => 'Y-m-d H:i:s'
                ]
            ]
        ));
    }
} 