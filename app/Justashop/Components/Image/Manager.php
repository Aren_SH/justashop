<?php


namespace Justashop\Components\Image;


use Justashop\Components\Image\Model\Image;
use Phalcon\Config;
use Phalcon\Http\Request\FileInterface;
use Phalcon\Mvc\User\Component;
use Phalcon\Validation\Message\Group;
use Phalcon\Validation\Message;

class Manager extends Component {

    const PROFILE_PARAM_DIR    = 'directory';
    const PROFILE_PARAM_SIZES  = 'sizes';
    const PROFILE_PARAM_WIDTH  = 'width';
    const PROFILE_PARAM_HEIGHT = 'height';
    const PROFILE_PARAM_RATIO  = 'aspectRatio';
    const PROFILE_PARAM_ID     = 'id';

    /** @var Config */
    protected $_profiles;

    /** @var string */
    protected $_rootDir;

    /** @var string */
    protected $_baseUrl;

    /** @var Group */
    protected $_errorMessages;


    public function __construct(){
        $this->_errorMessages = new Group();
    }


    /**
     * @param Config $profiles
     */
    public function setProfiles($profiles){
        $this->_profiles = $profiles;
    }


    /**
     * @return Config|null
     */
    public function getProfiles(){
        return $this->_profiles;
    }


    /**
     * @param  string $profile
     * @return bool
     */
    public function hasProfile($profile){
        return $this->_profiles->offsetExists($profile);
    }


    public function getProfileNameById($id){
        $name = null;
        foreach( $this->_profiles as $key => $profile){
            if($profile['id'] == $id){
                $name = $key;
                break;
            }
        }

        return $name;
    }


    /**
     * @return bool
     */
    public function validateProfiles(){
        if(!$this->_profiles){
            $this->_errorMessages->appendMessage(new Message('Profiles not loaded'));
            return false;
        }

        $ids = [];
        foreach( $this->_profiles as $profile => $settings){
            /** @var Config $settings */


            $id = $settings->get(self::PROFILE_PARAM_ID, false);
            if(!$id){
                $this->_errorMessages->appendMessage(
                    new Message(sprintf('Directory not set for profile %s', $profile))
                );
            }
            if( in_array($id, $ids)){
                $this->_errorMessages->appendMessage(
                    new Message(sprintf('ID not unique for profile %s', $profile))
                );
            }
            $ids[] = $id;

            if(!$settings->get(self::PROFILE_PARAM_DIR, false)){
                $this->_errorMessages->appendMessage(
                    new Message(sprintf('Directory not set for profile %s', $profile))
                );
            }

            if(!$settings->offsetExists(self::PROFILE_PARAM_SIZES)){
                continue;
            }

            $sizes = $settings->get(self::PROFILE_PARAM_SIZES);
            foreach($sizes as $size => $sizeSettings){
                /** @var Config $sizeSettings */
                if( !$sizeSettings->get(self::PROFILE_PARAM_WIDTH) && !$sizeSettings->get(self::PROFILE_PARAM_HEIGHT)){
                    $this->_errorMessages->appendMessage(
                        new Message(sprintf(
                            'Error in profile %s size %s: either width or height must be set',
                            $profile,
                            $size
                        ))
                    );
                }
            }
        }


        return count($this->_errorMessages) == 0;
    }


    /**
     * @param string $dir
     */
    public function setRootDir($dir){
        $this->_rootDir = $dir;
    }


    /**
     * @param string $url
     */
    public function setBaseUrl($url){
        $this->_baseUrl = $url;
    }



    /**
     * @param  string   $profile
     * @return Image[]
     */
    public function uploadFromRequest($profile){
        if(!$this->request->hasFiles()) return [];

        if(!$this->hasProfile($profile)){
            $this->_errorMessages->appendMessage(
                new Message(sprintf('Profile %s not found', $profile))
            );

            return [];
        }

        $files = $this->request->getUploadedFiles();

        $result = [];
        foreach($files as $file){
            $result[] = $this->_processFile($file, $profile);
        }

        return $result;
    }


    /**
     * @param  string $name
     * @param  string $profile
     * @param  string $size
     * @return string
     */
    public function getFilePath($name, $profile, $size = null){
        return $this->_getProfileDir($profile) . join( DIRECTORY_SEPARATOR, array_filter([$size, $name]) );
    }

    /**
     * @param  string $name
     * @param  string $profile
     * @param  string $size
     * @return string
     */
    public function getFileUrl($name, $profile, $size = null){
        if(!$this->hasProfile($profile) || !$this->_profiles->get($profile)->offsetExists(self::PROFILE_PARAM_DIR)){
            return '';
        }
        $profileDir = $this->_profiles[$profile][self::PROFILE_PARAM_DIR];

        $profileDir = str_replace(DIRECTORY_SEPARATOR, '/', $profileDir);

        return $this->_baseUrl . join( '/', array_filter([$profileDir, $size, $name]));
    }


    /**
     * @param string $filePath
     */
    protected function _createFilePathDirs($filePath){
        //remove rootDirectory
        $path  = str_replace($this->_rootDir, '', $filePath);
        $parts = explode( DIRECTORY_SEPARATOR, trim($path, DIRECTORY_SEPARATOR));

        //remove file
        array_pop($parts);

        $newPath = rtrim($this->_rootDir, DIRECTORY_SEPARATOR);
        foreach( $parts as $part){
            $newPath .= DIRECTORY_SEPARATOR . $part;

            if(is_dir($newPath)) continue;

            mkdir($newPath, 0775);
        }
    }


    /**
     * @param  string $fileName
     * @param  string $profile
     * @return string
     */
    protected function _getCorrectFileName($fileName, $profile){
        $dotPos = strrpos($fileName, '.');
        $name   = substr($fileName, 0, $dotPos);
        $ext    = substr($fileName, $dotPos);

        $suffix = '';
        $iter   = 1;

        $newName = sprintf('%s%s%s', $name, $suffix, $ext);
        while(file_exists($this->getFilePath($newName, $profile))){
            $suffix = $iter;
            $iter++;

            $newName = sprintf('%s%s%s', $name, $suffix, $ext);
        }

        return $newName;

    }


    /**
     * @param  string $profile
     * @return string
     */
    protected function _getProfileDir($profile){
        if(!$this->hasProfile($profile) || !$this->_profiles->get($profile)->offsetExists(self::PROFILE_PARAM_DIR)){
            return '';
        }

        return $this->_rootDir . $this->_profiles[$profile][self::PROFILE_PARAM_DIR] . DIRECTORY_SEPARATOR;
    }


    /**
     * @param  FileInterface $file
     * @param  string        $profile
     *
     * @return Model\Image
     */
    protected function _processFile($file, $profile){
        $profileId = $this->_profiles[$profile]['id'];
        $hash      = $this->_hash($file->getTempName());
        $image     = Image::findByHash($hash, $profileId);

        if($image){
            return $image;
        }

        $filePath = $this->getFilePath( $this->_getCorrectFileName($file->getName(), $profile), $profile);
        $this->_createFilePathDirs($filePath);
        $moved = move_uploaded_file($file->getTempName(), $filePath);
        if( !$moved ){
            $this->_errorMessages->appendMessage(
                new Message(sprintf("Cannot move %s to %s", $file->getName(), $filePath))
            );

            return null;
        }

        $sizes = $this->_getImageSizes($file->getName(), $profile);

        $image = new Image();
        $image->file       = $file->getName();
        $image->hash       = $hash;
        $image->sizes      = array_keys($sizes);
        $image->status     = 1;
        $image->profile_id = $profileId;

        if( $image->save() ){
            return $image;
        }
        else {
            $this->_errorMessages->appendMessages( $image->getMessages() );
            return null;
        }
    }


    /**
     * @param  string $fileName
     * @param  string $profile
     * @return array
     */
    protected function _getImageSizes($fileName, $profile){
        $resultSizes = [];

        $filePath  = $this->getFilePath($fileName, $profile);
        $imageType = $this->_getImageType($filePath);

        $imageSizes = getimagesize($filePath);
        if( !$imageSizes || count($imageSizes) < 2 ){
            $this->_errorMessages->appendMessage(
                new Message(sprintf("Cannot get size of %s", $fileName))
            );

            return null;
        }

        $originalWidth  = $imageSizes[0];
        $originalHeight = $imageSizes[1];

        try {
            $srcImage   = $this->_loadImage($imageType, $filePath);
        }
        catch(\Exception $e){
            $this->_errorMessages->appendMessage(new Message($e->getMessage()));
            return $resultSizes;
        }

        $sizes = $this->_getProfileSizes($profile);

        foreach( $sizes as $size => $settings ){
            $sizeImage = $this->_resizeImage($settings, $srcImage, $originalWidth, $originalHeight);
            $sizePath  = $this->getFilePath($fileName, $profile, $size);

            $this->_createFilePathDirs($sizePath);

            try {
                $this->_saveImage($imageType, $sizePath, $sizeImage);
                $resultSizes[$size] = $sizePath;

            } catch(\Exception $e){
                $this->_errorMessages->appendMessage(new Message($e->getMessage()));
            }
        }

        return $resultSizes;
    }

    /**
     * @param Config   $settings
     * @param resource $srcImage
     * @param int      $originalWidth
     * @param int      $originalHeight
     * @return resource
     */
    protected function _resizeImage($settings, $srcImage, $originalWidth, $originalHeight){
        $originalRatio = $originalWidth / $originalHeight;
        $targetWidth  = $settings->get('width', null);
        $targetHeight = $settings->get('height', null);

        if( !$targetHeight && !$targetWidth ){
            return null;
        }

        if(!$targetWidth)  $targetWidth  = $targetHeight * $originalRatio;
        if(!$targetHeight) $targetHeight = $targetWidth / $originalRatio;

        $targetRatio = $targetWidth / $targetHeight;

        if( $originalRatio > $targetRatio ){
            $cropHeight  = $originalHeight;
            $cropWidth   = $originalHeight * $targetRatio;
            $deltaWidth  = ($originalWidth - $cropWidth) / 2;
            $deltaHeight = 0;
        }
        elseif( $originalRatio < $targetRatio ){
            $cropHeight  = $originalWidth / $targetRatio;
            $cropWidth   = $originalWidth;
            $deltaWidth  = 0;
            $deltaHeight = ($originalHeight - $cropHeight) / 2;
        }
        else{
            $cropHeight  = $originalHeight;
            $cropWidth   = $originalWidth;
            $deltaWidth  = 0;
            $deltaHeight = 0;
        }

        $sizeImage = imagecreatetruecolor($targetWidth, $targetHeight);

        imagecopyresampled(
            $sizeImage, $srcImage,
            0, 0, $deltaWidth, $deltaHeight,
            $targetWidth, $targetHeight, $cropWidth, $cropHeight
        );

        return $sizeImage;
    }


    /**
     * @param  string $file
     * @return int
     */
    protected function _getImageType($file){
        return exif_imagetype($file);
    }


    /**
     * @param  int    $type
     * @param  string $file
     * @return resource
     * @throws \Exception
     */
    protected function _loadImage($type, $file){
        if(!$type) return null;

        $image = null;
        switch($type){
            case IMAGETYPE_JPEG:
            case IMAGETYPE_JPEG2000:
                $image = imagecreatefromjpeg($file);
                break;
            case IMAGETYPE_PNG:
                $image = imagecreatefrompng($file);
                break;
            case IMAGETYPE_GIF:
                $image = imagecreatefromgif($file);
                break;
            default:
                throw new \Exception("Unsupported image type");
                break;
        }

        return $image;
    }


    /**
     * @param int      $type
     * @param string   $file
     * @param resource $image
     * @return bool
     * @throws \Exception
     */
    protected function _saveImage($type, $file, $image){
        $result = false;
        switch($type){
            case IMAGETYPE_JPEG:
            case IMAGETYPE_JPEG2000:
                $result = imagejpeg($image, $file);
                break;
            case IMAGETYPE_PNG:
                $result = imagepng($image, $file);
                break;
            case IMAGETYPE_GIF:
                $result = imagegif($image, $file);
                break;
            default:
                throw new \Exception("Unsupported image type");
                break;
        }

        if(!$result) throw new \Exception( sprintf("Failed to save image to file %s", $file) );

        return true;
    }



    /**
     * @param  string $profile
     * @return Config
     */
    protected function _getProfileSizes($profile){
        if(empty($this->_profiles[$profile]) || empty($this->_profiles[$profile]['sizes'])){
            return new Config();
        }

        return $this->_profiles[$profile]['sizes'];
    }

    /**
     * @param  string $file
     * @return string
     */
    protected function _hash($file){
        return hash_file('sha256', $file);
    }
} 