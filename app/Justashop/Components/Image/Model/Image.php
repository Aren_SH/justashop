<?php


namespace Justashop\Components\Image\Model;


use Justashop\Components\Image\Manager;
use Phalcon\Db\Column;
use Phalcon\Mvc\Model;

class Image extends Model{

    /** @var int */
    public $id;

    /** @var string */
    public $hash;

    /** @var string */
    public $file;

    /** @var array */
    public $sizes;

    /** @var int */
    public $status;

    /** @var int */
    public $profile_id;

    /** @var Manager */
    protected $_imageManager;

    /** @var string */
    protected $_profileName;

    public function onConstruct(){
        $di = $this->getDI();

        $imageManager = $di->getShared('imageManager');
        if(!$this->getImageManager() && $imageManager){
            $this->setImageManager($imageManager);
        }
    }


    public function setImageManager($imageManager){
        $this->_imageManager = $imageManager;
    }


    public function getImageManager(){
        return $this->_imageManager;
    }


    public function getSource(){
        return 'images';
    }


    /**
     * @param  string $size
     * @return string
     */
    public function getUrl($size = null){
        if(!$this->_imageManager || !$this->_profileName || !$this->file) return null;

        return $this->_imageManager->getFileUrl($this->file, $this->_profileName, $size);
    }


    public static function findByHash($hash, $profile_id){
        return self::findFirst([
            'conditions' => 'hash = :hash: AND profile_id = :profile_id:',
            'bind'       => ['hash' => $hash, 'profile_id' => $profile_id],
            'bindTypes'  => ['hash' => Column::BIND_PARAM_STR, 'profile_id' => Column::BIND_PARAM_INT]
        ]);
    }


    public function beforeSave(){
        $this->sizes = is_array($this->sizes) ? join('|', $this->sizes) : $this->sizes;
    }


    public function afterFetch(){
        $this->sizes = $this->sizes ? explode('|', $this->sizes) : [];

        if($this->_imageManager){
            $this->_profileName = $this->_imageManager->getProfileNameById($this->profile_id);
        }
    }



}