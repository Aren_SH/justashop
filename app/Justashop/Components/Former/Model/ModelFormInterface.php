<?php

namespace Justashop\Components\Former\Model;


Interface ModelFormInterface {


    /**
     * @param  mixed $model
     */
    public function setModel( $model );


    /**
     * @return mixed
     */
    public function getModel();


    /**
     * @return mixed
     */
    public function getMessages();

    /**
     * @return array
     */
    public function getFields();


    /**
     * @return array
     */
    public function getFilters();


    /**
     * @param  array
     * @return mixed
     */
    public function validate();


    public function save();
} 