<?php

namespace Justashop\Components\Former\Fields;


/**
 * Select field
 *
 * @author   Aren_SH
 * @package  Justashop
 * @category Former
 */
class Select extends Multiple {

    /** @var string  */
    protected $_tag     = 'select';

    /** @var string  */
    protected $_pattern = '<:tag: name=":name:" :attributes:>:options:</:tag:>';

    /**
     * Is field multiple
     * 
     * @var bool 
     */
    protected $_multiple = false;


    /**
     * Set or Get multiple attribute of select
     * 
     * @param  bool|null $value
     * @return mixed
     */
    public function setMultiple($value = NULL)
    {
        $this->_multiple = (bool) $value;

        if ($this->_multiple)
        {
            $this->setAttribute('multiple', 'multiple');
        }
        else
        {
            unset($this->_attributes['multiple']);
        }


        return $this;
    }


    /**
     * @return bool
     */
    public function isMultiple(){
        return $this->_multiple;
    }


    /**
     * @return array
     */
    protected function _getRenderData(){
        $data = parent::_getRenderData();
        $data['options'] = $this->_getRenderedOptions();

        return $data;
    }


    protected function _getRenderedOptions(){
        $optPattern = '<option value="%s" %s>%s</option>';
        $optHtml    = '';

        $options    = $this->getOptions();
        foreach( $options as $value => $title ){
            $selected = in_array( $value, $this->_selected ) ? 'selected="selected"' : null;
            $optHtml .= sprintf( $optPattern, $value, $selected, $title );
        }

        return $optHtml;
    }

    /**
     * Sugar function set attribute size
     * 
     * @param  string $value
     * @return Select
     */
    public function setSize($value = NULL)
    {
        return $this->setAttribute('size', (int) $value);
    }

}