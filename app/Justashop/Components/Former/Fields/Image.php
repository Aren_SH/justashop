<?php

namespace Justashop\Components\Former\Fields;


class Image extends Multiple {


    public function __construct($name)
    {
        parent::__construct($name);
        $this->_viewFile = 'fields' . DIRECTORY_SEPARATOR . 'images.phtml';
    }


    protected function _renderPattern(){
        throw new \Exception( "View file for Images field is missing" );
    }

} 