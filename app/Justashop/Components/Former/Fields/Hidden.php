<?php

namespace Justashop\Components\Former\Fields;

use \Justashop\Components\Former\Field;

/**
 * Hidden field
 *
 * @author   Aren_SH
 * @package  Justashop
 * @category Former
 */
class Hidden extends Input {


    public function __construct($name)
    {
        parent::__construct($name);

        $this->setAttribute('type', 'hidden');
    }


    /**
     * @param null $value
     * @return mixed|null
     */
    public function setType($value = null)
    {
        return null;
    }

}