<?php

namespace Justashop\Components\Former\Fields;

use \Justashop\Components\Former\Field;

use \Phalcon\Tag;

/**
 * Input field
 *
 * @author   Aren_SH
 * @package  Justashop
 * @category Former
 */
class Input extends Field {

    protected $_tag = 'input';

    /**
     * Field value
     *
     * @var mixed
     */
    protected $_value;

    public function __construct($name)
    {
        parent::__construct($name);

        $this->setType('text');
    }


    /**
     * Set or Gets type of Input field
     * 
     * @param  mixed $value
     * @return mixed
     */
    public function setType($value = NULL)
    {
        return $this->setAttribute('type', $value);
    }


    /**
     * Set or Get value
     *
     * @param  mixed $value
     * @return Checkbox
     */
    public function setValue($value = NULL)
    {
        $this->_value = $value;

        return $this;
    }


    /**
     * @return mixed
     */
    public function getValue(){
        return $this->_value;
    }


    /**
     * Sugar function set attribute pattern
     * 
     * @param  string $regexp
     * @return string
     */
    public function setPattern($regexp)
    {
        return $this->setAttribute('pattern', $regexp);
    }


    /**
     * @return mixed
     */
    public function getPattern(){
        return $this->getAttribute('pattern');
    }


    /**
     * Sugar function set attribute placeholder
     * 
     * @param  string $placeholder
     * @return string
     */
    public function setPlaceholder($placeholder)
    {
        return $this->setAttribute('placeholder', $placeholder);
    }


    /**
     * @return mixed
     */
    public function getPlaceholder(){
        return $this->getAttribute('placeholder');
    }


}