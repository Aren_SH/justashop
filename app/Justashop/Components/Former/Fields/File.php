<?php

namespace Justashop\Components\Former\Fields;

use Phalcon\Exception;


/**
 * File field
 *
 * @author   Aren_SH
 * @package  Justashop
 * @category Former
 * @category Helpers
 */
class File extends Input {

    /** @var bool  */
    protected $_multiple = false;

    public function __construct($name)
    {
        parent::__construct($name);

        $this->setType('file');
    }

    /**
     * @return void
     * @throws \Phalcon\Exception
     */
    public function value()
    {
        throw new Exception("File field has no attribute 'value'");
    }

    /**
     * Sugar function set attribute multiple
     * 
     * @param  bool $value
     * @return File
     */
    public function setMultiple($value = NULL)
    {
        $this->_multiple = (bool) $value;

        if ($this->_multiple)
        {
            $this->setAttribute('multiple', 'multiple');
        }
        else
        {
            unset($this->_attributes['multiple']);
        }


        return $this;
    }

    public function isMultiple(){
        return $this->_multiple;
    }

    /**
     * Sugar function set attribute accepted
     * 
     * @param  string $value coma separated mime-types
     * @return File
     */
    public function setAccepted($value)
    {
        return $this->setAttribute('accepted', $value);
    }

}