<?php

namespace Justashop\Components\Former\Fields;

use Justashop\Components\Former\Field;
use Phalcon\Exception;

/**
 * Base class for all multi value fields
 *
 * @author Aren_SH
 * @package Rokku
 * @category Former
 * @category Helpers
 */
abstract class Multiple extends Field {

    /**
     * Select field options
     * 
     * @var array|\Justashop\Components\Former\Sources\MultipleInterface;
     */
    protected $_options = [];

    /**
     * $key of selected item
     * 
     * @var mixed 
     */
    protected $_selected;

    /**
     * return options as array ($value => $label)
     * 
     * @return array
     */
    public function getOptions()
    {
        if (is_array($this->_options))
        {
            return $this->_options;
        }

        if (is_object($this->_options))
        {
            return $this->_options->getFieldOptions();
        }

        return [];
    }


    /**
     * @param null $values
     * @return $this
     * @throws Exception
     */
    public function setOptions($values = NULL)
    {
        if (is_array($values))
        {
            $this->_options = $values;
        }
        elseif (is_object($values))
        {
            $interfaces = class_implements($values);

            if (in_array('\Former\Sources\MultipleInterface', $interfaces))
            {
                $this->_options = $values;
            }
            else
            {
                throw new Exception('\Former\Source\MultipleInterface interface must be implement by object used as value for' . __CLASS__);
            }
        }
        else
        {
            throw new Exception('Given value is not array and not implement \Former\Source\MultipleInterface interface');
        }

        return $this;
    }

    /**
     * Set or Get selected option key
     * 
     * @param  mixed $value
     * @return mixed
     */
    public function setSelected($value = NULL)
    {
        $this->_selected = is_array( $value ) ? $value : [$value];

        return $this;
    }


    /**
     * @return array
     */
    public function getSelected(){
        return $this->_selected ?: [];
    }

}