<?php

namespace Justashop\Components\Former\Fields;


class ObjectSelect extends Select {

    protected $_titleField;

    protected $_attributeFields = [];


    public function getTitleField(){
        return $this->_titleField;
    }

    public function setTitleField($title){
        $this->_titleField = $title;
        return $this;
    }

    public function getAttributeFields(){
        return $this->_attributeFields;
    }

    public function setAttributeFields($attrs){
        $this->_attributeFields = $attrs;
        return $this;
    }



    /**
     * @return array
     */
    protected function _getRenderedOptions(){

        $optPattern = '<option value="%s" %s %s>%s</option>';
        $optHtml    = '';

        $options    = $this->getOptions();
        foreach( $options as $value => $object ){
            $selected = in_array( $value, $this->_selected ) ? 'selected="selected"' : null;
            $title    = $object->{$this->getTitleField()};
            $attrs    = $this->getOptionAttributes( $object );
            $optHtml .= sprintf( $optPattern, $value, $selected, $attrs, $title );
        }

        return $optHtml;
    }


    protected function getOptionAttributes($object){
        $attrs = $this->getAttributeFields();

        $html = '';

        foreach( $attrs as $attr ){
            $html .= sprintf( 'data-%s="%s"', $attr, htmlspecialchars($object->{$attr}));
        }

        return $html;
    }
} 