<?php

namespace Justashop\Components\Former\Fields;

use Justashop\Components\Former\Field;

/**
 * Checkbox field
 *
 * @author   Aren_SH
 * @package  Justashop
 * @category Former
 */
class Checkbox extends Field {

    /** @var string */
    protected $_tag     = 'input';

    /** @var bool */
    protected $_checked = false;


    public function __construct($name)
    {
        parent::__construct($name);

        $this->setAttribute('type', 'checkbox');
    }


    /**
     * Set or Get value
     * 
     * @param  mixed $value
     * @return Checkbox
     */
    public function setValue($value = NULL)
    {
        $this->_value = $value;

        return $this;
    }


    /**
     * @return mixed
     */
    public function getValue(){
        return $this->_value;
    }


    /**
     * Set checked attribute
     * 
     * @param mixed $value
     * @return mixed chaining
     */
    public function setChecked($value)
    {
        $this->_checked = (bool) $value;

        if ($this->_checked)
        {
            $this->setAttribute('checked', 'checked');
        }
        else
        {
            unset($this->_attributes['checked']);
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function isChecked(){
        return $this->_checked;
    }
}