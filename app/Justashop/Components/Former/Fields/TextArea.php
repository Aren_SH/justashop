<?php

namespace Justashop\Components\Former\Fields;

use \Justashop\Components\Former\Field;

/**
 * Textarea field
 *
 * @author   Aren_SH
 * @package  Justashop
 * @category Former
 */
class TextArea extends Field {


    /** @var string  */
    protected $_tag     = 'textarea';

    /** @var string  */
    protected $_pattern = '<:tag: name=":name:" :attributes:>:value:</:tag:>';


    protected $_isHtml = false;

    /**
     * Set or Get value
     * 
     * @param  mixed $value
     * @return TextArea
     */
    public function setValue($value)
    {
        $this->_value = $value;

        return $this;
    }


    /**
     * @return mixed
     */
    public function getValue(){
        return $this->_value;
    }

    /**
     * Sugar function set attribute maxlength
     * 
     * @param string $value
     * @return TextArea
     */
    public function setMax($value = NULL)
    {
        return $this->setAttribute('maxlength', (int) $value);
    }

    /**
     * Sugar function set attribute placeholder
     * 
     * @param string $value
     * @return TextArea
     */
    public function setPlaceholder($value = NULL)
    {
        return $this->setAttribute('placeholder', $value);
    }

    /**
     * Sugar function set attribute placeholder
     * 
     * @param  string $value
     * @return TextArea
     */
    public function setRows($value = NULL)
    {
        return $this->setAttribute('rows', $value);
    }

    /**
     * if is_html set to false - text will be processed with htmlspecialchars
     * 
     * @param  bool $value
     * @return TextArea
     */
    public function isHtml($value = TRUE)
    {
        $this->_is_html = (bool) $value;
        
        return $this;
    }

    public function renderValue()
    {
        return !$this->_isHtml ? htmlspecialchars($this->_value) : $this->_value;
    }

}