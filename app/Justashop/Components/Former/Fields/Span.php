<?php


namespace Justashop\Components\Former\Fields;


use Justashop\Components\Former\Field;

class Span extends Field {

    protected $_tag = 'span';

    protected $_pattern = '<:tag: :attributes: > :value:<:tag:>';


    /**
     * Set or Get value
     *
     * @param  mixed $value
     * @return Checkbox
     */
    public function setValue($value = NULL)
    {
        $this->_value = $value;

        return $this;
    }


    /**
     * @return mixed
     */
    public function getValue(){
        return $this->_value;
    }
} 