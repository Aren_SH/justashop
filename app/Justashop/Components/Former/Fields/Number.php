<?php

namespace Justashop\Components\Former\Fields;

use Phalcon\Exception;

/**
 * Number field
 *
 * @author Aren_SH
 * @package Rokku
 * @category Former
 * @category Helpers
 */
class Number extends Input {


    public function __construct($name)
    {
        parent::__construct($name);

        $this->setType('number');

        //digits, +, -, and dot
        $this->setPattern('[-+]?([0-9]*\.[0-9]+|[0-9]+)');
    }

    /**
     * @param  int|float|string $value
     * @return Number
     * @throws \Phalcon\Exception
     */
    public function setValue($value = NULL)
    {
        if (!is_numeric($value) && $value !== NULL)
        {
            throw new Exception('Value of number field must be numeric');
        }

        return parent::setValue($value);
    }

    /**
     * Sugar function set attribute min
     * 
     * @param  string $value
     * @return Number
     */
    public function setMin($value)
    {
        return $this->setAttribute('min', $value);
    }

    /**
     * Sugar function set attribute max
     * 
     * @param  string $value
     * @return Number
     */
    public function setMax($value)
    {
        return $this->setAttribute('max', $value);
    }

    /**
     * Sugar function set attribute step
     * 
     * @param  string $value
     * @return Number
     */
    public function setStep($value)
    {
        return $this->setAttribute('step', $value);
    }

}