<?php

namespace Justashop\Components\Former\Fields;

use Phalcon\Exception;

/**
 * MultiCheckbox class
 *
 * @author   Aren_SH
 * @package  Justashop
 * @category Helpers
 */
class MultiCheckbox extends Multiple {

    protected $_viewFile = 'multicheckbox';

    public function __construct($name)
    {
        parent::__construct($name);

        $this->setAttribute('type', 'checkbox');
    }


    protected function _renderPattern(){
        throw new Exception( "View file for MultiCheckbox field is missing" );
    }

}