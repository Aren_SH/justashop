<?php

namespace Justashop\Components\Former;

use \Justashop\Components\Former\Fields;

/**
 * Render forms and fields from object
 *
 * @author   Aren_SH
 * @package  Justashop
 * @category Former
 */
class Builder {

    /** @var string */
    protected $_viewsPath;

    /**
     * @var array
     */
    protected $_mapping = [
        'input'         => '_input',
        'hidden'        => '_hidden',
        'disabled'      => '_disabled',
        'file'          => '_file',
        'select'        => '_select',
        'objectSelect'  => '_objectSelect',
        'textarea'      => '_textArea',
        'checkbox'      => '_checkbox',
        'multicheckbox' => '_multiCheckbox',
        'image'         => '_image',
        'span'          => '_span',
    ];




    /**
     * @param  $type
     * @param  $name
     * @return Fields\Input|Fields\Select|Field
     */
    public function field( $type, $name ){
        $method = !empty( $this->_mapping[$type] ) ? $this->_mapping[$type] : null;

        $field = null;
        if( $method ){
            /** @var Field $field */
            $field = $this->{$method}($name);
            $field->setViewsPath( $this->_viewsPath );
        }

        return $field;
    }

    /**
     * @param  Form $form
     * @return FormRenderer
     */
    public function renderer( $form ){
        $renderer = new FormRenderer( $form );
        if( count( $form->getCollections() ) > 1){
            $renderer->setWithTabs( true );
        }
        $renderer->setViewsPath( $this->_viewsPath );

        return $renderer;
    }


    public function filterForm(){}


    /**
     * @param string $path
     */
    public function setViewsPath( $path ){
        $this->_viewsPath = $path;
    }


    /**
     * @return string
     */
    public function getViewsPath(){
        return $this->_viewsPath;
    }


    protected function _input($name)
    {
        return new Fields\Input($name);
    }

    /**
     * Short for \Justashop\Components\Former\Fields\Hidden($name)
     * 
     * @param string $name
     * @return \Justashop\Components\Former\Fields\Hidden
     */
    protected function _hidden($name)
    {
        return new Fields\Hidden($name);
    }

    /**
     * Short for \Justashop\Components\Former\Fields\TextArea($name)
     * 
     * @param string $name
     * @return \Justashop\Components\Former\Fields\Textarea
     */
    protected function _textArea($name)
    {
        return new Fields\TextArea($name);
    }

    /**
     * Short for \Justashop\Components\Former\Fields\Select($name)
     * 
     * @param string $name
     * @return \Justashop\Components\Former\Fields\Select
     */
    protected function _select($name)
    {
        return new Fields\Select($name);
    }

    /**
     * Short for \Justashop\Components\Former\Fields\Select($name)
     *
     * @param string $name
     * @return \Justashop\Components\Former\Fields\ObjectSelect
     */
    protected function _objectSelect($name)
    {
        return new Fields\ObjectSelect($name);
    }

    /**
     * Short for \Justashop\Components\Former\Fields\Checkbox($name)
     * 
     * @param  string $name
     * @return \Justashop\Components\Former\Fields\Checkbox
     */
    protected function _checkbox($name)
    {
        return new Fields\Checkbox($name);
    }

    /**
     * Short for \Justashop\Components\Former\Fields\MultiCheckbox($name)
     * 
     * @param string $name
     * @return \Justashop\Components\Former\Fields\MultiCheckbox
     */
    protected function _multiCheckbox($name)
    {
        return new Fields\MultiCheckbox($name);
    }

    /**
     * Short for \Justashop\Components\Former\Fields\Input($name)->attribute('readonly', true);
     * 
     * @param  string $name
     * @return \Justashop\Components\Former\Fields\Input
     */
    protected function _disabled($name)
    {
        $field = new Fields\Input($name);
        return $field->setAttribute('readonly', true);
    }

    /**
     * Short for \Justashop\Components\Former\Fields\File($name);
     * 
     * @param  string $name
     * @return \Justashop\Components\Former\Fields\File
     */
    protected function _file($name)
    {
        return new Fields\File($name);
    }


    /**
     * Short for \Justashop\Components\Former\Fields\Image($name);
     *
     * @param  string $name
     * @return \Justashop\Components\Former\Fields\Image
     */
    protected function _image($name)
    {
        return new Fields\Image($name);
    }


    /**
     * Short for \Justashop\Components\Former\Fields\Span($name);
     *
     * @param  string $name
     * @return \Justashop\Components\Former\Fields\Span
     */
    protected function _span($name)
    {
        return new Fields\Span($name);
    }

}
