<?php

namespace Justashop\Components\Former;


class FieldCollection {

    protected $_title;

    protected $_description;

    protected $_attributes = [];

    protected $_fields;



    public function getTitle(){
        return $this->_title;
    }

    public function setTitle($title){
        $this->_title = $title;
    }


    /**
     * Set or Get attributes values
     *
     * @param array $values
     * @return array
     */
    public function setAttributes($values = null){
        $this->_attributes = array_merge($this->_attributes, $values);
    }


    /**
     * @return array
     */
    public function getAttributes(){
        return $this->_attributes;
    }

    /**
     * Return attribute by key
     * Or set attribute value
     *
     * @param string $key
     * @param mixed  $value
     * @return FieldCollection
     */
    public function setAttribute($key, $value = null){
        $this->_attributes[$key] = $value;

        return $this;
    }


    public function hasField($key){
        return array_key_exists($key, $this->_fields);
    }

    public function addField($key, $field){
        $this->_fields[$key] = $field;
        return $this;
    }


    public function getField($key){
        return $this->hasField( $key ) ? $this->_fields[$key] : null;
    }


    public function getFields(){
        return $this->_fields;
    }
} 