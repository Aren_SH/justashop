<?php

namespace Justashop\Components\Former\Sources;

/**
 * Interface for objects, that can be used as select options
 *
 * @author   Aren_SH
 * @package  Justashop
 * @category Former
 */

interface MultipleInterface {
    
    /**
     * Return options for select field as array ($value => $title)
     *
     * @return array
     */
    public function getFieldOptions();
}