<?php

namespace Justashop\Components\Former;

/**
 * Base field object
 *
 * @author   Aren_SH
 * @package  Justashop
 * @category Former
 */
class Field{

    /**
     * Field type
     *
     * @var string
     */
    protected $_tag;

    /**
     * Field name
     *
     * @var string
     */
    protected $_name;

    /**
     * Additional attributes of field (id, class, etc)
     *
     * @var array
     */
    protected $_attributes = [];

    /**
     * Path to field views
     *
     * @var string
     */
    protected $_viewsPath = '';

    /**
     * View file
     *
     * @var string
     */
    protected $_viewFile = '';


    protected $_pattern = '<:tag: name=":name:" value=":value:" :attributes: />';

    /** @var string */
    protected $_label;

    /** @var string */
    protected $_helpText;

    /** @var string */
    protected $_errorText;

    /** @var mixed */
    protected $_value;

    /** @var bool */
    protected $_required = false;


    public function __construct($name){
        $this->setName($name);
        $this->setAttribute('id', 'field-' . str_replace(' ', '-', $name));
    }


    /**
     * @param string $path
     */
    public function setViewsPath($path){
        $this->_viewsPath = $path;
    }


    /**
     * @return string
     */
    public function getViewsPath(){
        return $this->_viewsPath;
    }


    public function getTag(){
        return $this->_tag;
    }

    /**
     * Set or Get field name
     *
     * @param  string $value
     * @return mixed|Field chaining
     */
    public function setName($value = null){
        $this->_name = $value;

        return $this;
    }


    /**
     * @return string
     */
    public function getName(){
        return $this->_name;
    }


    /**
     * @param string $label
     * @return mixed|Field chaining
     */
    public function setLabel($label){
        $this->_label = $label;

        return $this;
    }


    /**
     * @return string
     */
    public  function getLabel(){
        return $this->_label;
    }


    /**
     * @param string $text
     * @return mixed|Field chaining
     */
    public function setHelpText($text){
        $this->_helpText = $text;

        return $this;
    }


    /**
     * @return string
     */
    public function getHelpText(){
        return $this->_helpText;
    }

    /**
     * @return bool
     */
    public function hasHelpText(){
        return (bool) $this->_helpText;
    }


    /**
     * @param string $text
     * @return mixed|Field chaining
     */
    public function setErrorText($text){
        $this->_errorText = $text;

        return $this;
    }


    /**
     * @return string
     */
    public function getErrorText(){
        return $this->_errorText;
    }

    /**
     * @return bool
     */
    public function hasErrorText(){
        return (bool) $this->_errorText;
    }


    /**
     * Wraps name with container
     *
     * $field->name('name[]')
     * $field->wrap_name('test'); // returns test[name][]
     *
     * @param  string $container
     * @return string
     */
    public function wrapName($container){
        $base_name = $name = $this->getName();
        $rest_name = '';

        if(strpos($name, '[') !== false){
            $c_pos     = strpos($name, '[');
            $base_name = substr($name, 0, $c_pos);
            $rest_name = substr($name, $c_pos, strlen($name));
        }

        return $container . '[' . $base_name . ']' . $rest_name;
    }

    /**
     * Return html code for field
     *
     * @return string
     */
    public function render(){
        $file = $this->_getViewFile();
        if(is_file($file)){
            return $this->_renderView($file);
        }
        else{
            return $this->_renderPattern();
        }
    }

    /**
     * @param  string $file
     * @return string
     */
    protected function _renderView($file){
        $field = $this;

        ob_start();
        include $file;
        $result = ob_get_clean();

        return $result;
    }

    /**
     * @return string
     */
    protected function _renderPattern(){
        $data = $this->_getRenderData();

        $resultData = [];
        foreach($data as $key => $val){
            $resultData[sprintf(':%s:', $key)] = $val;
        }

        return str_replace(array_keys($resultData), $resultData, $this->_pattern);
    }

    /**
     * @return array
     */
    protected function _getRenderData(){
        $dataDefault = [
            'tag'        => '',
            'name'       => '',
            'value'      => '',
            'attributes' => '',
        ];
        $data        = [
            'tag'        => $this->getTag(),
            'name'       => $this->getName(),
            'value'      => $this->renderValue(),
            'attributes' => $this->renderAttributes(),
        ];

        return array_merge($dataDefault, array_filter($data));
    }

    /**
     * Set or Get attributes values
     *
     * @param array $values
     * @return array
     */
    public function setAttributes($values = null){
        $this->_attributes = array_merge($this->_attributes, $values);
    }


    /**
     * @return array
     */
    public function getAttributes(){
        return $this->_attributes;
    }

    /**
     * Return attribute by key
     * Or set attribute value
     *
     * @param string $key
     * @param mixed  $value
     * @return Field
     */
    public function setAttribute($key, $value = null){
        $this->_attributes[$key] = $value;

        return $this;
    }


    /**
     * @param  $key
     * @return mixed
     */
    public function getAttribute($key){
        return array_key_exists($key, $this->_attributes) ? $this->_attributes[$key] : null;
    }


    /**
     * @param  string $key
     * @return Field
     */
    public function unsetAttribute($key){
        if(array_key_exists($key, $this->_attributes)){
            unset($this->_attributes[$key]);
        }

        return $this;
    }


    public function addClass($class){
        $classes = explode(' ', $this->getAttribute('class') ?: '');

        $classes[] = $class;
        $this->setAttribute('class', join(' ', array_unique(array_filter($classes))));

        return $this;
    }


    public function removeClass($class){
        $classes = $this->getAttribute('class') ?: '';

        if( strpos($classes, $class) !== false ){
            $classes = array_filter(array_map('trim', explode(' ', $classes)));
            unset( $classes[array_search($class, $classes)]);

            $this->setAttribute('class', join(' ', $classes));
        }

        return $this;
    }


    /**
     * Is field required
     *
     * @param  bool $value
     * @return Field
     */
    public function setRequired($value = false){
        $this->_required = (bool)$value;

        if($this->_required){
            $this->setAttribute('required', 'required');
        }
        else{
            $this->unsetAttribute('required');
        }

        return $this;
    }


    /**
     * @return bool
     */
    public function isRequired(){
        return $this->_required;
    }

    /**
     * Return ready to use string of attributes
     *
     * @return string
     */
    public function renderAttributes(){
        $html = '';
        foreach($this->_attributes as $tag => $value){
            $html .= $tag . '="' . $value . '" ';
        }

        return $html;
    }

    /**
     * You may want to prepare value before use it
     *
     * @return mixed
     */
    public function renderValue(){
        return $this->_value;
    }

    /**
     * Return path to view file
     *
     * @return string
     */
    protected function _getViewFile(){
        return $this->_viewsPath . $this->_viewFile;
    }
}