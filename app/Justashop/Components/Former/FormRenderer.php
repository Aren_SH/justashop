<?php

namespace Justashop\Components\Former;


class FormRenderer {

    /**
     * @var \Justashop\Components\Former\Model\ModelFormInterface
     */
    protected $_form;

    /** @var string */
    protected $_viewsPath;


    protected $_withTabs = false;


    public function __construct( $form ){
        $this->_form = $form;
    }


    /**
     * @param string $path
     */
    public function setViewsPath( $path ){
        $this->_viewsPath = $path;
    }


    /**
     * @return string
     */
    public function getViewsPath(){
        return $this->_viewsPath;
    }

    /**
     * @param string $path
     */
    public function setWithTabs( $bool ){
        $this->_withTabs = $bool;
    }


    /**
     * @return string
     */
    public function withTabs(){
        return $this->_withTabs;
    }

    /**
     * @return FieldCollection[]
     */
    public function getCollections(){
        return $this->_form->getCollections();
    }


    /**
     * @return string
     */
    public function renderForm(){}


    /**
     * @return string
     */
    public function renderFields(){
        $file = $this->_getViewFilePath( 'fields.php' );

        return file_exists( $file ) ? $this->_renderViewFile( $file ) : null;
    }


    /**
     * @param  string $file
     * @return string
     */
    protected function _getViewFilePath( $file ){
        return $this->_viewsPath . $file;
    }


    /**
     * @param  string $file
     * @return string
     */
    protected function _renderViewFile( $file ){
        $form = $this;

        ob_start();
        include $file;
        $result = ob_get_clean();

        return $result;
    }
} 