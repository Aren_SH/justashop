<?php

namespace Justashop\Components\Former;

use Phalcon\Mvc\User\Component;
use Phalcon\Validation;
use Phalcon\DI;

abstract class Form extends Component {

    /**@var Validation */
    protected $_validation;

    /** @var Validation\Message\Group */
    protected $_messages;

    /** @var FieldCollection[] */
    protected $_collections = [];

    /** @var mixed[] */
    protected $_requestData = [];

    /** @var bool */
    protected $_requestHandled    = false;

    protected $_fieldsInitialized = false;

    protected $_useCSRF        = true;


    public static function Factory($di = null){
        /** @var Form $object */
        $object = new static();

        $object->setDI($di ? $di : DI\FactoryDefault::getDefault());

        return $object;
    }


    public function __construct(){
        $this->_messages = new Validation\Message\Group();
        $this->_init();
    }


    protected function _init(){
        $this->collection('default');
    }

    public function setView( $view ){
        $this->_view = $view;
    }

    /**
     * @return bool
     */
    public function isRequestHandled(){
        return $this->_requestHandled;
    }


    /**
     * @return bool
     */
    public function handleRequest(){
        if( !$this->_requestHandled ){
            $this->_requestData = $this->_handleRequestData();
            $this->_requestHandled = true;
        }

        return $this->_requestHandled;
    }


    /**
     * @return \mixed[]
     */
    public function getRequestData(){
        return $this->_requestData;
    }


    /**
     * @param  string $key
     * @param  mixed  $default
     * @return mixed
     */
    public function getFieldRequestData( $key, $default = null ) {
        $data = $this->getRequestData();

        return array_key_exists( $key, $data ) ? $data[$key] : $default;
    }


    /**
     * @return Validation\Message\Group
     */
    public function getMessages() {
        return $this->_messages;
    }


    /**
     * @param  $key
     * @return string[]
     */
    public function getMessage( $key ) {
        $res = [];
        foreach( $this->_messages as $msg ){
            /** @var Validation\Message $msg */
            if( $msg->getField() == $key ){
                $res[] = $msg->getMessage();
            }
        }

        return $res ?: null;
    }


    /**
     * @param  string $key
     * @return string
     */
    public function getMessageText( $key ){
        $errors = $this->getMessage( $key );

        return $errors ? join(PHP_EOL, $errors ) : null;
    }


    public function addField( $key, $field){
        $this->_collections['default']->addField($key, $field);
        return $this;
    }


    public function collection($name){
        if( !array_key_exists($name, $this->_collections)){
            $this->_collections[$name] = new FieldCollection();
            $this->_collections[$name]->setTitle($name);
        }

        return $this->_collections[$name];
    }

    /**
     * @return FieldCollection[]
     */
    public function getCollections(){
        if ( !$this->_fieldsInitialized ) {
            $this->_initFields();
            $this->_initAdditionalFields();

            if( $this->_useCSRF ){

                $field   =  new Fields\Hidden('csrf_token');
                $field->setValue($this->getCsrfToken());

                $this->addField('csrf_token', $field);
            }

            $this->_fieldsInitialized = true;
        }

        return $this->_collections;
    }

    /**
     * @param  $key
     * @param  $collection
     * @return \mixed
     */
    public function getField( $key, $collection = 'default' ) {
        if( !array_key_exists($collection, $this->_collections) ) return null;

        return $this->_collections[$collection]->getField($key);
    }


    /**
     * @return bool|mixed
     */
    public function validate(){
        $data       = $this->getRequestData();
        $validation = $this->getValidation();

        if( $this->_useCSRF && empty($data['csrf_token']) && !$this->checkCsrfToken($data['csrf_token']) ){
            $this->_messages->appendMessage(new Validation\Message('Invalid security token', $this->getCsrfSessionKey()));
        }

        $this->_messages->appendMessages( $validation->validate( $data ) );


        return count( $this->_messages ) == 0;
    }


    /**
     * @return Validation
     */
    public function getValidation(){
        if( !$this->_validation ){
            $this->_validation = $this->_initValidation();
        }

        return $this->_validation;
    }


    /**
     * @return mixed
     */
    protected function _handleRequestData(){
        $data = $this->request->getPost();

        $data = $this->_prepareRequestData($data);

        return $data;

    }


    /**
     * @param  mixed $data
     * @return mixed
     */
    protected function _prepareRequestData($data) {
        return $data;
    }


    /**
     * @return string
     */
    protected function getCsrfSessionKey(){
        $class= get_class($this);

        return $class . '-' . 'csrf-token';
    }


    /**
     * @return string
     */
    protected function getCsrfToken(){
        $val = hash('sha256', $this->getCsrfSessionKey() . time() .rand(1, 10000) );

        if( session_status() == PHP_SESSION_NONE ){
            $this->session->start();
        }

        $this->session->set($this->getCsrfSessionKey(), $val);
        return $val;
    }

    /**
     * @param  string $val
     * @return bool
     */
    protected function checkCsrfToken($val){
        if( session_status() == PHP_SESSION_NONE ){
            $this->session->start();
        }

        $storedVal = $this->session->get($this->getCsrfSessionKey());

        return $storedVal && ($val === $storedVal);
    }

    /**
     * @return mixed[]
     */
    protected abstract function _initFields();

    protected function _initAdditionalFields(){}

    protected abstract function _initValidation();


} 