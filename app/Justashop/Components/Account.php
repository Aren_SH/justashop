<?php

namespace Justashop\Components;


use Phalcon\Mvc\User\Component;

class Account extends Component{

    /** @var \Justashop\Components\Account\Authentication\Manager */
    protected $_authenticationManager;

    /** @var \Justashop\Components\Account\Registration\Manager */
    protected $_registrationManager;


    /**
     *
     *
     * @param $config
     */
    public function __construct($config){

        $hasher                       = new Account\Hasher($config);
        $validation                   = new Account\Registration\DefaultValidation();
        $this->_authenticationManager = new Account\Authentication\Manager($config);
        $this->_registrationManager   = new Account\Registration\Manager();

        $this->_hasher = $hasher;
        $this->_authenticationManager->setHasher($hasher);
        $this->_registrationManager->setHasher($hasher);

        $this->_registrationManager->setValidation($validation);
    }


    /**
     * @param \Phalcon\DiInterface $dependencyInjector
     */
    public function setDI($dependencyInjector){
        parent::setDI($dependencyInjector);

        $this->_authenticationManager->setDI($dependencyInjector);
        $this->_registrationManager->setDI($dependencyInjector);
    }

    /**
     * @return Account\Hasher
     */
    public function getHasher(){
        return $this->_hasher;
    }


    /**
     * @return Account\Model\User
     */
    public function getCurrentUser(){
        return $this->_authenticationManager->getCurrentUser();
    }


    /**
     * @param  int $id
     *
     * @return Account\Model\User
     */
    public function getUserById($id){
        return $this->_authenticationManager->getUserById($id);
    }


    /**
     * @param  string $login
     * @param  string $pass
     *
     * @return Account\Model\User
     */
    public function getUserByCredentials($login, $pass){
        $user = $this->_authenticationManager->getUserByCredentials($login, $pass);

        $this->logIn($user);

        return $user;
    }


    /**
     * @param $user
     */
    public function logIn($user){
        if($user){
            $this->_authenticationManager->remember($user);
        }
    }


    /**
     * @param Account\Model\User $user
     */
    public function logOut($user){
        if($user && $user->id){
            $this->_authenticationManager->forget($user);
        }
    }


    /**
     * @param  array $data
     * @return Account\Model\User
     */
    public function register($data){
        $user = $this->_registrationManager->createUser($data);

        if($user){
            $this->sendActivationNotification($user);
        }

        return $user;
    }


    /**
     * @return \Phalcon\Validation\Message\Group
     */
    public function getRegistrationMessages(){
        return $this->_registrationManager->getMessages();
    }


    /**
     * @param  string $token
     * @return Account\Model\User
     */
    public function activate($token){
        return $this->_registrationManager->activateUser($token);
    }


    /**
     * @param $user
     * @param $newPassword
     * @param $token
     */
    public function recoverPassword($user, $newPassword, $token){
        $this->_registrationManager->recoverPassword($user, $newPassword, $token);
    }


    /**
     * @param  Account\Model\User $user
     * @return string
     */
    public function sendActivationNotification($user){
        $token = $this->_registrationManager->generateActivationToken($user);

        /** @var Mail $mailer */
        $mailer = $this->getDI()->getShared('mailer');

        $mail = $mailer->fromView('account/activation', ['code' => $token]);
        $mail->addAddress($user->email);
        $mail->send();

        return $token;
    }


    public function sendPasswordRecoveryNotification($user){
    }
} 