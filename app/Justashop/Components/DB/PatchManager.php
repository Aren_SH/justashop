<?php
namespace Justashop\Components\DB;


use Phalcon\Db;
use Phalcon\Mvc\User\Component;

class PatchManager extends Component {

    /** @var string */
    protected $_path;

    /** @var resource */
    protected $_logResource;

    /** @var  Db\AdapterInterface */
    protected $_connection;

    /** @var string string */
    protected $_table = '_patches';

    /** @var string */
    protected $_ext = '.sql';

    public function __construct($config){

        if( empty( $config['path'] ) ){
            throw new \Exception('Patches directory path is not set');
        }

        $this->_path = realpath( $config['path']);
        if( !$this->_path || !is_readable( $this->_path)){
            throw new \Exception('Patches directory path is not exists or not readable');
        }
    }



    public function setConnection($con){
        $this->_connection = $con;
    }


    public function applyPatches(){
        $availablePatches = $this->listAvailablePatches();
        $appliedPatches   = $this->listAppliedPatches();

        $this->_startLogging();
        if(!count($availablePatches)){
            $this->_log("There are no patches in specified directory");
            $this->_log("Exit.");
            $this->_endLogging();
            return;
        }

        if(!count(array_diff($availablePatches, $appliedPatches))){
            $this->_log("There are no new patches in specified directory");
            $this->_log("Exit.");
            $this->_endLogging();
            return;
        }

        foreach( $availablePatches as $file ){
            if( in_array($file, $appliedPatches) ) continue;
            $this->_log(sprintf("Try to open file %s", $file));

            $content = file_get_contents($this->_path . DIRECTORY_SEPARATOR . $file);
            if(!$content){
                $this->_log("Failed to open file");
                break;
            }

            if( !$this->_applyPatch($file, $content)){
                break;
            }

            $this->_log("Patch was successfully applied");
            $this->_log("");
        }

        $this->_endLogging();

    }


    /**
     * @param string $boundPatch
     */
    public function rollbackPatches($boundPatch){
        $patches = $this->listAppliedPatches();

        sort($patches);
        $patches = array_reverse($patches);

        $this->_startLogging();
        if( !in_array($boundPatch, $patches)){
            $this->_log(sprintf("Patch '%s' was not applied", $boundPatch));
            $this->_endLogging();
            return;
        }

        $rPatches = [];
        foreach($patches as $patch){
            if( $patch == $boundPatch ){
                break;
            }

            $rPatches[] = $patch;
        }

        $rPatches[] = $boundPatch;

        foreach($rPatches as $patch){
            $this->_log(sprintf("Try to open file rollback/%s", $patch));

            $content = file_get_contents($this->_path . DIRECTORY_SEPARATOR . 'rollback' . DIRECTORY_SEPARATOR . $patch);
            if(!$content){
                $this->_log("Has no rollback patch. Skip");
                continue;
            }

            if( !$this->_rollbackPatch($patch, $content)){
                break;
            }

            $this->_log("Patch was successfully rolled back");
            $this->_log("");
        }

        $this->_endLogging();

    }


    /**
     * @return string[]
     * @throws \Exception
     */
    public function listAvailablePatches(){

        $dir = dir($this->_path);
        if( !$dir ){
            throw new \Exception('Cannot open patches directory');
        }

        $files = [];
        while( ($entry = $dir->read()) != false) {
            if($entry == '.' || $entry == '..' || is_dir($this->_path . $entry)){
                continue;
            }

            if( !strpos($entry, $this->_ext)) {
                continue;
            }

            $files[] = $entry;
        }


        return $files;
    }


    /**
     * @return array
     */
    public function listAppliedPatches(){
        $result = [];
        $query  = "SELECT * FROM " . $this->_table;

        try {
            $data   = $this->_connection->query($query)->fetchAll();

        } catch(\Exception $e){
            $data = false;
        }

        if( $data ){
            foreach($data as $row){
                $result[] = $row['file'];
            }
        }

        return $result ?: [];

    }


    /**
     * @param  string $file
     * @param  string $query
     * @return bool
     */
    protected function _applyPatch($file, $query){
        $insertQuery = sprintf('INSERT INTO %s VALUES(%s);', $this->_table, $this->_connection->escapeString($file));

        $status = true;
        $this->_connection->begin();
        try {
            $this->_connection->execute($query);
            $this->_connection->execute($insertQuery);
            $this->_connection->commit();
        }
        catch(\Exception $e){
            $status = false;
            $this->_connection->rollback();
            $this->_log("Failed to roll back patch:");
            $this->_log($e->getMessage());
        }


        return $status;
    }


    /**
     * @param  string $file
     * @param  string $query
     * @return bool
     */
    protected function _rollbackPatch($file, $query){
        $insertQuery = sprintf('DELETE FROM %s WHERE file = %s;', $this->_table, $this->_connection->escapeString($file));

        $status = true;
        $this->_connection->begin();
        try {
            $this->_connection->execute($query);
            $this->_connection->execute($insertQuery);
            $this->_connection->commit();
        }
        catch(\Exception $e){
            $status = false;
            $this->_connection->rollback();
            $this->_log("Failed to apply rollback patch:");
            $this->_log($e->getMessage());
        }


        return $status;
    }


    protected function _startLogging(){
        $this->_logResource = fopen('php://stdout', 'w');
    }


    protected function _endLogging(){
        fclose($this->_logResource);
    }


    protected function _log($str){
        fwrite($this->_logResource, $str . PHP_EOL);
    }
} 