<?php


namespace Justashop\Components\Common;

trait ListMixin {

    /**
     * @var array[]
     */
    protected $_items = [];

    /** @var int  */
    protected $_key    = 0;


    /**
     * Add new entry
     *
     * @param      $title
     * @param null $link
     */
    public function add( $title, $link = null ){
        $this->_items[] = new ListItem( $title, $link );
    }


    /**
     * @return ListItem
     */
    public function current(){
        return $this->valid() ? $this->_items[$this->_key] : null;
    }


    public function next(){
        $this->_key++;
    }


    public function key(){
        return $this->_key;
    }

    public function valid(){
        return $this->_key < count( $this->_items );
    }

    public function rewind(){
        $this->_key = 0;
    }
} 