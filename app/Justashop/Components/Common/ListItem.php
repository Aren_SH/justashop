<?php

namespace Justashop\Components\Common;


class ListItem {

    /** @var string */
    protected $_title;

    /** @var string */
    protected $_link;

    /** @var string */
    protected $_linkPattern   = '<li><a href=":link:" title=":title:">:title:</a></li>';

    /** @var string */
    protected $_activePattern = '<li>:title:</li>';

    public function __construct( $title, $link = null){
        $this->_title = $title;
        $this->_link  = $link;
    }


    /**
     * @param  string $linkPattern
     * @param  string $activePattern
     * @return string
     */
    public function render($linkPattern = null, $activePattern = null){

        $linkPattern   = $linkPattern ?: $this->_linkPattern;
        $activePattern = $activePattern ?: $this->_activePattern;

        $data = [
            ':title:' => $this->_title ?: '',
            ':link:'  => $this->_link ?: '',
        ];

        return str_replace( array_keys( $data ), array_values( $data), $this->_link ? $linkPattern : $activePattern );
    }

}