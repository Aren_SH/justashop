<?php

namespace Justashop\Components\Common\Validation;


use Justashop\Components\Messages\Translator;
use Phalcon\Validation;

class TranslatedValidation{

    /** @var \Phalcon\Validation */
    protected $_validation;

    /** @var \Justashop\Components\Messages\Translator */
    protected $_translator;

    /** @var string */
    protected $_path;


    /**
     *
     * @param Validation $validation
     * @param Translator $translator
     * @param string     $path Path to message file
     */
    public function __construct($validation, $translator, $path){

        $this->_validation = $validation;
        $this->_translator = $translator;
        $this->_path       = $path;
    }


    /**
     * @return Validation\Message\Group
     */
    public function getMessages(){
        $messages = $this->_validation->getMessages();

        /** @var Validation\Message $message */
        foreach($messages as $message){
            $message->setMessage($this->_translator->_($this->_path, $message->getMessage()));
        }

        return $messages;
    }


    /**
     * @param  string $name
     * @param  array  $arguments
     * @return mixed
     * @throws \Exception
     */
    public function __call($name, $arguments){
        if(method_exists($this->_validation, $name)){
            return $this->_validation->{$name}($arguments);
        }

        throw new \Exception(sprintf('Method %s does not exists in validation class', $name));
    }
}