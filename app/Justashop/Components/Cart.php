<?php

namespace Justashop\Components;


use Phalcon\Mvc\User\Component;

class Cart extends Component implements \Iterator, \Countable {

    const SESSION_KEY = 'cart';

    /** @var Cart\Item[] */
    protected $_items = [];

    /** @var int */
    protected $_key;

    protected $_sessionKey;


    /**
     * @param string $key session kye to store data;
     */
    public function __construct( $key = null ){
        $this->_sessionKey =  (string)$key ?: self::SESSION_KEY;
    }



    /**
     * @param mixed $id
     * @param float $price
     * @param int   $qty
     *
     * @return Cart\Item
     */
    public function create($id, $price, $qty = 1){
        return new Cart\Item($id, $price, $qty);
    }


    /**
     * @param Cart\Item $item
     */
    public function add( $item ){
        $replaced = false;
        foreach ( $this->_items as $key => $exItem ){
            if( $exItem->getId() == $item->getId() ){
                $this->_items[$key] = $item;
                $replaced = true;
                break;
            }
        }

        if( !$replaced ){
            $this->_items[] = $item;
        }
    }


    /**
     * @param  mixed     $id
     * @return Cart\Item
     */
    public function get( $id ){
        $result = null;
        foreach( $this->_items as $item ){
            if( $item->getId() == $id ){
                $result = $item;
                break;
            }
        }

        return $result;
    }


    /**
     * @param mixed $id
     */
    public function delete( $id ){
        $this->_items = array_values( array_filter(
            $this->_items,
            function( Cart\Item $n) use ($id) {
                return $n->getId() != $id;
            }
        ) );

    }


    public function clear(){
        $this->_items = [];
    }


    /**
     * @return float
     */
    public function getTotalPrice(){
        $price = 0;

        foreach($this->_items as $item){
            $price += $item->getPrice();
        }

        return $price;
    }


    public function setDiscount($discount){
        foreach( $this->_items  as $item){
            $item->setDiscount($discount);
        }
    }



    public function storeToSession(){
        /** @var \Phalcon\Session\AdapterInterface $session */
        $session = $this->getDI()->getShared('session');
        if( !$session->isStarted() ){
            $session->start();
        }

        $session->set($this->_sessionKey, $this->serialize() );
    }


    public function loadFromSession(){
        /** @var \Phalcon\Session\AdapterInterface $session */
        $session = $this->getDI()->getShared('session');
        if( !$session->isStarted() ){
            $session->start();
        }

        $data = $session->get($this->_sessionKey, false);
        if( $data ){
            $this->unserialize($data);
        }
    }


    /**
     * @return string
     */
    public function serialize(){
        return serialize($this->_items);
    }

    /**
     * @param string $data
     */
    public function unserialize($data){
        $items = unserialize($data);

        $this->_items = $items ? $items : [];
    }


    /**
     * @return Cart\Item
     */
    public function current(){
        return $this->valid() ? $this->_items[$this->_key] : null;
    }


    public function next(){
        $this->_key++;
    }


    public function key(){
        return $this->_key;
    }

    public function valid(){
        return array_key_exists($this->_key, $this->_items);
    }

    public function rewind(){
        $this->_key = 0;
    }


    public function count(){
        return count($this->_items);
    }
} 