<?php

namespace Justashop\Components;

use Phalcon\Mvc\User\Component;

class Messages extends Component{

    /** @var string */
    protected $_messagesPath;

    /** @var array */
    protected $_messages = [];

    /** @var string */
    protected $_delimiter = '.';

    /** @var string */
    protected $_extension = '.php';


    /**
     * @param string $path
     */
    public function setMessagesPath($path){
        $this->_messagesPath = $path;
    }


    /**
     * @return string
     */
    public function getMessagesPath(){
        return $this->_messagesPath;
    }


    /**
     * @param string $delimiter
     */
    public function setDelimiter($delimiter){
        $this->_delimiter = $delimiter;
    }


    /**
     * @return string
     */
    public function getDelimiter(){
        return $this->_delimiter;
    }


    /**
     * @param string $ext
     */
    public function setExtension($ext){
        $this->_extension = $ext;
    }


    /**
     * @return string
     */
    public function getExtension(){
        return $this->_extension;
    }


    /**
     * @param  string $path
     * @param  string $message
     * @param  string $default
     *
     * @return string
     */
    public function get($path, $message, $default = null){

        $dirs     = $this->splitPath($path);
        $filePath = $this->makePath($dirs);

        if(!$this->_pathLoaded($filePath)){
            $this->_loadPath($filePath);
        }

        $file  = array_pop($dirs);
        $arr   = $this->_messages;
        $found = true;
        foreach($dirs as $dir){
            if(empty($arr) || empty($arr[$dir])){
                $found = false;
                break;
            }
            $arr = $arr[$dir];
        }

        if($found && !empty($arr[$file][$message])){
            return $arr[$file][$message];
        }

        return $default !== null ? $default : $message;
    }


    /**
     * @param  string[] $parts
     * @return string
     */
    public function makePath( $parts ){
        if(!$parts) return '';

        return join( $this->_delimiter, $parts);
    }


    /**
     * @param  $path
     * @return array
     */
    public function splitPath($path){
        if(!$path){
            return [];
        }

        return explode($this->_delimiter, $path);
    }


    /**
     * @param $path
     * @return bool
     */
    public function _pathLoaded($path){
        if(!$path)
            return false;

        $result = true;
        $parts  = explode($this->_delimiter, $path);
        $arr    = $this->_messages;
        foreach($parts as $part){
            if(!is_array($arr) || empty($arr[$part]) || !is_array($arr[$part])){
                $result = false;
                break;
            }

            $arr = $arr[$part];
        }

        return $result;
    }


    /**
     * @param string $path
     */
    protected function _loadPath($path){
        $dirs = $this->splitPath($path);
        while( $dirs ){
            $result = $this->_loadFileData(join( $this->getDelimiter(), $dirs ));

            if(!$result || !is_array($result)){
                array_pop( $dirs );
            } else {
                break;
            }
        }

        $file = array_pop($dirs);
        $arr  = & $this->_messages;

        foreach($dirs as $dir){
            if(!is_array($arr)){
                $arr = [];
            }

            if(empty($arr[$dir])){
                $arr[$dir] = [];
            }

            $arr = & $arr[$dir];
        }

        $arr[$file] = $result;
    }


    /**
     * @param  string $path
     * @return array
     */
    protected function _loadFileData($path){
        if(!$path)
            return null;

        $filePath = sprintf('%s.php', str_replace($this->_delimiter, DIRECTORY_SEPARATOR, $path));

        $filePath = $this->_messagesPath . $filePath;

        if(!file_exists($filePath)){
            return null;
        }

        return require $filePath;
    }
} 