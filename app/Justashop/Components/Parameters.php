<?php

namespace Justashop\Components;

use \Phalcon\DI\InjectionAwareInterface;
use \Phalcon\Events\EventsAwareInterface;


/**
 * Provide simple access to App parameters loaded from DB
 *
 * @package Justashop\Helpers
 */
class Parameters implements InjectionAwareInterface, EventsAwareInterface{

    const DEFAULT_SITE_NAME = 'Justashop';


    /** @var \Phalcon\DiInterface */
    protected $_di;

    /** @var \Phalcon\Events\ManagerInterface  */
    protected $_eventsManager;

    /** @var array */
    protected $_parameters;

    /** @var string */
    protected $_cacheKey = 'parameters-parameters';

    /** @var int */
    protected $_cacheLifeTime = 86400; // 24 hours



    public function getSiteName(){
        return $this->getParameter('site.name', self::DEFAULT_SITE_NAME);
    }

    /**
     *
     * @param  string $key
     * @param  mixed  $default
     * @return mixed
     */
    public function getParameter($key, $default = null){
        if( is_null( $this->_parameters ) ){
            $this->_loadParameters();
        }

        return array_key_exists( $key, $this->_parameters ) ? $this->_parameters[$key] : $default;
    }


    /**
     * drop cached parameters
     */
    public function dropCache(){
        /** @var \Phalcon\Cache\Backend $cache */
        $cache = $this->_di->has('cache') ? $this->_di->get('cache') : null;
        if( $cache ){
            $cache->delete( $this->_cacheKey );
        }
    }


    /**
     * Get parameters from cache or load them from DB
     */
    protected function _loadParameters(){
        /** @var \Phalcon\Cache\Backend $cache */
        $cache = $this->_di->has('cache') ? $this->_di->get('cache') : null;

        if( $cache ){
            $this->_parameters = $cache->get( $this->_cacheKey );
        }

        if( !$this->_parameters ){
            $this->_parameters = [];

            $parameters = Parameters\Model\Parameter::find();
            foreach( $parameters as $parameter ){
                $this->_parameters[$parameter->key] = $parameter->value;
            }

            if( $cache && $this->_parameters ){
                $cache->save( $this->_cacheKey, $this->_parameters, $this->_cacheLifeTime );
            }
        }
    }


    /**
     * @param \Phalcon\DiInterface $dependencyInjector
     * @throw \Phalcon\Di\Exception
     */
    public function setDI ($dependencyInjector){
        $this->_di = $dependencyInjector;
    }


    /**
     * @return \Phalcon\DiInterface
     */
    public function getDI (){
        return $this->_di;
    }


    /**
     * @param \Phalcon\Events\ManagerInterface $eventsManager
     */
    public function setEventsManager($eventsManager){
        $this->_eventsManager = $eventsManager;
    }



    /**
     * @return \Phalcon\Events\ManagerInterface
     */
    public function getEventsManager(){
        return $this->_eventsManager;
    }
} 