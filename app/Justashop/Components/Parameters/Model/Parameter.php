<?php

namespace Justashop\Components\Parameters\Model;

use \Justashop\Mvc\Model;


/**
 * App Parameters
 *
 * @package Justashop\Model
 */
class Parameter extends Model {

    /** @var int */
    public $id;

    /** @var string */
    public $key;

    /** @var string */
    public $value;

    /** @var string */
    public $description;

    /** @var string */
    protected static $_table = 'parameters';

} 