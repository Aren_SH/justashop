<?php

namespace Justashop\Components;

use Phalcon\Mvc\User\Component;
use Phalcon\Mvc\View;

class Mail extends Component {


    const MAILER_SMTP = 'smtp';
    const MAILER_MAIL = 'mail';

    /** @var string */
    protected $_viewPath;

    protected $_supportEmail = 'support@mail.local';
    protected $_supportName  = 'Justashop Support';


    private static $_phpMailerIncluded = false;


    public function __construct($config = []){
        $this->_includePHPMailer();
    }


    /**
     * @param string $path
     */
    public function  setViewPath( $path ){
        $this->_viewPath = $path;
    }


    /**
     * @return string
     */
    public function getViewPath(){
        return $this->_viewPath;
    }


    /**
     * @return \PHPMailer
     */
    public function create(){
        $mail = new \PHPMailer();
        $this->_setInitialParams($mail);

        return $mail;
    }


    /**
     * @param  string $viewFile
     * @param  array  $params
     *
     * @return \PHPMailer
     */
    public function fromView($viewFile, $params = []){
        /** @var View\Simple $view */
        $view = $this->getDI()->getShared('simpleView');
        //todo add exception if no view service

        $message = $view->render($this->_prepareViewPath($viewFile), $params);

        $mail = $this->create();

        $mail->Body = $message;

        return $mail;
    }


    /**
     * @param  string $file
     * @return string
     */
    protected function _prepareViewPath($file){
        return join(DIRECTORY_SEPARATOR, [$this->_viewPath, $file]);
    }



    /**
     * @param \PHPMailer $mail
     */
    protected function _setInitialParams($mail){
        $mail->Mailer = self::MAILER_MAIL;
        $mail->Port   = 25;
        $mail->Host   = 'localhost';
        $mail->CharSet = 'utf-8';
        $mail->addReplyTo($this->_supportEmail, $this->_supportName);
    }


    /**
     * Include PHPMailer files
     */
    protected function _includePHPMailer(){
        if( static::$_phpMailerIncluded ) return;

        $path = join(DIRECTORY_SEPARATOR, [__DIR__, 'Mail', 'vendor', 'PHPMailer', '']);

        foreach(['phpmailer', 'pop3', 'smtp'] as $class ){
            include_once sprintf('%sclass.%s.php', $path, $class);
        }

        static::$_phpMailerIncluded = true;
    }
} 