<?php

namespace Justashop\Console;


class ArgParser {

    /** @var string */
    private $_taskNS;

    /** @var string */
    private $_shortPrefix    = '-';

    /** @var string */
    private $_extendedPrefix = '--';

    /** @var string */
    private $_arguments = [];

    /** @var string */
    private $_task;

    /** @var string */
    private $_action;


    /**
     * @param string $tasksNS
     */
    public function __construct($tasksNS = null){
        $this->_taskNS = $tasksNS;
    }


    /**
     * @param string $ns
     */
    public function setTasksNamespace($ns){
        $this->_taskNS = $ns;
    }


    /**
     * @return string
     */
    public function getTasksNamespace(){
        return $this->_taskNS;
    }


    /**
     * @return string
     */
    public function getTask(){
        return $this->_taskNS . ucfirst($this->_task);
    }


    /**
     * @return string
     */
    public function getAction(){
        return $this->_action;
    }


    /**
     * @param  int|string $key
     * @return bool
     */
    public function hasParam($key){
        return array_key_exists($key, $this->_arguments);
    }


    /**
     * @param  int|string $key
     * @param  mixed      $default
     * @return mixed
     */
    public function getParam($key, $default = null ){
        return array_key_exists($key, $this->_arguments) ? $this->_arguments[$key] : $default;
    }


    /**
     * @param  string[] $argv
     * @return bool
     */
    public function parse($argv){

        // remove script name
        array_shift($argv);
        if( !count( $argv) ) return false;

        $this->_task   = array_shift($argv);
        if( !count($argv)) return true;

        $this->_action = array_shift($argv);
        if( !count($argv)) return true;

        $isVal  = false;
        $valArg = null;
        foreach($argv as $i => $arg){
            if( strpos($arg, $this->_extendedPrefix) === 0 ){
                $valArg = substr($arg, strlen( $this->_extendedPrefix));
                $this->_arguments[$valArg] = true;

                $isVal  = true;
                continue;
            }

            if( strpos($arg, $this->_shortPrefix) === 0 ){
                $valArg = $this->_pushShortArgs(substr($arg, strlen( $this->_shortPrefix)));
                $isVal  = true;
                continue;
            }

            if( $isVal && $valArg ){
                $this->_arguments[$valArg] = $arg;
                $isVal  = false;
                continue;
            }

            $this->_arguments[$i] = $arg;
        }

        return true;
    }


    /**
     * @param  string $arg
     * @return string
     */
    private function _pushShortArgs($arg){
        for($i = 0; $i < strlen($arg); $i++){
            $this->_arguments[$arg[$i]] = true;
        }


        return $arg[strlen($arg) - 1];
    }
} 