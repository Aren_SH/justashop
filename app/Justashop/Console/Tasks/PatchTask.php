<?php

namespace Justashop\Console\Tasks;


use Phalcon\CLI\Task;

class PatchTask extends Task{


    public function applyAction(){

        $patchManager = $this->getDI()->getShared('patchManager');

        $patchManager->applyPatches();
    }


    public function listAction(){

        $patchManager = $this->getDI()->getShared('patchManager');
        $applied   = $patchManager->listAppliedPatches();
        $available = $patchManager->listAvailablePatches();

        foreach( $applied as $path ){
            echo "+". $path . PHP_EOL;
        }

        $notApplied = array_diff( $available, $applied);
        foreach( $notApplied as $path ){

            echo "-". $path . PHP_EOL;
        }
    }


    public function rollbackAction(){
        $patchManager = $this->getDI()->getShared('patchManager');
        $arguments    = $this->getDI()->getShared('arguments');

        if(!$arguments->hasParam('p')){
            echo "specify patch with -p" . PHP_EOL;
            return;
        }

        $path = $arguments->getParam('p');

        $patchManager->rollbackPatches($path);

    }

} 