<?php

namespace Justashop\Mvc\Model\Resultset;

use Justashop\Mvc\Model;
use Phalcon\Db\ResultInterface;
use Phalcon\Mvc\Model\Resultset;
use Phalcon\Mvc\ModelInterface;

class Multiple extends Resultset {

    /** @var string[] */
    protected $_classMap;


    /** @var string  */
    protected $_delimiter = '.';


    protected $_dataPrepared = false;

    /** @var ResultInterface */
    protected $_result;


    /**
     * @param array $classMap
     * @param $result
     */
    public function __construct($result, $classMap = null){
        $this->_result   = $result;
        $this->_classMap = $classMap;
    }


    /**
     * @return bool
     */
    public function valid(){
        $this->prepareData();
        return count($this->_rows) > $this->_pointer;
    }


    /**
     * @return string
     */
    public function serialize(){
        $this->prepareData();
        $data = [
            'rows'        => $this->_rows,
            'classMap'    => $this->_classMap,
            'cache'       => $this->_cache,
            'hydrateMode' => $this->_hydrateMode,
        ];

        return serialize($data);
    }


    public function unserialize( $serialized ){
        $this->prepareData();
        $data = unserialize($serialized);
        if( !is_array($data)) return;

        foreach(['rows', 'classMap', 'cache', 'hydrateMode'] as $param){
            if(!array_key_exists($param, $data)) continue;
            $this->{'_'.$param}     = $data[$param];
        }
    }


    /**
     * @return ModelInterface[]
     */
    public function toArray(){
        $this->prepareData();
        $result = [];

        foreach( $this->_rows as $row ){
            $result[] = $this->_fetchObject($row, $this->_classMap);
        }

        return $result;
    }


    /**
     * @return ModelInterface
     */
    public function current(){
        $this->prepareData();
        if( $this->_pointer < 0 ) $this->_pointer = 0;

        $row = $this->_rows[$this->_pointer];
        return $this->_fetchObject($row, $this->_classMap);
    }


    /**
     * @param  array $row
     * @return ModelInterface
     */
    protected function _fetchObject($row, $classMap, $object = null){
        $object = $object ?: new $classMap['default'];
        /** @var Model $object */
        $object->assign($this->_getClassRows($row ));

        foreach($classMap as $prefix => $settings){
            if( $prefix == 'default') continue;

            if (!is_array($settings)){
                $field       = $prefix;
                $class       = $settings;
                $subClassMap = null;
            } else {
                $class       = $settings['class'];
                $field       = $settings['field'];
                $subClassMap = !empty($settings['classMap']) ? $settings['classMap'] : null;
            }

            $relatedObjectRow = $this->_getClassRows($row, $prefix);

            if( !count($relatedObjectRow) ) {
                continue;
            }

            /** @var Model $relatedObject */
            $relatedObject = new $class;
            if( !$subClassMap ) {
                $relatedObject->assign($relatedObjectRow);
            } else {
                $this->_fetchObject($relatedObjectRow, $subClassMap, $relatedObject);
            }

            if( method_exists($relatedObject, 'afterFetch' ) ){
                $relatedObject->afterFetch();
            }

            $object->{$field} = $relatedObject;
        }

        return $object;
    }


    /**
     * @param  array  $row
     * @param  string $prefix
     * @return array
     */
    protected function _getClassRows($row, $prefix = null ){
        $result = [];

        $prefixWithDelimiter = $prefix . $this->_delimiter;
        $prefixLength = strlen($prefixWithDelimiter);
        foreach( $row as $key => $val){
            if( is_numeric($key) ) continue;
            if( $prefix && strpos( $key, $prefixWithDelimiter) !== false){
                $newKey = substr($key, $prefixLength);
                $result[$newKey] = $val;
            }

            if(!$prefix && strpos($key, $this->_delimiter) === false ){
                $result[$key] = $val;
            }
        }

        return array_filter($result, function($n){ return !is_null($n); });
    }


    protected function prepareData(){
        if( $this->_pointer != -1 ) return;
        $this->rewind();
    }
} 