<?php

namespace Justashop\Mvc\Model\Query;

use Phalcon\Db;
use Phalcon\Mvc\Model\QueryInterface;
use \Phalcon\DI\InjectionAwareInterface;

class Simple implements QueryInterface {

    const BIND_PARAM_NULL    = 0; //Phalcon\Db\Column::BIND_PARAM_NULL;
    const BIND_PARAM_INT     = 1; //Phalcon\Db\Column::BIND_PARAM_INT;
    const BIND_PARAM_STR     = 2; //Phalcon\Db\Column::BIND_PARAM_STR;
    const BIND_PARAM_FLOAT   = 3;
    const BIND_PARAM_BOOL    = 5; //Phalcon\Db\Column::BIND_PARAM_BOOL;

    const BIND_PARAM_INT_LIST   = 101;
    const BIND_PARAM_STR_LIST   = 102;
    const BIND_PARAM_FLOAT_LIST = 103;
    const BIND_PARAM_BOOL_LIST  = 105;


    protected $_queryString;

    protected $_bindParams = [];

    protected $_bindTypes = [];

    protected $_di;

    /** @var Db\AdapterInterface */
    protected $_connection;


    protected $_typeConverters = [

    ];


    protected static $_initialized = false;


    protected static $_defaultConverters = [];


    /**
     * @param  string $queryString
     * @return Simple
     */
    public static function factory($queryString){
        $className = __CLASS__;
        $di        = \Phalcon\DI\FactoryDefault::getDefault();

        /** @var Simple $query */
        $query = new $className($queryString);
        $query->setConnection( $di->getShared('db'));

        return $query;
    }


    /**
     * @param string $queryString
     */
    public function __construct( $queryString ){
        $this->_queryString = $queryString;

        if( !self::$_initialized ){
            self::_initialize();
        }

        $this->_typeConverters = self::$_defaultConverters;
    }

    public function getConnection(){
        return $this->_connection;
    }


    public function setConnection( $conn ){
        $this->_connection = $conn;
    }



    public function parse(){
        return $this->_getQueryStringWithParams();
    }


    public function execute($bindParams = null, $bindTypes = null){
        $queryString = $this->parse();

        return $this->_connection->query($queryString);
    }


    public function setParam( $param, $value, $type = self::BIND_PARAM_STR ){

        $this->_bindParams[$param] = $value;
        $this->_bindTypes[$param]  = $type;

    }


    public function _getQueryStringWithParams(){
        $preparedParams = [];
        foreach($this->_bindParams as $param => $value){
            $preparedParams[':' . $param . ':'] = $this->_prepareValue($value, $this->_bindTypes[$param]);
        }

        return str_replace(array_keys($preparedParams), array_values($preparedParams), $this->_queryString);
    }


    /**
     * @param  $value
     * @param  $type
     * @return mixed
     */
    public function _prepareValue( $value, $type){
        if( !array_key_exists($type, $this->_typeConverters)) return $value;

        $converted = $this->_typeConverters[$type]($value);

        return $this->_isValueNull($converted) ? 'null' : $converted;
    }


    protected function _isValueNull( $value ){
        return is_null($value) || $value == '' || strtolower($value == 'null');
    }



    protected static function _initialize(){

        $toNull =  function ( $value ){
            return null;
        };

        $toInt = function ( $value ){
            return is_int($value) ? intval($value) : null;
        };

        $toFloat = function ( $value ){
            return is_numeric($value) ? floatval($value) : null;
        };

        $toStr = function( $value ){
            return pg_escape_string(null, $value);
        };

        $toBool = function( $value ){
            return !empty( $value );
        };


        $toList = function($callback){

            $func = function( $values) use( &$func, &$callback ){
                if(!is_array($values)) return $callback($values);

                $result = [];
                foreach( $values as $val ){
                    $result[] = $func($val);
                }

                return join(', ', $result);
            };

            return $func;
        };

        $toIntList   = $toList($toInt);
        $toStrList   = $toList($toStr);
        $toBoolList  = $toList($toBool);
        $toFloatList = $toList($toFloat);

        self::$_defaultConverters = [
            self::BIND_PARAM_INT        => $toInt,
            self::BIND_PARAM_STR        => $toStr,
            self::BIND_PARAM_NULL       => $toNull,
            self::BIND_PARAM_FLOAT      => $toFloat,
            self::BIND_PARAM_BOOL       => $toBool,
            self::BIND_PARAM_INT_LIST   => $toIntList,
            self::BIND_PARAM_STR_LIST   => $toStrList,
            self::BIND_PARAM_FLOAT_LIST => $toFloatList,
            self::BIND_PARAM_BOOL_LIST  => $toBoolList,
        ];
    }
} 