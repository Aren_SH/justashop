<?php

namespace Justashop\Mvc\Model\Query\Builder;


use Phalcon\DI\FactoryDefault;
use Phalcon\Mvc\Model\Query\BuilderInterface;

class Simple {

    protected $_columns    = [];

    protected $_conditions = [];

    protected $_limit;

    protected $_offset;

    protected $_orderBy;

    protected $_bindParams = [];

    protected $_bindTypes = [];

    protected $_tables = [];

    /** @var  \Phalcon\Db\AdapterInterface */
    protected $_conn;

    public static function factory($params = null, $di = null){
        $di = $di ?: FactoryDefault::getDefault();

        $class = __CLASS__;

        /** @var Simple $builder */
        $builder = new $class($params);
        $builder->setConnection($di->getShared('db'));

        return $builder;

    }


    public function __construct($params = null){
        $this->_initTable($params);
        $this->_initColumns($params);
        $this->_initConditions($params);
        $this->_initGroupBy($params);
        $this->_initOrderBy($params);
        $this->_initLimit($params);
        $this->_initBinds($params);
    }


    public function setConnection($conn){
        $this->_conn = $conn;
    }

    public function getConnection(){
        return $this->_conn;
    }


    public function columns( $columns ){
        $this->setColumns( $columns );
    }


    /**
     * @param array $columns
     */
    public function setColumns( $columns ) {
        $this->_columns = $this->_addAliasesToField($columns);
    }

    /**
     * @param array $columns
     */
    public function addColumns( $columns ){
        $this->_columns = array_merge( $this->_columns, $this->_addAliasesToField($columns));
    }


    /**
     * @return array
     */
    public function getColumns(){
        return $this->_columns;
    }

    /**
     * @param  array $columns
     * @return array
     */
    protected function _addAliasesToField($columns){
        $result = [];

        if( !$columns ) return $result;

        foreach( $columns as $column ){
            if( !is_array($column)){
                $column = ['name' => $column, 'alias' => null];
            }
            elseif(!array_key_exists('alias', $column)){
                $column['alias'] = null;
            }

            $result[] = $column;
        }

        return $result;
    }


    public function setFrom( $from ){
        $this->_tables = $this->_addAliasesToField($from);
    }

    public function addFrom( $from ){
        $this->_tables = array_merge($this->_tables, $this->_addAliasesToField($from));
    }

    public function getTables(){
        return $this->_tables;
    }


    public function setConditions( $conditions, $bind = null, $bindTypes = null ){
        $this->_conditions = ['condition' => $conditions, 'glue' => 'AND'];

        if( $bind )      $this->_bindParams = array_merge($this->_bindParams, $bind);
        if( $bindTypes ) $this->_bindTypes  = array_merge($this->_bindTypes,  $bindTypes);
    }


    public function addConditions( $conditions, $bind = null, $bindTypes = null, $type = 'AND'){
        $this->_conditions = array_merge($this->_conditions, ['condition' => $conditions, 'glue' => $type ] );

        if( $bind )      $this->_bindParams = array_merge($this->_bindParams, $bind);
        if( $bindTypes ) $this->_bindTypes  = array_merge($this->_bindTypes,  $bindTypes);
    }


    public function orderBy( $orderBy ){
        $this->_orderBy = $orderBy;
    }

    public function limit( $limit ){
        if(!is_array($limit)){
            $limit = ['number' => $limit, 'offset' => null];
        }

        $this->_limit = $limit;
    }


    public function getQuery(){
        $queryString = $this->getQueryString();

        $query = new \Justashop\Mvc\Model\Query\Simple( $queryString );
        $query->setConnection($this->_conn);
        foreach( $this->_bindParams as $param => $value ){
            $query->setParam( $param, $value, $this->_bindTypes[$param] );
        }

        return $query;
    }


    public function getQueryString(){
        $query = sprintf( 'SELECT %s FROM %s', $this->_compileColumns(), $this->_compileFrom());

        if( $this->_conditions ){
            $query .= ' WHERE ' . $this->_compileConditions();
        }

        $this->_compileOrderBy($query);
        $this->_compileLimit($query);

        return $query;
    }


    protected function _compileColumns(){
        if( !$this->_columns ) return ' * ';

        $columns = [];
        foreach( $this->_columns as $column ){
            if( $column['alias'] ){
                $columns[] = $this->_conn->escapeIdentifier($column['name']) . ' AS ' . $this->_conn->escapeIdentifier($column['alias']);
            } else {
                $columns[] = $this->_conn->escapeIdentifier($column['name']);
            }
        }

        return join(', ', $columns);
    }


    protected function _compileFrom(){
        $tables = [];
        foreach( $this->_tables as $table ){
            if( $table['alias'] ){
                $tables[] = $this->_conn->escapeIdentifier($table['name']) . ' AS ' . $this->_conn->escapeIdentifier($table['alias']);
            } else {
                $tables[] = $this->_conn->escapeIdentifier($table['name']);
            }
        }

        return join(', ', $tables);
    }

    protected function _compileConditions(){
        $conditions = '';
        foreach( $this->_conditions as $i => $cond ){
            if( $i == 0 ){
                $conditions .= $cond['condition'];
            } else {
                $conditions .= sprintf( ' %s %s', $cond['glue'], $cond['condition'] );
            }
        }

        return $conditions;
    }


    protected function _compileLimit(&$query){
        if(!$this->_limit) return;

        $parts = [$query, 'LIMIT ' . $this->_limit['number']];
        if($this->_limit['offset']){
            $parts[] = 'OFFSET ' . $this->_limit['offset'];
        }

        $query = join(' ', $parts);
    }

    protected function _compileOrderBy(&$query){
        if( $this->_orderBy){
            $query = $query . ' ORDER BY ' . $this->_orderBy;
        }
    }





    protected  function _initTable($params){
        if(!$params || !array_key_exists('from', $params)) return;

        $this->setFrom( $params['from'] );
    }


    /**
     * @param  array|null $params
     */
    protected function _initColumns($params){
        if( !$params || !array_key_exists('columns', $params)) return;

        $columns = $params['columns'];
        if( !is_array($columns ) ){
            $columns = array_map(function($n){ return trim($n);}, explode(',', $columns));
        }

        $this->setColumns($columns);

    }


    /**
     * @param  array|null $params
     */
    protected  function _initConditions( $params){
        if( !$params || !array_key_exists('conditions', $params)) return;

        $this->_conditions[] = ['condition' => $params['conditions'], 'glue' => 'AND'];
    }


    /**
     * @param  array|null $params
     */
    protected function _initGroupBy( $params){
        if( !$params || !array_key_exists('group', $params)) return;

        $this->_groupBy = $params['group'];
    }


    /**
     * @param  array|null $params
     */
    protected function _initLimit($params){
        if( !$params || !array_key_exists('limit', $params)) return;

        $limit = $params['limit'];
        if(!is_array($limit)){
           $limit = ['number' => $limit, 'offset' => null];
        }

        $this->_limit = $limit;
    }


    /**
     * @param  array|null $params
     */
    protected function _initOrderBy($params){
        if( !$params || !array_key_exists('order', $params)) return;

        $this->_orderBy = $params['order'];
    }


    /**
     * @param  array|null $params
     */
    protected function _initBinds($params){
        if( !$params || !array_key_exists('bind', $params)) return;

        $this->_bindParams = $params['bind'];
        $this->_bindTypes  = $params['bindTypes'];

    }

} 