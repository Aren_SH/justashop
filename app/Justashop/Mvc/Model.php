<?php

namespace Justashop\Mvc;

use Justashop\Mvc\Model\Query\Builder\Simple;
use \Justashop\Mvc\Model\Resultset;
use Phalcon\Mvc\ModelInterface;

/**
 * Class Model
 *
 * @package Justashop\Mvc
 */
abstract class Model extends \Phalcon\Mvc\Model {

    /** @var string */
    protected static $_table;

    /** @var string */
    protected static $_view;


    protected static $_classMap;


    /**
     * @return string
     */
    public function getSource(){
        return static::$_table;
    }


    /**
     * By default try to fetch from view;
     */
    public function initialize(){
        $this->_setRelations();
        $this->_setBehaviours();
    }


    protected function _setRelations(){}
    protected function _setBehaviours(){}


    /**
     * @return string
     */
    public static function getClass(){
        return get_called_class();
    }


    /**
     * @return array
     */
    public static function getClassMap(){
        return [
            'default' => static::getClass(),
        ];
    }


    /**
     * @param  int|int[] $id
     *
     * @return ModelInterface|Resultset\Multiple
     */
    public static function findById( $id ){
        if( !is_array( $id ) ) $id = [$id];

        $ids = array_filter($id, 'is_int');
        if( !$ids ) return null;

        if( count( $ids ) == 1) {
            $params = [
                'conditions' => 'id = :id:',
                'bind'       => ['id' => reset($id) ],
                'bindTypes'  => ['id' => Model\Query\Simple::BIND_PARAM_INT]
            ];
        } else {
            $params = [
                'conditions' => 'id IN (:id:)',
                'bind'       => ['id' => $id ],
                'bindTypes'  => ['id' => Model\Query\Simple::BIND_PARAM_INT_LIST]
            ];
        }

        $objects = static::findWithRelations($params);
        return $objects->count() == 1 ? $objects->current() : $objects->toArray();
    }


    /**
     * @param null $params
     * @return Resultset\Multiple
     */
    public static function findWithRelations( $params = null ){

        if( !$params ) $params = [];

        $params['from'] = [static::$_view ?: static::$_table];

        $classMap = static::getClassMap();

        $builder = Simple::factory($params);

        $query = $builder->getQuery();

        return new Resultset\Multiple($query->execute(), $classMap);


    }


    public static function findFirst($parameters = null){
        return parent::findFirst($parameters);
    }

    public static function find($parameters = null){
        return parent::find( $parameters );
    }

    public function save($data = null, $whiteList = null){
        return parent::save($data, $whiteList);
    }
} 