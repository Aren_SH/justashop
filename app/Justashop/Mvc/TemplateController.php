<?php

namespace Justashop\Mvc;

use Justashop\Components\Parameters;
use \Justashop\Helpers\DI;


/**
 * Class TemplateController
 *
 * @package Justashop\Mvc
 */
class TemplateController extends Controller {


    protected $_template;

    protected $_title;

    public function initialize(){
        $parameters = $this->di->get( DI::PARAMETERS );
        $this->_title = $parameters->getSiteName();

        $view       = $this->di->get( DI::VIEW );
        if( $view instanceof TemplateView ){
            $view->setTemplate( $this->_getTemplate() );
        }
    }


    protected function _getTemplate(){
        if( $this->_template ){
            return $this->_template;
        }
        else{
            /** @var Parameters $parameters */
            $parameters = $this->di->get( DI::PARAMETERS );
            return $parameters ? $parameters->getParameter( 'template.name', 'default' ) : 'default';
        }

    }
}