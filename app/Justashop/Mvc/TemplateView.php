<?php

namespace Justashop\Mvc;

use Phalcon\Mvc\View;

/**
 * Class TemplateView
 * Allow to use templates without copy/paste each view file.
 * If some view file don`t exists in current template folder, it will be loaded from "default" folder
 *
 *
 * @package Justashop\Mvc
 */
class TemplateView extends View{

    /**
     * @var string
     */
    protected $_template;

    /**
     * @var string
     */
    protected $_defaultTemplate = 'default';


    public function setTemplate($template){
        $this->_template = $template;
    }

    public function getTemplate(){
        return $this->_template;
    }


    /**
     * Checks whether view exists on registered extensions and render it
     *
     * @param array                           $engines
     * @param string                          $viewPath
     * @param boolean                         $silence
     * @param boolean                         $mustClean
     * @param \Phalcon\Cache\BackendInterface $cache
     */
    protected function _engineRender($engines, $viewPath, $silence, $mustClean, $cache){
        $viewPath  = $this->_fixPath($viewPath);
        $finalPath = $this->_getFinalPathWithTemplate($viewPath);

        if($this->_template && $this->_viewPathExists(array_keys($engines), $finalPath)){
            $viewPath = $this->_template . DIRECTORY_SEPARATOR . $viewPath;
        }
        else {
            $viewPath = $this->_defaultTemplate . DIRECTORY_SEPARATOR . $viewPath;
        }

        parent::_engineRender($engines, $viewPath, $silence, $mustClean, $cache);
    }


    /**
     * @param  string $path
     * @return string
     */
    protected function _fixPath($path){
        $path = str_replace(['\\', '/'], DIRECTORY_SEPARATOR, $path);

        $pathParts = explode(DIRECTORY_SEPARATOR, $path);
        foreach($pathParts as &$part){
            $part = lcfirst($part);
        }

        return join(DIRECTORY_SEPARATOR, $pathParts);
    }


    /**
     * Get final path to view file including template dir
     *
     * @param  string $viewPath
     * @return string
     */
    protected function _getFinalPathWithTemplate($viewPath){
        return sprintf('%s%s%s%s',
            $this->_basePath,
            $this->_viewsDir,
            $this->_template . DIRECTORY_SEPARATOR,
            $viewPath
        );
    }


    /**
     * Check if specified file exists for at least one of engines
     *
     * @param  string[] $extensions
     * @param  string   $viewPath
     * @return bool
     */
    protected function _viewPathExists($extensions, $viewPath){
        foreach($extensions as $extension){
            if(file_exists($viewPath . $extension)){
                return true;
            }
        }

        return false;
    }
} 