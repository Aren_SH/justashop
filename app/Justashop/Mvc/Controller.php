<?php


namespace Justashop\Mvc;


abstract class Controller extends \Phalcon\Mvc\Controller {


    public function addBehaviour( $behaviour ){
        $this->getDI()->getShared('eventsManager')->attach('dispatch', $behaviour );
    }
} 