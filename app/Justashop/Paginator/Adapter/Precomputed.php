<?php

namespace Justashop\Paginator\Adapter;


class Precomputed implements \Phalcon\Paginator\AdapterInterface{


    public function __construct($config){
        $this->_items = $config['data'];
        $this->_total = (int)$config['total'];
        $this->_limit = (int)$config['limit'];
        $this->_page  = (int)$config['page'];

    }


    public function setCurrentPage($page){
        $this->_page = $page;
    }

    public function getPaginate(){
        $result = new \stdClass();
        $result->total_pages = ceil($this->_total / $this->_limit);
        $result->total_items = $this->_total;

        if( $this->_page > $result->total_pages){
            $this->_page = $result->total_pages;
        }

        $result->current = $this->_page;
        $result->before  = $this->_page <= 1 ? 1 : $this->_page - 1;
        $result->next    = $this->_page >= $result->total_pages ? $result->total_pages : $this->_page + 1;
        $result->last    = $result->total_pages;
        $result->first   = 1;

        $result->items = $this->_items;


        return $result;
    }
} 