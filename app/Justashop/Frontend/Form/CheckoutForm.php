<?php
namespace Justashop\Frontend\Form;


use Justashop\Components\Cart;
use Justashop\Components\Former\Form;
use Justashop\Helpers\DI;
use Justashop\Model\Address;
use Justashop\Model\Order;
use Justashop\Model\Delivery;
use Phalcon\Validation;
use Phalcon\Validation\Validator;

class CheckoutForm extends Form{

    /** @var Cart */
    private $_cart;

    /** @var  Delivery\Type[] */
    private $_deliveryTypes;

    /** @var Address */
    private $_address;

    /** @var Order */
    private $_order;


    public function __construct(){
        parent::__construct();

        $this->_order   = new Order();
        $this->_address = new Address();
    }


    public function handleRequest(){
        $result = parent::handleRequest();
        $this->_setAddressData();
        $this->_setOrderData();

        return $result;
    }


    public function setCart($cart){
        $this->_cart = $cart;
    }


    public function getOrder(){
        return $this->_order;
    }


    public function getDeliveryTypes(){
        if(!$this->_deliveryTypes){
            $this->_deliveryTypes = $this->_loadDeliveryTypes();
        }

        return $this->_deliveryTypes;
    }


    protected function _initFields(){
        //todo: phone and zip-code validation
        $t = $this->getDI()->get(DI::TRANSLATOR);

        /** @var \Justashop\Components\Former\Builder $former */
        $former = $this->getDI()->getShared('formBuilder');

        $this->addField('name', $former
                ->field('input', 'name')
                ->setLabel($t->_('cart.checkout.fields', 'name'))
                ->setHelpText($t->_('cart.checkout.help', 'name'))
                ->setErrorText($this->getMessageText('name'))
                ->setValue($this->getFieldRequestData('name'))
                ->setRequired(true)
                ->setAttribute('maxlength', 128)
                ->setAttribute('minlength', 5)
            )
            ->addField('email', $former
                    ->field('input', 'email')
                    ->setLabel($t->_('cart.checkout.fields', 'email'))
                    ->setHelpText($t->_('cart.checkout.help', 'email'))
                    ->setErrorText($this->getMessageText('email'))
                    ->setValue($this->getFieldRequestData('email'))
                    ->setAttribute('email', 'email')
                    ->setRequired(true)
            )
            ->addField('phone', $former
                    ->field('input', 'phone')
                    ->setLabel($t->_('cart.checkout.fields', 'phone-number'))
                    ->setHelpText($t->_('cart.checkout.help', 'phone-number'))
                    ->setErrorText($this->getMessageText('phone-number'))
                    ->setValue($this->getFieldRequestData('phone'))
                    ->setAttribute('pattern', '^[0-9]+$')
                    ->setRequired(true)
            )
            ->addField('country', $former
                    ->field('select', 'country')
                    ->setLabel($t->_('cart.checkout.fields', 'country'))
                    ->setHelpText($t->_('cart.checkout.help', 'country', ''))
                    ->setErrorText($this->getMessageText('country'))
                    ->setSelected([1])
                    ->setOptions([1 => 'Россия'])
                    ->setRequired(true)
            )
            ->addField('zip_code', $former
                    ->field('input', 'zip_code')
                    ->setLabel($t->_('cart.checkout.fields', 'zip-code'))
                    ->setHelpText($t->_('cart.checkout.help', 'zip-code'))
                    ->setErrorText($this->getMessageText('zip-code'))
                    ->setValue($this->getFieldRequestData('zip_code'))
                    ->setRequired(true)
            )
            ->addField('address', $former
                    ->field('textarea', 'address')
                    ->setLabel($t->_('cart.checkout.fields', 'address'))
                    ->setHelpText($t->_('cart.checkout.help', 'address'))
                    ->setErrorText($this->getMessageText('address'))
                    ->setValue($this->getFieldRequestData('address'))
                    ->setAttribute('minlength', 5)
                    ->setRequired(true)
            )
            ->addField('delivery_type_id', $former
                    ->field('objectSelect', 'delivery_type_id')
                    ->setLabel($t->_('cart.checkout.fields', 'delivery-type'))
                    ->setHelpText($t->_('cart.checkout.help', 'delivery-type', ''))
                    ->setErrorText($this->getMessageText('delivery-type'))
                    ->setSelected([1])
                    ->setOptions($this->getDeliveryTypesOptions())
                    ->setTitleField('title')
                    ->setAttributeFields(['price'])
                    ->setRequired(true)
            );
    }

    protected function getDeliveryTypesOptions(){
        $types = [];

        foreach($this->getDeliveryTypes() as $type){
            $types[$type->id] = $type;
        }

        return $types;
    }

    protected function _loadDeliveryTypes(){
        return Delivery\Type::find();
    }


    protected function _initValidation(){
        $validator = new Validation();

        $t = $this->getDI()->get(DI::TRANSLATOR);

        $validator->add('name', new Validator\PresenceOf([
            'message' => $t->_('cart.checkout.validation', 'name required')
        ]));
        $validator->add('name', new Validator\StringLength([
            'max'            => 128,
            'messageMaximum' => $t->_('cart.checkout.validation', 'name too long'),
            'min'            => 5,
            'messageMinimum' => $t->_('cart.checkout.validation', 'name too short'),
        ]));

        $validator->add('email', new Validator\PresenceOf([
            'message' => $t->_('cart.checkout.validation', 'email required')
        ]));
        $validator->add('email', new Validator\Email([
            'message' => $t->_('cart.checkout.validation', 'invalid email format')
        ]));

        $validator->add('phone', new Validator\PresenceOf([
            'message' => $t->_('cart.checkout.validation', 'phone number required')
        ]));
        $validator->add('phone', new Validator\Regex([
            'pattern' => '/[0-9]+$/',
            'message' => $t->_('cart.checkout.validation', 'invalid phone number format')
        ]));

        $validator->add('zip_code', new Validator\PresenceOf([
            'message' => $t->_('cart.checkout.validation', 'zip-code required')
        ]));

        $validator->add('address', new Validator\PresenceOf([
            'message' => $t->_('cart.checkout.validation', 'address required')
        ]));
        $validator->add('address', new Validator\StringLength([
            'min'            => 5,
            'messageMinimum' => $t->_('cart.checkout.validation', 'address too short'),
        ]));


        return $validator;
    }


    public function save(){
        /** @var \Phalcon\Db\AdapterInterface $connection */
        $connection = $this->getDI()->getShared('db');
        $connection->begin();

        if(!$this->_address->save()){
            $this->_messages->appendMessages($this->_address->getMessages());
            $connection->rollback();

            return false;
        }

        if(!$this->_saveOrder()){
            $connection->rollback();

            return false;
        }

        if(!$this->_saveOrderProducts()){
            $connection->rollback();

            return false;
        }

        $connection->commit();

        return true;
    }

    protected function _setAddressData(){
        $data = $this->getRequestData();

        $this->_address->country  = (int)$data['country'];
        $this->_address->address  = $data['address'];
        $this->_address->zip_code = $data['zip_code'];
        $this->_address->status   = 1;
    }

    protected function _setOrderData(){
        $data = $this->getRequestData();

        $this->_order->address_id       = $this->_address->id;
        $this->_order->status           = Order::STATUS_CREATED;
        $this->_order->user_name        = $data['name'];
        $this->_order->user_phone       = $data['phone'];
        $this->_order->user_email       = filter_var($data['email'], FILTER_SANITIZE_EMAIL);
        $this->_order->delivery_type_id = (int)$data['delivery_type_id'];
    }


    protected function _countAmount($discount = 1){
        $this->_cart->setDiscount($discount);

        return $this->_cart->getTotalPrice();
    }


    protected function _saveOrder(){
        $deliveryType = null;
        foreach($this->getDeliveryTypes() as $type){
            if($type->id == $this->_order->delivery_type_id){
                $deliveryType = $type;
                break;
            }
        }

        $this->_order->status     = Order::STATUS_NOT_PAID;
        $this->_order->address_id = $this->_address->id;
        $this->_order->amount     = $this->_countAmount(1);
        $this->_order->hash       = hash('sha256', $this->_order->user_phone . $this->_order->user_email);

        $result = $this->_order->save();
        if(!$result){
            $this->_messages->appendMessages($this->_order->getMessages());
        }

        $this->_order->deliveryType = $deliveryType;
        $this->_order->address      = $this->_address;

        return $result;
    }


    protected function _saveOrderProducts(){
        /** @var Order\Product[] $products */
        $products = [];

        foreach($this->_cart as $item){
            /** @var Cart\Item $item */
            $product             = new Order\Product();
            $product->product_id = $item->getId();
            $product->order_id   = $this->_order->id;
            $product->quantity   = $item->getQuantity();
            $product->price      = $item->getPrice();

            $products[] = $product;
        }

        $success = true;
        foreach($products as $product){
            if(!$product->save()){
                $success = false;
                $this->_messages->appendMessages($product->getMessages());
                break;
            }
        }


        return $success;
    }
}