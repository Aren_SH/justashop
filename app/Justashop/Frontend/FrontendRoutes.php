<?php

namespace Justashop\Frontend;

use Phalcon\Mvc\Router\Group as RouteGroup;

/**
 * Class Contains all routes for Frontend part
 *
 * @package Justashop\Frontend
 */
class FrontendRoutes extends RouteGroup {

    public function initialize(){

        $this->setPaths( [
            'namespace' => '\Justashop\Frontend\Controllers',
            'controller' => 'main',
            'action'     => 'index',
        ] );


        $this->add( '/api/user/:action', [
            'controller' => 'Api\User',
            'action'     => 1,
        ] )->setName('frontend.api.user');


        $this->add( '/api/cart/:action', [
            'controller' => 'Api\Cart',
            'action'     => 1,
        ] )->setName('frontend.api.cart');


        $this->add( '/', [
            'controller' => 'main',
            'action'     => 'index',
        ] )->setName('frontend.main');

        $this->add('/categories/{id:[0-9]+}-{alias:[a-zA-Z\-]+}', [
            'controller' => 'categories',
            'action'     => 'index',
            'id'         => 1,
            'alias'      => 2
        ])->setName('frontend.categories');

        $this->add('/products/{id:[0-9]+}-{alias:[a-zA-Z\-]+}', [
            'controller' => 'products',
            'action'     => 'index',
            'id'         => 1,
            'alias'      => 2
        ])->setName('frontend.product');

        $this->add('/products/search/', [
            'controller' => 'products',
            'action'     => 'index',
        ])->setName('frontend.products.search');

        $this->add('/orders/{action:[a-z]+}/{id:[0-9]+}-{hash:[A-z0-9]+}', [
            'controller' => 'orders',
        ])->setName('frontend.order');


        $this->add( '/cart/', [
            'controller' => 'Cart\Cart',
            'action'     => 'index',
        ] )->setName('frontend.cart.index');

        $this->add( '/cart/:action/?', [
            'controller' => 'Cart\Cart',
            'action'     => 1,
        ] )->setName('frontend.cart');


        $this->add( '/payment/{system:[a-zA-Z0-9]+}/{action:[a-z]+}', [
            'controller' => 'Payment',
        ] )->setName('frontend.payment');


        $this->add( '/contacts[/]{0,1}', [
            'controller' => 'static-page',
            'action'     => 'index',
            'alias'      => 'contacts',
        ] )->setName('frontend.contactsPage');


        $this->add( '/about[/]{0,1}', [
            'controller' => 'static-page',
            'action'     => 'index',
            'alias'      => 'about',
        ] )->setName('frontend.aboutPage');


        $this->add( '/static/{alias:[a-zA-Z0-9]+}[/]{0,1}', [
            'controller' => 'static-page',
        ] )->setName('frontend.staticPage');

    }

} 