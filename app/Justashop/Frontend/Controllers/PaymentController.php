<?php
namespace Justashop\Frontend\Controllers;


use Justashop\Components\Payment\Data;
use Justashop\Components\Payment\Manager;
use Justashop\Helpers\DI;
use Justashop\Helpers\Text;
use Justashop\Model\Order;
use Justashop\Model\Product;
use Justashop\Mvc\TemplateController;

class PaymentController extends TemplateController {


    public function resultAction(){
        $this->view->disable();
        /** @var Manager $paymentsManager */
        $paymentsManager = $this->getDI()->getShared(DI::PAYMENT);

        $system = $paymentsManager->get( $this->dispatcher->getParam('system') );

        $responseData = $this->request->getPost();
        if( $system->isResponseValid($responseData) ){
            $data = $system->getPaymentDataFromResponse($responseData);

            $this->response->setContent($system->responseSuccess($responseData));
        } else {
            $this->response->setContent($system->responseFail($responseData));
        }
    }


    public function successAction(){
        // show success page

    }


    public function failAction(){
        //show fail page
    }

} 