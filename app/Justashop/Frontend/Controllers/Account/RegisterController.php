<?php

namespace Justashop\Frontend\Controllers\Account;


use Justashop\Components\Account\Registration\Manager;
use Justashop\Helpers\DI;
use Justashop\Mvc\TemplateController;

class RegisterController extends TemplateController {


    public function indexAction(){

        $data = [
            'email'    => 'admin@mail.com',
            'password' => '123456',
            'passwordConfirm' => '123456'
        ];

        if( $this->request->isPost() || true ){
            /** @var Manager $regManager */
            $regManager = $this->getDI()->getShared(DI::ACCOUNT_REG);
            if($regManager->createUser($data)){
                $this->response->redirect( $this->request->getHTTPReferer() );
            }
        }
    }
}