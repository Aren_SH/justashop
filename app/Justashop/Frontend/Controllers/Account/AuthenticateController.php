<?php

namespace Justashop\Frontend\Controllers\Account;


use Justashop\Components\Account\Authentication\Manager;
use Justashop\Helpers\DI;
use Justashop\Mvc\TemplateController;

class AuthenticateController extends TemplateController {


    public function indexAction(){

        $data = [
            'login'    => 'admin@mail.com',
            'password' => '123456',
        ];

        if( $this->request->isPost() || true ){
            /** @var Manager $authManager */
            $authManager = $this->getDI()->getShared(DI::ACCOUNT_AUTH);

            $user = $authManager->getCurrentUser();
            $user = $authManager->getUserByCredentials($data['login'], $data['password']);
            if( $user ){
                $authManager->remember($user);
            }
        }
    }
}