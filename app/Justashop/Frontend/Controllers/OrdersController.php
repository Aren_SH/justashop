<?php


namespace Justashop\Frontend\Controllers;


use Justashop\Components\Payment\Data;
use Justashop\Components\Payment\Manager;
use Justashop\Helpers\DI;
use Justashop\Helpers\Text;
use Justashop\Model\Order;
use Justashop\Model\Payment\Type;
use Justashop\Model\Product;
use Justashop\Mvc\TemplateController;

class OrdersController extends TemplateController {

    public function indexAction(){}

    public function showAction(){
        $orderId = (int) $this->dispatcher->getParam('id');
        $hash    = $this->dispatcher->getParam('hash');


        if( !$orderId || !$hash ){
            $this->response->setStatusCode('404', 'Not Found');
            $this->view->disable();
            return;
        }

        $order = Order::findById($orderId);
        if( !$order || $order->hash != $hash ){
            $this->response->setStatusCode('404', 'Not Found');
            $this->view->disable();
            return;
        }


        $orderProducts = Order\Product::findByOrderId($order->id);
        $resOrderProducts = [];
        $productIds = [];
        foreach( $orderProducts as $product ){
            $resOrderProducts[$product->product_id] = $product;
            $productIds[] = (int)$product->product_id;
        }

        $products = Product::findById($productIds);
        if( !is_array($products) ) $products = [$products];

        $paymentTypes = Type::find();

        $this->view->setVar('paymentTypes', $paymentTypes);
        $this->view->setVar('order', $order);
        $this->view->setVar('orderProducts', $resOrderProducts);
        $this->view->setVar('products', $products);

    }


    public function payAction(){
        $orderId  = (int) $this->dispatcher->getParam('id');
        $hash     = $this->dispatcher->getParam('hash');
        $typeId   = (int) $this->request->getPost('typeId');


        if( !$orderId || !$hash || !$typeId ){
            $this->response->setStatusCode('404', 'Not Found');
            $this->view->disable();
            return;
        }

        /** @var Order $order */
        $order = Order::findById($orderId);
        if( !$order || $order->hash != $hash ){
            $this->response->setStatusCode('404', 'Not Found');
            $this->view->disable();
            return;
        }

        /** @var Type $paymentType */
        $paymentType = Type::findById($typeId);
        if( !$paymentType ){
            $this->response->setStatusCode('404', 'Not Found');
            $this->view->disable();
            return;
        }

        if($paymentType->adapter == Type::ADAPTER_TYPE_CACHE){
            $order->payment_type_id = $paymentType->id;
            $order->status = Order::STATUS_IN_PROGRESS;
            $order->save();

            $this->response->redirect( $order->getUrl() );
        }
        else {
            /** @var Manager $paymentsManager */
            $paymentsManager = $this->getDI()->getShared('paymentManager');
            $system = $paymentsManager->get( $paymentType->adapter );

            $data = new Data();

            $data->orderId     = $order->id;
            $data->amount      = $order->amount * $paymentType->discount;
            $data->email       = $order->user_email;
            $data->description = $this->getPaymentDescription( $order );

            $formData = $system->getRequestData($data);
            $this->view->setVar('url',  $system->getRequestUrl());
            $this->view->setVar('data', $formData);
        }
    }


    private function getPaymentDescription( $order ){
        $t = $this->getDI()->getShared(DI::TRANSLATOR);

        $products = Order\Product::findByOrderId($order->id);
        $products = $products->toArray();
        $first = array_shift($products);
        $qty = 0;
        /** @var Order\Product[] $products */
        foreach( $products as $prod ){
            $qty += $prod->quantity;
        }

        /** @var Product $product */
        $text    = $t->_('patterns.payment', count( $products ) < 1 ? 'title-single' : 'title-multiple');

        $product = Product::findById($first->product_id);
        $params  = [
            ':title:'    => $product->title,
            ':count:'    => $qty + $first->count - 1,
            ':products:' => $t->declension('product', $qty ),
            ':amount:'   => Text::money_format($order->amount),
        ];

        return str_replace(array_keys($params), $params, $text);

    }
} 