<?php

namespace Justashop\Frontend\Controllers;


use Justashop\Controller\Behaviour\WithMetaTags;
use Justashop\Helpers\Text;
use Justashop\Model\Page;
use Justashop\Mvc\TemplateController;

class StaticPageController extends TemplateController {

    public function indexAction(){
        $alias = $this->dispatcher->getParam('alias');
        if( !$alias ){
            $this->response->setStatusCode(404, 'Page not found');
            return;
        }

        $page = Page::findByAlias($alias);
        if( !$page ){
            $this->response->setStatusCode(404, 'Page not found');
            return;
        }

        $this->view->setVar('page', $page);

        $this->addBehaviour(new WithMetaTags([
            'metaTagId'   => $page->meta_tag_id,
            'titlePrefix' => $this->_title,
            'defaults'  => [
                'title'       => $page->title,
                'description' => Text::cut( $page->content, 150),
            ]
        ]));
    }

} 