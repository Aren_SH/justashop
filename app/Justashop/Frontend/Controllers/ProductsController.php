<?php

namespace Justashop\Frontend\Controllers;


use Justashop\Controller\Behaviour\WithCategories;
use Justashop\Controller\Behaviour\WithMetaTags;
use Justashop\Helpers\Text;
use Justashop\Model\Product;
use Justashop\Mvc\TemplateController;

class ProductsController extends TemplateController {


    public function indexAction(){
        $mainPageUrl = $this->url->get(['for' => 'frontend.main']);
        $id = (int) $this->dispatcher->getParam('id');
        if( !$id ){
            $this->response->redirect($mainPageUrl);
            return;
        }

        /** @var Product $product */
        $product = Product::findById($id);
        if( !$product ){
            $this->response->redirect($mainPageUrl);
            return;
        }

        $alias = $this->dispatcher->getParam('alias');
        if( $alias != Text::slug_format( $product->title ) ){
            $this->response->redirect($product->getUrl(), null, 301);
            return;
        }

        $images = Product\Image::findByProductId($product->id)->toArray();
        $this->view->setVar('product', $product);
        $this->view->setVar('images',  $images);

        $this->addBehaviour(new WithCategories(['currentId' => $product->category_id]));
        $this->addBehaviour(new WithMetaTags([
            'metaTagId'   => $product->meta_tag_id,
            'titlePrefix' => $this->_title,
            'defaults'  => [
                'title'       => $product->title,
                'description' => Text::cut( $product->description, 150),
            ]
        ]));

    }
}