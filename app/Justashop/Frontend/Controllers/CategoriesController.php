<?php

namespace Justashop\Frontend\Controllers;


use Justashop\Components\Messages\Translator;
use Justashop\Controller\Behaviour\WithCategories;
use Justashop\Controller\Behaviour\WithMetaTags;
use Justashop\Helpers\DI;
use Justashop\Helpers\Text;
use Justashop\Model\Category;
use Justashop\Model\Product;
use Justashop\Mvc\Model\Query\Simple;
use Justashop\Mvc\TemplateController;
use Justashop\Paginator\Adapter\Precomputed;

class CategoriesController extends TemplateController {

    protected $_pageSize = 20;

    protected $_sortFields = ['title', 'price', 'createdAt'];

    protected $_defaultSortField = 'createdAt';

    protected $_defaultSortOrder = 'desc';

    public function indexAction(){
        $pageNumber = $this->_getPageNumber();
        $sort       = $this->_getSortParams();
        $sortFields = $this->_getSortFields( $sort );

        $mainPageUrl = $this->url->get(['for' => 'frontend.main']);

        $id = (int) $this->dispatcher->getParam('id');
        if( !$id ){
            $this->response->redirect($mainPageUrl);
            return;
        }

        /** @var Category $category */
        $category = Category::findById($id);
        if( !$category ){
            $this->response->redirect($mainPageUrl);
            return;
        }

        $alias = $this->dispatcher->getParam('alias');
        if( $alias != Text::slug_format( $category->title ) ){
            $this->response->redirect($category->getUrl(), null, 301);
            return;
        }

        $products = Product::findWithRelations([
            'conditions' => 'category_id = :id:',
            'bind'       => ['id' => $category->id],
            'bindTypes'  => ['id' => Simple::BIND_PARAM_INT],
            'limit'      => ['number' => $this->_pageSize, 'offset' => ($pageNumber - 1) * $this->_pageSize],
            'order'      => sprintf('"%s" %s', $sort['field'], $sort['order'] )
        ]);
        $productsQty = Product::count([
            'conditions' => 'category_id = :id:',
            'bind'       => ['id' => $category->id],
            'bindTypes'  => ['id' => Simple::BIND_PARAM_INT]
        ]);

        $pagination = new Precomputed([
            'data'  => $products->toArray(),
            'total' => $productsQty,
            'page'  => $pageNumber,
            'limit' => $this->_pageSize,
        ]);

        $page = $pagination->getPaginate();

        $this->view->setVar('category', $category);
        $this->view->setVar('page', $page);

        $this->view->setVar('sort',       $sort);
        $this->view->setVar('sortFields', $sortFields);


        $this->addBehaviour(new WithCategories([
            'currentId' => $id,
        ]));

        $this->addBehaviour(new WithMetaTags([
            'metaTagId'   => $category->meta_tag_id,
            'titlePrefix' => $this->_title,
            'defaults'    => [
                'title'       => $category->title,
                'description' => Text::cut( $category->description, 150),
            ]
        ]));
    }


    private function _getPageNumber(){
        $pageNumber = $this->request->get('page', 'int', 1);

        if( !$pageNumber || $pageNumber <= 0 ) $pageNumber = 1;

        return $pageNumber;
    }


    /**
     * @return array
     */
    private function _getSortParams(){

        $result = [
            'field' => $this->_defaultSortField,
            'order' => $this->_defaultSortOrder,
        ];

        $sort = $this->request->get('sort');

        if(!$sort || !is_array($sort)) return $result;

        if(!empty( $sort['field']) && in_array( $sort['field'], $this->_sortFields ) ){
            $result['field'] = $sort['field'];
        }

        if(!empty( $sort['order']) && in_array( strtolower($sort['order']), ['asc', 'desc'])){
            $result['order'] = strtolower($sort['order']);
        }

        return $result;

    }


    private function _getSortFields( $sort ){
        /** @var Translator $t */
        $t = $this->getDI()->getShared(DI::TRANSLATOR);
        $res = [];
        foreach( $this->_sortFields as $field ){
            $res[$field] = [
                'title'     => $t->_('sort.fields', $field),
                'order'     => $field == $sort['field'] ? $sort['order'] : 'none',
                'nextOrder' => $field == $sort['field'] ? ( $sort['order'] == 'asc' ? 'desc' : 'asc') : 'asc'
            ];
        }

        return $res;
    }


}