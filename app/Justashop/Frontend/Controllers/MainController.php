<?php

namespace Justashop\Frontend\Controllers;

use Justashop\Model\Product;
use \Justashop\Mvc\TemplateController;
use \Justashop\Controller\Behaviour\WithCategories;


class MainController extends TemplateController {

    public function indexAction(){
        $this->addBehaviour(new WithCategories(['afterExecuteRoute' => []]));
        $popular = Product::findPopular(3)->toArray();

        $maskIds = [];
        array_map(function($n) use(&$maskIds){ $maskIds[] = (int)$n->id;}, $popular);

        $new = Product::findNew(3, $maskIds)->toArray();

        $this->view->setVar("popularProducts", $popular);
        $this->view->setVar("newProducts", $new);
    }

}