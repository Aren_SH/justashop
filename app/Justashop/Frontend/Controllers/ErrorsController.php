<?php

namespace Justashop\Frontend\Controllers;


use Justashop\Mvc\Controller;

class ErrorsController extends Controller {

    public function error404Action(){
        $this->view->pick('errors/404');
    }
} 