<?php


namespace Justashop\Frontend\Controllers\Cart;


use Justashop\Components\Account;
use Justashop\Components\Cart;
use Justashop\Components\Mail;
use Justashop\Components\Parameters;
use Justashop\Frontend\Form\CheckoutForm;
use Justashop\Model\Order;
use Justashop\Model\Product;
use Justashop\Mvc\Model\Query\Simple;
use Phalcon\Mvc\Controller;

class CartController extends Controller{



    public function indexAction(){

        $cart = new Cart();

        $cart->loadFromSession();

        $this->view->setVar('cart', $cart);
        $this->view->setVar('_cartDisabled', true );

        if ($this->request->isPost()){
            $this->_setCartFromRequest($cart);

            if ($cart->count()){
                $this->redirectToCheckoutPage();
                return;
            } else {
                $this->redirectToNoProductsPage();
            }

        }
    }


    public function checkoutAction(){

        /** @var Account $account */
        $account = $this->getDI()->getShared('accountManager');
        $cart    = $this->getCart( $this->request->isPost() );
        /** @var CheckoutForm $form */
        $form    = CheckoutForm::Factory();
        $form->setCart( $cart );
        if( $account->getCurrentUser() ){
            //$this->_setAdressesToView();
        }

        if( $this->request->isPost() ){
            $form->handleRequest();
            if( $form->validate() && $form->save() ) {
                $order = $form->getOrder();
                $this->view->disable();
                $this->_sendNotification( $order, $cart );
                $this->_clearCart();
                $this->response->redirect( $order->getUrl() );

                return;
            };
        }

        $renderer = $this->getDI()->getShared('formBuilder')->renderer( $form);
        $this->view->setVar('form', $renderer);
        $this->view->setVar('cart', $cart );
        $this->view->setVar('_cartDisabled', true );
        $this->view->setVar('referrer', $this->request->get('referrer', null, '/'));

    }

    /**
     * @param Cart $cart
     */
    private function _setCartFromRequest($cart){
        $cart->clear();

        $requestCart = $this->request->getPost('cart');
        $productIds  = array_map( function( $n ){
            return (int)$n['id'];
        }, $requestCart );

        /** @var Product[] $products */
        $products = Product::findWithRelations([
            'conditions' => 'id in (:ids:)',
            'bind'       => ['ids' => $productIds ],
            'bindTypes'  => ['ids' => Simple::BIND_PARAM_INT_LIST ]
        ]);


        foreach( $products as $product ){
            $item = $cart->create($product->id, $product->price, $requestCart[$product->id]['count']);
            $item->setTitle( $product->title);
            $item->setThumbnail($product->getImageUrl('xs'));
            $cart->add($item);
        }
    }

    /**
     *
     */
    private function redirectToCheckoutPage(){
        $url = $this->url->get([
            'for'    => 'frontend.cart',
            'action' => 'checkout',
        ]);

        $this->response->redirect($url, true);
    }


    /**
     * @param  bool $isPost
     * @return Cart
     */
    private function getCart( $isPost = false ){
        $checkoutCart = new Cart( 'checkout-cart' );
        $checkoutCart->loadFromSession();
        if( !$isPost ){
            $defaultCart = new Cart();
            $defaultCart->loadFromSession();

            $checkoutCart->clear();
            foreach($defaultCart as $item){
                $checkoutCart->add($item);
            }
            $checkoutCart->storeToSession();
        }

        return $checkoutCart;

    }


    /**
     * Clear default cart
     */
    private function _clearCart(){
        $defaultCart = new Cart();
        $defaultCart->loadFromSession();
        $defaultCart->clear();
        $defaultCart->storeToSession();
    }



    /**
     * @param Order $order
     * @param Cart  $cart
     */
    private function _sendNotification( $order, $cart ){
        /** @var Mail $mailer */
        $mailer = $this->getDI()->getShared('mailer');
        /** @var Parameters $params */
        $params = $this->getDI()->getShared('parameters');

        $mail = $mailer->fromView('checkout/success', ['order' => $order, 'cart' => $cart]);

        $mail->setFrom($params->getParameter('site.email', 'support@shop'), $params->getParameter('site.email.alias'));
        $mail->addAddress($order->user_email);
        $mail->isHTML( true );
        $mail->send();
    }
} 