<?php

namespace Justashop\Frontend\Controllers\Api;

use Justashop\Components\Messages\Translator;
use Justashop\Helpers\DI;
use Phalcon\Mvc\Controller;

use Justashop\Components\Account;
use Phalcon\Validation\Message;

class UserController extends Controller {


    public function getCurrentAction() {
        /** @var Account $account */
        $account = $this->getDi()->getShared('accountManager');

        $this->view->disable();
        $this->response->setJsonContent(
            ['status' => 'success', 'user' =>$this->getUserData( $account->getCurrentUser())]
        );

        return $this->response;
    }


    public function logInAction(){

        if( !$this->request->isPost()){
            return $this->returnJson(['status' => 'error']);
        }

        /** @var Account $account */
        $account = $this->getDi()->getShared(DI::ACCOUNT);
        /** @var Translator $t */
        $t       = $this->getDI()->getShared(DI::TRANSLATOR);

        $login   = $this->request->getPost('login');
        $pass    = $this->request->getPost('password');

        if( !$login || !$pass ){
            return $this->returnJson(
                ['status' => 'error', 'error' => $t->_('errors.user', 'Invalid login or password' )]
            );
        }

        $user = $account->getUserByCredentials($login, $pass);
        if(!$user){
            return $this->returnJson(
                ['status' => 'error', 'error' => $t->_('errors.user', 'Invalid login or password' )]
            );
        }

        if($user->isNotVerified()){
            $account->sendActivationNotification($user);
        } else {
            $account->logIn($user);
        }

        return $this->returnJson(['status' => 'success', 'user' => $this->getUserData($user)]);


    }


    public function logOutAction(){
        /** @var Account $account */
        $account = $this->getDi()->getShared('accountManager');

        $user = $account->getCurrentUser();
        $account->logOut( $user );

        return $this->returnJson(['status' => 'success']);
    }


    public function registerAction(){
        /** @var Account $account */
        $account = $this->getDi()->getShared('accountManager');

        $data = $this->request->getPost();

        $user = $account->register($data);
        if($user){
            return $this->returnJson(['status' => 'success', 'user' => $this->getUserData($user)]);
        } else {
            return $this->returnJson(['status' => 'error', 'errors' => $this->messagesToArray( $account->getRegistrationMessages() )]);
        }
    }


    public function activateAction(){
        /** @var Account $account */
        $account = $this->getDi()->getShared('accountManager');

        $data = $this->request->getPost();

        $user = $account->activate($data['token']);

        if($user){
            $account->logIn($user);
            return $this->returnJson(['status' => 'success', 'user' => $this->getUserData($user)]);
        } else {
            return $this->returnJson(['status' => 'error', 'errors' => ['token' => 'invalid token']]);
        }
    }


    private function returnJson( $data ){
        $this->view->disable();
        $this->response->setJsonContent($data);

        return $this->response;
    }


    /**
     * @param  Account\Model\User $user
     * @return array|null
     */
    private function getUserData($user){
        if( !$user ){
            return null;
        }

        return [
            'id'        => $user->id,
            'login'     => $user->login,
            'email'     => $user->email,
            'status'    => $user->status,
            'createdAt' => $user->createdAt,
        ];
    }

    /**
     * @param  \Phalcon\Validation\Message\Group $messages
     * @return array
     */
    private function messagesToArray($messages){
        /** @var Translator $t */
        $t       = $this->getDI()->getShared(DI::TRANSLATOR);
        $result = [];
        foreach($messages as $message){
            /** @var Message $message */
            $result[$message->getField()] = $t->_('errors.user.' ,$message->getMessage());
        }

        return $result;
    }

}