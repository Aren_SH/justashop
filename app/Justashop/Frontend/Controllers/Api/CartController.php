<?php

namespace Justashop\Frontend\Controllers\Api;


use Justashop\Components\Cart;
use Justashop\Model\Product\Image;
use Justashop\Model\Product;
use Phalcon\Mvc\Controller;


class CartController extends Controller{


    public function addAction(){
        $product_id = $this->request->getPost('product_id', 'int', 0);
        $qty        = $this->request->getPost('count', 'int', 1);

        $cart = new Cart();
        $cart->loadFromSession();
        $cartItem = $cart->get($product_id);
        if($cartItem){
            $cartItem->setQuantity($cartItem->getQuantity() + $qty);
        } else {
            $cartItem = $this->createCartItemFromProductId($cart, $product_id, $qty);
        }

        if(!$cartItem){
            return $this->returnJson(['status' => 'error', 'errors' => ['Product not exists']]);
        }


        $cart->add($cartItem);
        $cart->storeToSession();

        return $this->returnJson(['status' => 'success', 'items' => [$this->_getItemData($cartItem)]]);
    }


    public function setAction(){
        $product_id = $this->request->getPost('product_id', 'int', 0);
        $qty        = $this->request->getPost('count', 'int', 1);

        $cart = new Cart();
        $cart->loadFromSession();
        $cartItem = $cart->get($product_id);

        if(!$cartItem){
            return $this->returnJson(['status' => 'error', 'errors' => ['Product not exists']]);
        }

        $cartItem->setQuantity($qty);

        $cart->add($cartItem);
        $cart->storeToSession();

        return $this->returnJson(['status' => 'success', 'items' => [$this->_getItemData($cartItem)]]);
    }


    public function removeAction(){
        $product_id = $this->request->getPost('product_id', 'int', 0);
        $qty        = $this->request->getPost('count', 'int', 1);

        $cart = new Cart();
        $cart->loadFromSession();

        $cartItem = $cart->get($product_id);
        if($cartItem && $cartItem->getQuantity() > $qty){
            $cartItem->setQuantity($cartItem->getQuantity() - $qty);
            $cart->storeToSession();
        }

        elseif($cartItem){
            $cart->delete($cartItem->getId());
            $cart->storeToSession();
        }

        $items = [];
        foreach($cart as $cartItem){
            /** @var Cart\Item $cartItem */
            $items[] = $this->_getItemData($cartItem);
        }

        return $this->returnJson(['status' => 'success', 'items' => $items]);
    }


    public function listAction(){
        $cart = new Cart();
        $cart->loadFromSession();

        $items = [];
        foreach($cart as $cartItem){
            /** @var Cart\Item $cartItem */
            $items[] = $this->_getItemData($cartItem);
        }

        return $this->returnJson(['status' => 'success', 'items' => $items]);
    }


    public function clearAction(){
        $cart = new Cart();

        $cart->loadFromSession();
        $cart->clear();
        $cart->storeToSession();

        return $this->returnJson(['status' => 'success', 'items' => []]);
    }


    /**
     * @param Cart $cart
     * @param int  $product_id
     * @param int  $qty
     * @return Cart\Item
     */
    protected function createCartItemFromProductId($cart, $product_id, $qty){
        $product = Product::findById((int)$product_id);

        if(!$product){
            return null;
        }

        $cartItem = $cart->create($product->id, $product->price, $qty);
        $cartItem->setTitle($product->title);
        $cartItem->setUrl($product->getUrl());
        $cartItem->setThumbnail($product->getImageUrl('s'));

        return $cartItem;
    }


    /**
     * @param  Cart\Item $cartItem
     * @return array
     */
    protected function _getItemData($cartItem){
        if(!$cartItem){
            return null;
        }

        return [
            'id'         => $cartItem->getId(),
            'thumbnail'  => $cartItem->getThumbnail(),
            'title'      => $cartItem->getTitle(),
            'url'        => $cartItem->getUrl(),
            'price'      => $cartItem->getPrice(),
            'count'      => $cartItem->getQuantity(),
            'totalPrice' => $cartItem->getTotalPrice(),
            'discount'   => $cartItem->getDiscount(),
        ];
    }


    private function returnJson($data){
        $this->view->disable();
        $this->response->setJsonContent($data);

        return $this->response;
    }
} 