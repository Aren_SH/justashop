<?php

namespace Justashop\Controller\Behaviour;


use Justashop\Model\Category;
use Phalcon\Events\Event;
use Phalcon\Mvc\Dispatcher;

class WithCategories {

    private $_currentCategoryId;


     public function __construct( $params = null ){
         if( $params && !empty( $params['currentId'])){
             $this->_currentCategoryId = $params['currentId'];
         }
     }

    /**
     * @param Event      $event
     * @param Dispatcher $dispatcher
     * @param \mixed     $data
     */
    public function afterDispatch($event, $dispatcher, $data){
        $this->notify($dispatcher, $data);
    }


    /**
     * @param Dispatcher $dispatcher
     */
    public function notify( $dispatcher ){

        $controller = $dispatcher->getActiveController();

        $categories = Category::find();

        $controller->view->setVar('_categories',        $categories);
        $controller->view->setVar('_currentCategoryId', $this->_currentCategoryId);
    }
} 