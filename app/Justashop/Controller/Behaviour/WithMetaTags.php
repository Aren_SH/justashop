<?php

namespace Justashop\Controller\Behaviour;


use Justashop\Model\MetaTag;

class WithMetaTags {

    private $_metaTagId;

    private $_titlePrefix = '';

    private $_defaults = [
        'title'       => null,
        'description' => null,
        'keywords'    => null,
        'robots'      => null,
    ];


    public function __construct( $params = null ){
        if( $params && !empty( $params['metaTagId'])){
            $this->_metaTagId = $params['metaTagId'];
        }

        if( $params && !empty( $params['defaults'] )){
            $this->_defaults = array_merge( $this->_defaults, $params['defaults'] );
        }

        if( $params && !empty( $params['titlePrefix'] )){
            $this->_titlePrefix = $params['titlePrefix'];
        }
    }

    /**
     * @param Event      $event
     * @param Dispatcher $dispatcher
     * @param \mixed     $data
     */
    public function afterDispatch($event, $dispatcher, $data){
        $this->notify($dispatcher, $data);
    }


    /**
     * @param Dispatcher $dispatcher
     */
    public function notify( $dispatcher ){

        $controller = $dispatcher->getActiveController();

        $metaTag = $this->_metaTagId ? MetaTag::findById($this->_metaTagId) : null;

        if( !$metaTag ) {
            $metaTag = new MetaTag();
        }

        $title = join(' - ', array_filter([$this->_titlePrefix, $metaTag->title ?: $this->_defaults['title']]));

        $controller->view->setVar('_metaTitle',       $title);
        $controller->view->setVar('_metaDescription', $metaTag->description ?: $this->_defaults['description']);
        $controller->view->setVar('_metaKeywords',    $metaTag->keywords    ?: $this->_defaults['keywords']);
        $controller->view->setVar('_metaRobots',      $metaTag->robots      ?: $this->_defaults['robots']);
    }
} 