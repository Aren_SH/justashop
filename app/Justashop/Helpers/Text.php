<?php

namespace Justashop\Helpers;


class Text {



    public static function money_format( $val ){
        return number_format($val, 2, ',', ' ');
    }

    public static function slug_format( $text ){
        $t = \Transliterator::create("Any-Latin; NFD; [:Nonspacing Mark:] Remove; NFC; [:Punctuation:] Remove; Lower();");
        $slug = $t->transliterate( $text );

        return $slug ? trim(preg_replace('/[^a-z]+/', '-', $slug), '- ') : '';
    }


    public static function controllerAlias($text){
        return str_replace('_', '-', \Phalcon\Text::uncamelize( $text ) );
    }


    public static function cut( $text, $length, $suffix = '...' ){
        if( mb_strlen($text) <= $length ){
            return $text;
        }

        $pos = mb_strrpos($text, ' ', mb_strlen($text) - $length);

        return mb_substr($text, 0, $pos ) . $suffix;
    }
} 