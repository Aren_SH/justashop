<?php

namespace Justashop\Helpers;

use \Justashop\Components;
use Phalcon\Db;
use Phalcon\DI\FactoryDefault\CLI;
use Phalcon\Http\Response\Cookies;
use Phalcon\Mvc\View;


class DI{

    const EVENTS_MANAGER = 'eventsManager';
    const MODELS_MANAGER = 'modelsManager';

    const DISPATCHER     = 'dispatcher';
    const ROUTER         = 'router';

    const REQUEST        = 'request';
    const RESPONSE       = 'response';

    const VIEW           = 'view';
    const SIMPLE_VIEW    = 'simpleView';

    const DB             = 'db';
    const SESSION        = 'session';
    const CACHE          = 'cache';

    const PARAMETERS   = 'parameters';
    const BREADCRUMBS  = 'breadcrumbs';

    const FORM_BUILDER = 'formBuilder';

    const TRANSLATOR   = 'translator';

    const ACCOUNT      = 'accountManager';

    const PAYMENT      = 'paymentManager';


    /**
     * @param \Phalcon\DI $di
     */
    public static function configureServices($di){
        /** @var Cookies $cookies */
        $cookies = $di->getShared('cookies');
        $cookies->useEncryption( false );
    }


    /**
     * @param \Phalcon\DI $di
     */
    public static function registerAdditionalServices($di){

        $di->getShared('dispatcher')->setEventsManager($di->getShared('eventsManager'));
        //self::setDispatcherErrorshandler($di);


        self::registerCache($di);
        self::registerDatabase($di);
        self::registerParametersHelper($di);
        self::registerAuthManager($di);
        self::registerBreadCrumbs($di);
        self::registerTranslator($di);
        self::registerURL($di);
        self::registerTemplateView($di);
        self::registerFormBuilder($di);
        self::registerImageManager($di);
        self::registerMailer($di);
        self::registerPaymentManager($di);
    }


    public static function registerCLIServices($di){
        self::registerCache($di);
        self::registerDatabase($di);
        self::registerParametersHelper($di);
        self::registerAuthManager($di);
        self::registerImageManager($di);
        self::registerMailer($di);
        self::registerDbPatchManager($di);

    }




    /**
     * @param \Phalcon\DI $di
     */
    public static function setDispatcherErrorsHandler($di){

        $di->getShared('eventsManager')->attach("dispatch:beforeException", function($event, $dispatcher, $exception) {
            //Handle 404 exceptions
            if ($exception instanceof \Phalcon\Mvc\Dispatcher\Exception) {
                $dispatcher->forward(array(
                    'controller' => 'Justashop\\Frontend\\Controllers\\Errors',
                    'action'     => 'show404'
                ));
                return false;
            }

            return true;
        });
    }



    /**
     * @param \Phalcon\DI $di
     */
    public static function registerCache($di){
        $config = new \Phalcon\Config\Adapter\Json(CONF_PATH . 'cache.json');

        if(!$config->offsetExists('host')){
            return;
        }

        $backend      = ucfirst($config->get('backend', 'memcache'));
        $backendClass = '\Phalcon\Cache\Backend\\' . $backend;

        $frontend      = ucfirst($config->get('frontend', 'data'));
        $frontendClass = '\Phalcon\Cache\Frontend\\' . $frontend;

        if(!class_exists($backendClass) || !class_exists($frontendClass)){
            return;
        }

        $frontCache = new $frontendClass ([
            "lifetime" => $config->get('lifetime', 3600)
        ]);

        $cache = new $backendClass($frontCache, $config->toArray());

        $di->setShared(self::CACHE, $cache);
    }


    /**
     * @param \Phalcon\DI $di
     */
    public static function registerTemplateView($di){
        $view = new \Justashop\Mvc\TemplateView();
        $view->setViewsDir(APP_PATH . 'views' . DS);

        $view->setVar('t', $di->getShared(DI::TRANSLATOR));

        $di->setShared(self::VIEW, $view);

        $simpleView = new View\Simple();
        $di->setShared(self::SIMPLE_VIEW, $simpleView);
    }


    /**
     * @param \Phalcon\DI $di
     */
    public static function registerParametersHelper($di){
        $parameters = new Components\Parameters();

        $di->setShared(self::PARAMETERS, $parameters);
    }


    /**
     * @param \Phalcon\DI $di
     */
    public static function registerDatabase($di){
        $config = new \Phalcon\Config\Adapter\Json(CONF_PATH . 'database.json');

        if(!$config->offsetExists('host')){
            return;
        }

        $db = new \Phalcon\Db\Adapter\Pdo\Postgresql($config->toArray());

        $eventsManager = $di->get('eventsManager');

        $logger = new \Phalcon\Logger\Adapter\File(LOG_PATH . 'sql.log');

        $db->setEventsManager($eventsManager);

        $di->setShared(self::DB, $db);
    }

    /**
     * @param \Phalcon\DI $di
     */

    public static function registerURL($di){
        /** @var \Phalcon\Mvc\Url $url */
        $url = $di->getShared('url');
        $url->setBaseUri(sprintf('http://%s/', trim($_SERVER['HTTP_HOST'], ' \\/')));
    }


    /**
     * @param \Phalcon\DI $di
     */
    public static function registerFormBuilder($di){
        $builder = new Components\Former\Builder();

        $builder->setViewsPath(APP_PATH . 'views' . DIRECTORY_SEPARATOR . 'former' . DIRECTORY_SEPARATOR);

        $di->setShared(self::FORM_BUILDER, $builder);
    }


    /**
     * @param \Phalcon\DI $di
     */
    public static function registerBreadCrumbs($di){
        $di->set(self::BREADCRUMBS, new \Justashop\Components\BreadCrumbs());
    }


    /**
     * @param \Phalcon\DI $di
     */
    public static function registerTranslator($di){
        $translator = new Components\Messages\Translator();

        $translator->setMessagesPath(APP_PATH . 'L10n' . DIRECTORY_SEPARATOR);
        $translator->setLanguage('ru_ru');

        $di->setShared(self::TRANSLATOR, $translator);
    }


    /**
     * @param \Phalcon\DI $di
     */
    public static function registerAuthManager($di){
        $config  = new \Phalcon\Config\Adapter\Json(CONF_PATH . 'account.json');
        $account = new \Justashop\Components\Account($config);

        $di->setShared(self::ACCOUNT, $account);
    }


    /**
     * @param \Phalcon\DI $di
     */
    public static function registerImageManager($di){
        $config  = new \Phalcon\Config\Adapter\Json(CONF_PATH . 'image-profiles.json');
        $manager = new \Justashop\Components\Image\Manager();

        $manager->setProfiles($config);
        $manager->setRootDir(DOC_ROOT . join( DIRECTORY_SEPARATOR, ['uploads','images', '']));
        $manager->setBaseUrl( $di->getShared('url')->getBaseUri() . 'uploads/images/');

        $di->setShared('imageManager', $manager);
    }


    /**
     * @param \Phalcon\DI $di
     */
    public static function registerMailer($di){
        $mailer = new Components\Mail();
        $mailer->setViewPath( join(DIRECTORY_SEPARATOR, [APP_PATH, 'views', 'mail', ''] ) );
        $di->setShared('mailer', $mailer);
    }


    /**
     * @param \Phalcon\DI $di
     */
    public static function registerPaymentManager($di){
        $config  = new \Phalcon\Config\Adapter\Json(CONF_PATH . 'payment-systems.json');
        $manager = new \Justashop\Components\Payment\Manager();

        foreach( $config as $name => $params ){
            if( !$params['class'] || !$params['config'] ) continue;

            try {
                $config = new \Phalcon\Config\Adapter\Json(CONF_PATH . $params['config'] );
                $system = new $params['class']($config);

                $manager->register($name, $system);
            } catch (\Exception $e){

            }

        }

        $di->setShared('paymentManager', $manager);
    }


    /**
     * @param \Phalcon\DI $di
     */
    public static function registerDbPatchManager($di){
        $patchManager = new Components\DB\PatchManager(['path' => CLI_PATH . 'patches' . DS]);
        $patchManager->setConnection($di->getShared(self::DB));

        $di->setShared('patchManager', $patchManager);

    }
} 