<?php

namespace Justashop\Helpers;

class Url {

    /**
     * @var array URL parts
     */
    protected $_parts = array(
        "scheme"   => '',
        "host"     => '',
        'port'     => '',
        "user"     => '',
        "pass"     => '',
        "path"     => '',
        "fragment" => '',
    );

    /**
     * @var array query params
     */
    protected $_params;


    /**
     * Initialize new URLHelper
     *
     * @param string $url
     * @param array $params additional params
     * @return Url
     */
    public static function factory($url, $params = array())
    {
        $parts = parse_url($url);
        $queryStr = '';


        if(array_key_exists('query', $parts)) {
            $queryStr = $parts['query'];
            unset( $parts['query'] );
        }

        if($queryStr)
        {
            parse_str($queryStr, $params);
        }

        return new Url($parts, $params);

    }

    protected function __construct ($parts, $params)
    {
        $this->_parts = array_merge($this->_parts, $parts);

        $this->_params = !empty($params) ? $params : array();
    }

    /**
     * Set or replace query param
     *
     * @param string $key
     * @param mixed $value
     * @return $this chaining
     */
    public function setQueryParam($key, $value) {
        $this->_params[$key] = $value;
        return $this;
    }

    /**
     * Set or replace url part
     *
     * @param string $key
     * @param string|int $value
     * @return $this chaining
     */
    public function setUrlParam($key, $value) {
        if (array_key_exists($key, $this->_parts)) {
            $this->_parts[$key] = $value;
        }

        return $this;
    }

    /**
     * Get compiled URL string
     *
     * @return string
     */
    public function compile() {
        $result = ( !empty( $this->_parts['scheme'] ) ? $this->_parts['scheme'] : 'http' ) . '://';

        if( !empty( $this->_parts['user'] ) ) {
            $result .= $this->_parts['user'] . ':' . $this->_parts['pass'] . '@';
        }

        $result .= $this->_parts['host'] . $this->_parts['path'];

        array_filter($this->_params);

        if( !empty( $this->_params ) ) {
            $result .= '?' . http_build_query($this->_params);
        }

        if(!empty( $this->_parts['fragment'] ) ) {
            $result .= '#' . $this->_parts['fragment'];
        }

        return $result;
    }


}
