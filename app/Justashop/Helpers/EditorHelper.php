<?php

namespace Justashop\Helpers;


class EditorHelper {



    public static function getColumnTitle( $column, $default = null ){
        $title = null;
        if( !is_array( $column ) ){
            $title = $column;
        }
        elseif( array_key_exists( 'title', $column ) ){
            $title = $column['title'];
        }

        return $title ?: $default;
    }
} 