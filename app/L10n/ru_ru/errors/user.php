<?php
return [
    'Email already exists'                 => 'Email уже используется другим пользователем',
    'Login required'                       => 'Вы должны указать логин',
    'Login too long'                       => 'Логин слишком длинным',
    'Login too short'                      => 'Логин слишком короткий',
    'Password required'                    => 'Вы должны указать пароль',
    'Password too short'                   => 'Слишком короткий пароль',
    'Password too long'                    => 'Слишком длинный пароль',
    'Password doesn\'t match confirmation' => 'Пароль не совпадает с подтвержением',
    'Invalid login or password'            => 'Пользователь с таким логином и паролем не найден',
];