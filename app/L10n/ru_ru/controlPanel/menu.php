<?php
return [
    'main'       => 'Панель управления',
    'users'      => 'Пользователи',
    'products'   => 'Товары',
    'categories' => 'Категории',
    'parameters' => 'Параметры сайта',
    'orders'     => 'Заказы',
    'metaTags'   => 'Мета теги',
    'pages'      => 'Страницы',
    'default'    => 'Основные',
];