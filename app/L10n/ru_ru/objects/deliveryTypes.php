<?php

return [
    'page-title'      => 'Способы доставки',
    'page-title-list' => 'Список способов доставки',
    'page-title-edit' => 'Редактирование способа доставки',
    'page-title-add'  => 'Добавление способа доставки',

    'fields' => [
        'title'       => 'Название',
        'description' => 'Описание',
        'price'       => 'Цена',
    ],

    'validation' => [
            'title too long' => 'Слишком длинный заголовок',
    ],

    'help' => [
        'price' => 'Цена за доставку.'
    ]

];