<?php

return [
    'page-title'      => 'Заказы',
    'page-title-list' => 'Список заказов',
    'page-title-edit' => 'Редактирование заказа',
    'page-title-add'  => 'Добавление заказа',

    'fields' => [
        'url'          => 'Ссылка',
        'user_name'    => 'ФИО покупателя',
        'user_phone'   => 'Номер телефона',
        'user_email'   => 'Email',
        'address'      => 'Адрес',
        'deliveryType' => 'Способ доставки',
        'paymentType'  => 'Способ оплаты',
        'amount'       => 'Сумма',
        'status'       => 'Статус',
    ],

    'validation' => [
            'status not in domain' => 'Статуса нет в списке',
    ],

    'statuses' => [
        'status1' => 'Добавлен',
        'status2' => 'Отклонен',
        'status3' => 'Ожидает оплаты',
        'status4' => 'Платеж в обработке',
        'status5' => 'Оплачен',
        'status6' => 'В обработке',
        'status7' => 'Ожидает отправки покупателю',
        'status8' => 'Отправлен покупателю',
        'status9' => 'Завершен',

    ],

];