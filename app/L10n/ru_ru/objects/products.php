<?php

return [
    'page-title'      => 'Товары',
    'page-title-list' => 'Список Товаров',
    'page-title-edit' => 'Редактирование товара',
    'page-title-add'  => 'Добавление товара',

    'fields' => [
        'sku'         => 'Ариткул',
        'title'       => 'Наименование',
        'price'       => 'Цена',
        'description' => 'Описание',
        'category'    => 'Категория',
        'images'      => 'Изображения',
        'createdAt'   => 'Дата добавления'
    ],

    'validation' => [
            'title too long' => 'Слишком длинный заголовок',
    ],

];