<?php

return [
    'page-title'      => 'Мета теги',
    'page-title-list' => 'Список мета тегов',
    'page-title-edit' => 'Редактирование мета тегов',
    'page-title-add'  => 'Добавление мета тегов',

    'fields' => [
        'title'       => 'Название',
        'description' => 'Описание',
        'keywords'    => 'Ключевые слова',
        'robots'      => 'Robots',
    ],

    'validation' => [
            'title too long'       => 'Слишком длинный заголовок',
            'description too long' => 'Слишком длинное описание',
            'keywords too long'    => 'Слишком много ключевых слов',
    ],

    'help' => [
        'title'       => 'Название страницы (тег &lt;title&gt;)',
        'description' => 'Описание страницы (выводится при поиске в google, yandex и других поисковиках)',
        'keywords'    => 'Ключевые слова для страницы',
        'robots'      => 'Параметры для поисковых роботов. Например "noindex, nofollow"',
    ]

];