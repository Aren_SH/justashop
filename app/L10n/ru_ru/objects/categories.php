<?php

return [
    'page-title'      => 'Категории',
    'page-title-list' => 'Список категорий',
    'page-title-edit' => 'Редактирование категории',
    'page-title-add'  => 'Добавление категории',

    'fields' => [
        'title'       => 'Название',
        'description' => 'Описание',
        'parent'      => 'Родительская атегория',
    ],

    'validation' => [
            'title too long' => 'Слишком длинный заголовок',
    ],

];