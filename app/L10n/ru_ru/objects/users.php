<?php

return [
    'page-title'      => 'Пользователи',
    'page-title-list' => 'Список пользователей',
    'page-title-edit' => 'Редактирование пользователя',
    'page-title-add'  => 'Добавление пользователя',

    'fields' => [
        'login'        => 'Логин',
        'password'     => 'Новый пароль',
        'email'        => 'Email',
        'roles'        => 'Роли',
        'status'       => 'Статус',
    ],

    'validation' => [
        'email already exists'                 => 'Email уже используется другим пользователем',
        'email is not valid'                   => 'Email некорректен',
        'login required'                       => 'Вы должны указать логин',
        'login too long'                       => 'Логин слишком длинным',
        'login too short'                      => 'Логин слишком короткий',
        'password required'                    => 'Вы должны указать пароль',
        'password too short'                   => 'Слишком короткий пароль',
        'password too long'                    => 'Слишком длинный пароль',
        'password doesn\'t match confirmation' => 'Пароль не совпадает с подтвержением',
        'invalid login or password'            => 'Пользователь с таким логином и паролем не найден',
    ],

    'statuses' => [
        'status0' => 'Не активирован',
        'status1' => 'Активирован',
        'status2' => 'Ожидает подтверждения',

    ],

];