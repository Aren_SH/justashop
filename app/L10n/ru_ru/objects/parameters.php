<?php

return [
    'page-title'      => 'Параметры сайта',
    'page-title-list' => 'Список параметров сайта',
    'page-title-edit' => 'Редактирование параметра',
    'page-title-add'  => 'Добавление параметра',

    'fields' => [
        'key'         => 'Ключ',
        'description' => 'Описание',
        'value'       => 'Значение',
    ],

    'validation' => [
            'key required'         => 'Ключ не указан',
            'key too long'         => 'Слишком длинный ключ',
            'value required'       => 'Значение не указано',
            'description too long' => 'Слишком длинное описание',
    ],

    'help' => [
    ]

];