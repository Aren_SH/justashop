<?php

return [
    'payment' => [
        'title-single'   => ':title:, :amount: руб',
        'title-multiple' => ':title: и еще :count: :products: на сумму :amount: руб.'
    ],
    'order' => [
        'timeLimitWarning' => 'ВНИМАНИЕ! Если вы не оплатите ваш заказ в течение :time: :hours:, ваш заказ будет отменен'
    ]
];