<?php
return [
    'site-name' => 'Justashop',


    'actions' => 'Действия',
    'edit'    => 'Редактировать',
    'add'     => 'Добавить',
    'delete'  => 'Удалить',
    'apply'   => 'Применить',
    'save'    => 'Сохранить',

    'login'   => 'Войти',
    'logout'  => 'Выйти',

    'currency-short' => 'руб.',
    'currency-sign'  => 'р',

];