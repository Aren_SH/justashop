<?php
/**
 * @var Justashop\Components\Former\FormRenderer $form
 */


$collections = $form->getCollections();


?>

<?php if($form->withTabs()): ?>
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist" style="margin-bottom: 15px;">
        <?php foreach($collections as $cKey => $collection): ?>
            <li class="<?= $cKey == 'default' ? 'active' : '' ?>">
                <a href="#<?= $cKey ?>" role="tab" data-toggle="tab"><?= $collection->getTitle() ?></a>
            </li>
        <?php endforeach ?>
    </ul>
<?php endif ?>

<div class="tab-content">
    <?php foreach($collections as $cKey => $collection): ?>
        <div class="tab-pane <?= $cKey == 'default' || !$form->withTabs() ? 'active' : '' ?>" id="<?= $cKey ?>">
            <?php foreach($collection->getFields() as $key => $field): ?>
                <?php
                if(in_array($field->getTag(), ['input', 'select', 'textarea'])){
                    $field->addClass('form-control');
                } else {
                    $field->addClass('form-data');
                }
                ?>

                <?php if($field instanceof \Justashop\Components\Former\Fields\Hidden): ?>
                    <?php echo $field->render(); ?>

                <?php else: ?>
                    <fieldset class="form-group <?= $field->hasErrorText() ? 'has-error' : '' ?>">
                        <label id="<?= 'label-' . $field->getName() ?>"
                               for="<?= 'field-' . $field->getName() ?>"
                            >
                            <?= $field->getLabel() ?>
                            <?php if($field->isRequired()) : ?> <b style="color:#B50000">*</b> <?php endif ?>
                        </label>
                        <?php echo $field->render(); ?>
                        <?php if($field->hasErrorText()) : ?>
                            <p class="bg-danger text-danger js-error"> <?= $field->getErrorText() ?></p>
                        <?php endif ?>
                        <?php if($field->hasHelpText()) : ?>
                            <p class="help-block"> <?= $field->getHelpText() ?></p>
                        <?php endif ?>
                    </fieldset>

                <?php endif ?>

            <?php endforeach ?>
        </div>
    <?php endforeach ?>
</div>