CREATE TABLE IF NOT EXISTS "parameters"
(
  "id"          SERIAL        NOT NULL,
  "key"         VARCHAR(512)  NOT NULL UNIQUE,
  "description" VARCHAR(1024) DEFAULT NULL,
  "value"       TEXT          DEFAULT NULL,

  PRIMARY KEY ("id")
) Without Oids;

CREATE TABLE IF NOT EXISTS "products"
(
  "id"          SERIAL        NOT NULL,
  "sku"         VARCHAR(64)   NOT NULL UNIQUE,
  "title"       VARCHAR(512)  NOT NULL,
  "price"       FLOAT         NOT NULL,
  "description" TEXT          DEFAULT NULL,
  "category_id" INT           DEFAULT NULL,
  "metaTag_id"  INT           DEFAULT NULL,
  "createdAt"   TIMESTAMP     NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "updatedAt"   TIMESTAMP     NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "status"      SMALLINT      DEFAULT 1,

  PRIMARY KEY ("id")
) Without Oids;


CREATE TABLE IF NOT EXISTS "categories"
(
  "id"          SERIAL        NOT NULL,
  "title"       VARCHAR(512)  NOT NULL,
  "description" TEXT          DEFAULT NULL,
  "parent_id"   INT           DEFAULT NULL,
  "metaTag_id"  INT           DEFAULT NULL,
  "createdAt"   TIMESTAMP     NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "status"      SMALLINT      DEFAULT 1,

  PRIMARY KEY ("id")
) Without Oids;


CREATE TABLE IF NOT EXISTS "users"
(
  "id"          SERIAL        NOT NULL,
  "login"       VARCHAR(256)  NOT NULL,
  "password"    VARCHAR(64)   NOT NULL,
  "email"       VARCHAR(256)  DEFAULT NULL,
  "createdAt"   TIMESTAMP     NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "status"      SMALLINT      DEFAULT 1,

  PRIMARY KEY ("id")
) Without Oids;

CREATE TABLE IF NOT EXISTS "roles"
(
  "id"          SERIAL        NOT NULL,
  "name"        VARCHAR(256)  NOT NULL,
  "description" VARCHAR(512)  DEFAULT NULL,

  PRIMARY KEY ("id")
) Without Oids;


CREATE TABLE IF NOT EXISTS "users_roles"
(
  "user_id"     INT           NOT NULL,
  "role_id"     INT           NOT NULL,

  UNIQUE ("role_id", "user_id")
) Without Oids;


CREATE TABLE IF NOT EXISTS "action_tokens"
(
  "token"       VARCHAR(64)   NOT NULL,
  "user_id"     BIGINT        NOT NULL,
  "action"      SMALLINT      DEFAULT 1,
  "expiredAt"   TIMESTAMP     NOT NULL,

  UNIQUE ("token"),  PRIMARY KEY("token")
) Without Oids;


CREATE TABLE IF NOT EXISTS "images"
(
  "id"          SERIAL        NOT NULL,
  "hash"        VARCHAR(64)   NOT NULL,
  "file"        VARCHAR(1024) NOT NULL,
  "sizes"       VARCHAR(64)   NOT NULL,
  "profile_id"  SMALLINT      NOT NULL,
  "status"      SMALLINT      DEFAULT 1,

  PRIMARY KEY ("id"),
  UNIQUE ("hash", "profile_id")
) Without Oids;


CREATE TABLE IF NOT EXISTS "product_images"
(
  "id"          SERIAL        NOT NULL,
  "product_id"  BIGINT        NOT NULL,
  "image_id"    BIGINT        NOT NULL,
  "is_main"     BOOLEAN       DEFAULT FALSE,
  "title"       VARCHAR(256)  DEFAULT NULL,
  "description" TEXT          DEFAULT NULL,

  PRIMARY KEY ("id"),
  UNIQUE ("product_id", "image_id")
) Without Oids;


CREATE TABLE IF NOT EXISTS "addresses"
(
  "id"          SERIAL        NOT NULL,
  "user_id"     BIGINT,
  "country"     INT           NOT NULL,
  "zip_code"    VARCHAR(32)   NOT NULL,
  "address"     TEXT          NOT NULL,
  "createdAt"   TIMESTAMP     NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "status"      SMALLINT      DEFAULT 1,

  PRIMARY KEY ("id")
) Without Oids;



CREATE TABLE IF NOT EXISTS "orders"
(
  "id"               SERIAL        NOT NULL,
  "user_id"          BIGINT,
  "address_id"       INTEGER       NOT NULL,
  "user_name"        VARCHAR(128)  NOT NULL,
  "user_email"       VARCHAR(128)  NOT NULL,
  "user_phone"       VARCHAR(32)   NOT NULL,
  "amount"           FLOAT         NOT NULL,
  "payment_type_id"  INTEGER       DEFAULT NULL,
  "delivery_type_id" INTEGER       NOT NULL,
  "hash"             VARCHAR(64)   NOT NULL,
  "createdAt"        TIMESTAMP     NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "status"           SMALLINT      DEFAULT 1,

  PRIMARY KEY ("id")
) Without Oids;


CREATE TABLE IF NOT EXISTS "order_products"
(
  "id"          SERIAL        NOT NULL,
  "order_id"    BIGINT        NOT NULL,
  "product_id"  BIGINT        NOT NULL,
  "quantity"    INTEGER       NOT NULL,
  "price"       FLOAT         NOT NULL,

  PRIMARY KEY ("id"),
  UNIQUE ("product_id", "order_id")
) Without Oids;



CREATE TABLE IF NOT EXISTS "payment_types"
(
  "id"             SERIAL        NOT NULL,
  "title"          VARCHAR(128)  NOT NULL,
  "description"    TEXT,
  "discount"       FLOAT         DEFAULT 1,
  "adapter"        VARCHAR(64)   NOT NULL,
  "status"         SMALLINT      DEFAULT 1,

  PRIMARY KEY ("id")
) Without Oids;


CREATE TABLE IF NOT EXISTS "delivery_types"
(
  "id"             SERIAL        NOT NULL,
  "title"          VARCHAR(128)  NOT NULL,
  "description"    TEXT,
  "price"          FLOAT         DEFAULT 0,
  "status"         SMALLINT      DEFAULT 1,

  PRIMARY KEY ("id")
) Without Oids;





-- VIEWS
CREATE VIEW "product_images_view" AS SELECT
  "product_images"."id" as "id",
  "product_id",
  "image_id",
  "title",
  "description",
  "is_main",
  "image"."id" as "image.id",
  "image"."hash" as "image.hash",
  "image"."file" as "image.file",
  "image"."profile_id" as "image.profile_id",
  "image"."sizes" as "image.sizes",
  "image"."status" as "image.status"
FROM
  "product_images"
LEFT JOIN
  "images" as "image" ON "image"."id" = "product_images"."image_id";


CREATE VIEW "products_view" AS SELECT
  "products"."id"                as "id",
  "products"."sku"               as "sku",
  "products"."title"             as "title",
  "products"."price"             as "price",
  "products"."description"       as "description",
  "products"."category_id"       as "category_id",
  "products"."createdAt"         as "createdAt",
  "products"."updatedAt"         as "updatedAt",
  "products"."status"            as "status",
  COUNT("order_products"."id")   as "popularity",

  "product_image"."id"           as "product_image.id",
  "product_image"."product_id"   as "product_image.product_id",
  "product_image"."image_id"     as "product_image.image_id",
  "product_image"."title"        as "product_image.title",
  "product_image"."description"  as "product_image.description",
  "product_image"."is_main"      as "product_image.is_main",
  "image"."id"                   as "product_image.image.id",
  "image"."hash"                 as "product_image.image.hash",
  "image"."file"                 as "product_image.image.file",
  "image"."profile_id"           as "product_image.image.profile_id",
  "image"."sizes"                as "product_image.image.sizes",
  "image"."status"               as "product_image.image.status"
FROM
  "products"
LEFT JOIN
  "product_images" as "product_image" ON "product_image"."product_id" = "products"."id" AND "product_image"."is_main" = true
LEFT JOIN
  "images" as "image" ON "product_image"."image_id" = "image"."id"
LEFT JOIN
  "order_products" as "order_products" ON "order_products"."product_id" = "products"."id"
GROUP BY "products"."id", "product_image"."id", "image"."id";




CREATE VIEW "orders_view" AS SELECT
  "orders"."id"      as "id",
  "orders"."user_id" as "user_id",
  "address_id",
  "user_name",
  "user_email",
  "user_phone",
  "amount",
  "payment_type_id",
  "delivery_type_id",
  "hash",
  "orders"."createdAt" as "createdAt",
  "orders"."status"    as "status",

  "address"."id"        as "address.id",
  "address"."user_id"   as "address.user_id",
  "address"."country"   as "address.country",
  "address"."zip_code"  as "address.zip_code",
  "address"."address"   as "address.address",
  "address"."createdAt" as "address.createdAt",
  "address"."status"    as "address.status",


  "delivery_type"."id"          as "delivery_type.id",
  "delivery_type"."title"       as "delivery_type.title",
  "delivery_type"."description" as "delivery_type.description",
  "delivery_type"."price"       as "delivery_type.price",
  "delivery_type"."status"      as "delivery_type.status",


  "payment_type"."id"          as "payment_type.id",
  "payment_type"."title"       as "payment_type.title",
  "payment_type"."description" as "payment_type.description",
  "payment_type"."discount"    as "payment_type.discount",
  "payment_type"."status"      as "payment_type.status"

FROM
  "orders"

LEFT JOIN "addresses"      as "address"       ON "address"."id" = "orders"."address_id"
LEFT JOIN "payment_types"  as "payment_type"  ON "payment_type"."id" = "orders"."payment_type_id"
LEFT JOIN "delivery_types" as "delivery_type" ON "delivery_type"."id" = "orders"."delivery_type_id";
