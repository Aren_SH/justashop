<?php

/** VEEERY useful */
define ( "DS", DIRECTORY_SEPARATOR );

/** public directory path */
define( "DOC_ROOT", realpath( dirname(__FILE__) ) . DS );

/** app directory path */
define( "APP_PATH", realpath( DOC_ROOT . '..' . DS . 'app' ) . DS  );

define( "LOG_PATH", APP_PATH . 'logs' . DS  );

/** config directory path */
define( "CONF_PATH", APP_PATH . 'config' . DS  );


function __logError( $code, $msg = null, $file = null, $line = null, $ctxt = null ){
    $fd = @fopen( LOG_PATH . 'error.log', 'a' );

    if( !$fd ){
        return;
    }

    $text = 'ERROR: ' . $code . PHP_EOL;

    if( $msg )  $text .= "\tMESSAGE: " . $msg  . PHP_EOL;
    if( $file ) $text .= "\tFILE: "    . $file . PHP_EOL;
    if( $line ) $text .= "\tLINE: "    . $line . PHP_EOL;;
    //if( $ctxt ) $text .= "\tCONTEXT: " . $ctxt . PHP_EOL;

    fwrite($fd, $text);
    fclose( $fd );
}

//set_error_handler('__logError', E_ALL);

try {
    include APP_PATH . 'bootstrap.php';

    $app = new \Phalcon\Mvc\Application($di);
    echo $app->handle()->getContent();
} catch (\Exception $e) {
    __logError($e->getCode(), $e->getMessage(), $e->getFile(), $e->getLine() );
    http_response_code(500);
    throw $e;
}
