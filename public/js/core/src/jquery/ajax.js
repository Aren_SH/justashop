;(function( core, document, $ ){

    var ajax = {

        get: function(url, data){
        	return new Promise(function (resolve, reject){
    			var request = $.get(url, data);
    				request.then(
    					function(data, textStatus, jqXHR){ resolve(jqXHR) },
    					function(jqXHR, textStatus, errorThrown){ reject(jqXHR) }
    				);
    		});
        },

    };



    core.ajax = ajax;
}( core, document, $ ) );