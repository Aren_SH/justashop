;(function (core, document) {

    var format = {
        moneyFormat: function (num) {
            var str = num.toFixed(2).replace(/./g, function (c, i, a) {
                return i && c !== "." && !((a.length - i) % 3) ? ' ' + c : c;
            });

            return str.replace('.', ',');
        }

    };


    core.utils.format = format;
}(core, document) );