;( function( core ){

    /**
     * Functions to get value from  different DOM elements
     *
     * @type {object}
     */
    var valueGetters = {
        'select': function( element ){
            var values = [];
            for ( var i = 0; i < element.options.length; i++ ){
                if( element.options[i].selected ){
                    values.push( element.options[i].value );
                }
            }

            return element.multiple ? values : values[0];
        }
        ,
        'input': function( element ){
            var type = ( element.type ).toLowerCase();
            if( type == 'reset' || type == 'button' || type == 'submit' ) {
                return null;
            }

            var val;
            if( type == 'file' && element.files.length ) {
                val = [];
                for( var i = 0; i < element.files.length; i++ ){
                    val.push( element.files.item(i).name );
                }
            }
            else {
                val = element.value;
            }

            return ( type == 'checkbox' || type == 'radio' ) && !element.checked ? null : val;
        }
        ,
        'textarea': function( element ){
            return element.value;
        }
    };

    /**
     * Email regexp
     * @see http://stackoverflow.com/questions/46155/validate-email-address-in-javascript
     *
     * @type {RegExp}
     */
    var emailRegExp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    /**
     * @param  {Element} element
     * @return {String|Array}
     */
    function getElementValue ( element ){
        var tag = ( element.tagName ).toLowerCase();
        var val  = null;
        if( tag in valueGetters ){
            val = valueGetters[tag]( element );
        }

        return val;

    }


    function Validator ( messages ) {
        this._messages = messages || {};
    }

    Validator.prototype.constructor = Validator;
    /**
     * Error messages  ( attribute => messages )
     * @type {{}}
     */
    Validator.prototype._messages  = {};

    Validator.prototype._defaultMessages = {
        'required':  "Поле обязательно для заполнения",
        'min':       "Значение не должно быть меньше {{min}}",
        'max':       "Значение не должно быть больше {{max}}",
        'minlength': "Значение  не должно быть мнее {{maxlength}}",
        'maxlength': "Значение не должно быть более {{maxlength}}",
        'minsize':   "размер файла меньше минимального {{min}}",
        'maxsize':   "Размер Файл превышает максимальный размер {{max}}",
        'pattern':   "Значение не соответсвует формату",
        'email':     "Значение должно быть корректным email адресом",
        'confirm':   "Введенные значения не совпадают"
    };

    /**
     * Validation callbacks ( attribute => callback )
     * @type {{}}
     */
    Validator.prototype.callbacks = {};


    /**
     *
     * @param   {HTMLFormElement} form
     * @returns {boolean|{}}
     */
    Validator.prototype.validateForm = function ( form ){
        var elements = form.elements;
        if( !elements ){
            return false;
        }

        var errors  = {};
        var error   = false;
        var element = null;
        for( var i = 0; i < elements.length; i++ ){
            element = elements[i];
            error = this.validateField( element );
            if( error ){
                errors[element.name] = error;
            }
        }

        return !core.utils.isEmpty(errors) ? errors : null;
    };


    /**
     *
     * @param  {Element} element
     * @return {Array}
     */
    Validator.prototype.validateField = function( element ){
        var errors = [];
        var attrs = Object.keys( this.callbacks );

        for( var i = 0; i < attrs.length; i++  ){
            if( !this.callbacks[attrs[i]] ){
                continue;
            }
            var error     = false;
            var attrValue = element.getAttribute( attrs[i] );
            if( attrValue ){
                error = ( this.callbacks[attrs[i]].bind( this ) )( element, attrValue );
            }

            if( error ){
                errors.push( error );
            }
        }

        return errors.length > 0 ? errors : false;
    };

    Validator.validateField = function( element ){
        var validator = new Validator();

        return validator.validateField( element );
    };


    Validator.prototype._getMessage = function( field, message ){

        if( this._messages.hasOwnProperty(field) && !core.utils.isEmpty(this._messages[field][message])){
            return this._messages[field][message];
        } else {
            return this._defaultMessages[message];
        }
    };


    Validator.prototype.callbacks['required'] = function( element ){
        var value = getElementValue( element );

        return core.utils.isEmpty( value ) ? this._getMessage(element.name, 'required') : null;
    };

    Validator.prototype.callbacks['pattern'] = function( element, pattern ){
        var regExp = new RegExp( pattern );
        var values = getElementValue( element );
        var error  = null;

        if( core.utils.isEmpty( values ) ){
            return null;
        }
        if( !core.utils.isArray( values ) ){
            values = [values];
        }

        for ( var i = 0; i < values.length; i++ ){
            if(  !regExp.test( values[i] ) ) {
                error = this._getMessage(element.name, 'pattern');
                break;
            }
        }
        return error;
    };

    Validator.prototype.callbacks['email'] = function( element ){
        var values = getElementValue( element );
        var error = null;


        if( core.utils.isEmpty( values ) ){
            return null;
        }
        if( !core.utils.isArray( values ) ){
            values = [values];
        }

        for ( var i = 0; i < values.length; i++ ){
            if(  !emailRegExp.test( values[i] ) ) {
                error = this._getMessage(element.name, 'email');
                break;
            }
        }
        return error;
    };

    Validator.prototype.callbacks['min'] = function( element, min ){
        var values = getElementValue( element );
        var error  = null;

        if( core.utils.isEmpty( values ) ){
            return null;
        }
        if( !core.utils.isArray( values ) ){
            values = [values];
        }

        min = parseFloat( min );
        for ( var i = 0; i < values.length; i++ ){
            if(  parseFloat( values[i] ) < min ) {
                error = this._getMessage(element.name, 'min');
                break;
            }
        }
        return error;
    };

    Validator.prototype.callbacks['max'] = function( element, max ){
        var values = getElementValue( element );
        var error  = null;

        if( core.utils.isEmpty( values ) ){
            return null;
        }
        if( !core.utils.isArray( values ) ){
            values = [values];
        }

        max = parseFloat( max );
        for ( var i = 0; i < values.length; i++ ){
            if(  parseFloat( values[i] ) > max ) {
                error = this._getMessage(element.name, 'max');
                break;
            }
        }
        return error;
    };

    Validator.prototype.callbacks['maxlength'] = function( element, maxlength ){
        var values = getElementValue( element );
        var error  = null;

        if( core.utils.isEmpty( values ) ){
            return null;
        }
        if( !core.utils.isArray( values ) ){
            values = [values];
        }

        maxlength = parseFloat( maxlength );
        for ( var i = 0; i < values.length; i++ ){
            if(   values[i].length > maxlength ) {
                error = this._getMessage(element.name, 'maxlength');
                break;
            }
        }
        return error;
    };

    Validator.prototype.callbacks['minlength'] = function( element, minlength ){
        var values = getElementValue( element );
        var error  = null;

        if( core.utils.isEmpty( values ) ){
            return null;
        }
        if( !core.utils.isArray( values ) ){
            values = [values];
        }

        minlength = parseFloat( minlength );
        for ( var i = 0; i < values.length; i++ ){
            if(  values[i].length < minlength ) {
                error = this._getMessage(element.name, 'minlength');
                break;
            }
        }
        return error;
    };

    Validator.prototype.callbacks['data-validator-confirm'] = function( element ){
        var value = getElementValue( element );
        var form  = element.form;
        var confirmField = element.getAttribute('data-validator-confirm');
        var confirmValue = form.elements.namedItem( confirmField).value;

        return value == confirmValue ? null : this._getMessage(element.name, 'confirm');
    };

    Validator.prototype.callbacks['maxsize'] = function( element, maxsize ){
        var type = ( element.type ).toLowerCase();
        maxsize  = parseFloat( maxsize );

        var errors = [];
        if( type == 'file' && element.files.length ) {
            for( var i = 0; i < element.files.length; i++ ){
                var file = element.files.item(i);
                if( file.size > maxsize ){
                    errors.push( this._getMessage(element.name, 'maxsize').replace( '{{file}}', file.name ) );
                }
            }
        }

        return errors.length ? errors.join( '\n' ) : null;
    };

    Validator.prototype.callbacks['minsize'] = function( element, minsize ){
        var type = ( element.type ).toLowerCase();
        minsize  = parseFloat( minsize );

        var errors = [];
        if( type == 'file' && element.files.length ) {
            for( var i = 0; i < element.files.length; i++ ){
                var file = element.files.item(i);
                if( file.size < minsize ){
                    errors.push( this._getMessage(element.name, 'minsize').replace( '{{file}}', file.name ) );
                }
            }
        }

        return errors.length ? errors.join( '\n' ) : null;
    };



    core.utils.Validator = Validator;
}( core || {} ) );