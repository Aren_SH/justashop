;(function (core, document) {

    "use strict";

    var dom = {};


    /**
     * List of attributes accessible directly without setAttribute
     *
     * @type {string[]}
     * @private
     */
    var _standardAttributes = [
        "accesskey", "className", "contenteditable", "contextmenu", "dir", "draggable", "dropzone",
        "hidden", "id", "itemid", "itemprop", "itemref", "itemscope", "itemtype", "lang",
        "spellcheck", "style", "tabindex", "title"
    ];


    /**
     *
     * @param  {Node} node
     * @param  {String}      tag
     * @return {Node}
     */
    dom.wrap = function (node, tag) {
        var wrapper = document.createElement(tag);
        wrapper.appendChild(node);

        return wrapper;
    };

    /**
     * @return {Node}
     */
    dom.getBody = function () {
        return document.getElementsByTagName('body')[0];
    };


    /**
     *
     * @param {Function}callback
     */
    dom.documentReady = function (callback) {
        if (document.readyState == 'complete') {
            callback();
        } else {
            document.addEventListener("DOMContentLoaded", callback, false);
        }
    };


    /**
     *
     * @param {Node} element
     * @param {{}}   data
     */
    dom.setAttributes = function (element, data) {
        if (core.utils.isEmpty(data)) return;

        for (var attr in data) {
            if (!data.hasOwnProperty(attr)) continue;

            if (attr == 'data') {
                dom.setDataAttributes(element, data[attr]);
            }

            if (_standardAttributes.indexOf(attr) != -1) {
                element[attr] = data[attr];
            }
            else {
                element.setAttribute(attr, data[attr]);
            }
        }
    };


    /**
     *
     * @param {Node} element
     * @param {{}}   data
     */
    dom.setDataAttributes = function (element, data) {
        if (core.utils.isEmpty(data)) {
            return;
        }

        for (var prop in data) {
            if (!data.hasOwnProperty(prop)) continue;

            element.dataset[prop] = data[prop];
        }
    };


    /**
     *
     * @param {Node} element
     */
    dom.clear = function(element){
        while(element.firstChild){
            element.removeChild(element.firstChild);
        }
    };


    /**
     *
     * @param  {String}                tag
     * @param  {{}}                    attributes
     * @param  {HTMLElement[]|String} [data]
     *
     * @return {HTMLElement}
     */
    dom.create = function (tag, attributes, data) {
        if(arguments.length == 2){
            return dom.create(tag, {}, attributes);
        }

        var element = document.createElement(tag);
        dom.setAttributes(element, attributes);

        if(typeof data == 'string' ){
            element.appendChild(document.createTextNode(data));
        } else {
            if( !core.utils.isArray(data)){
                data = [data];
            }

            for( var i = 0; i < data.length; i++ ){
                if( typeof data[i] == 'undefined' || data[i] === null ) continue;
                if(typeof data[i] != 'object'){
                    data[i] = document.createTextNode(data[i]);
                }

                element.appendChild(data[i]);
            }
        }

        return element;
    };


    /**
     *
     * @param  {[]|HtmlElement} elements
     * @param  {[]|HtmlElement} children
     * @return {[]|HtmlElement}
     */
    dom.append = function (elements, children) {
        if (!children) {
            return elements;
        }

        if (!( elements instanceof Array)) {
            elements = [elements];
        }

        if (!( children instanceof Array)) {
            children = [children];
        }

        for (var j = 0; j < elements.length; j++) {
            for (var i = 0; i < children.length; i++) {
                if (!elements[j]) continue;

                elements[j].appendChild(children[i]);
            }
        }

        return elements.length == 1 ? elements[0] : elements;
    };


    /**
     *
     * @param  {Node} elements
     */
    dom.remove = function (elements) {

        if(!elements.length) elements = [elements];

        for (var i = 0; i < elements.length; ++i) {
            if (!elements[i].parentNode) continue;

            elements[i].parentNode.removeChild(elements[i]);
        }
    };


    /**
     *
     * @param {Node} srcElement
     * @param {Node} newElement
     */
    dom.replace = function(srcElement, newElement){
        if (!srcElement.parentNode) {
            return;
        }

        srcElement.parentNode.insertBefore(newElement, srcElement);
        srcElement.parentNode.removeChild(srcElement);
    };


    /**
     *
     * @param  {HTMLElement} element
     * @param  {string}      cls
     * @return {boolean}
     */
    dom.hasClass = function(element, cls){
        return (new RegExp('\\b'+cls+'\\b')).test(element.className);
    };


    /**
     * @param {HTMLElement} element
     * @param {string}      cls
     */
    dom.addClass = function(element, cls){
        if( !this.hasClass(element, cls) ){
            element.className += ' ' + cls;
        }
    };


    /**
     *
     * @param  {HTMLElement} element
     * @param  {string} cls
     */
    dom.removeClass = function(element, cls){
        var regExp = new RegExp('(\\s|^)' + cls + '(\\s|$)', 'g');
        element.className = element.className.replace(regExp, '$2');
    };


    /**
     *
     * @param  {HTMLElement} element
     * @param  {string} cls
     */
    dom.toggleClass = function(element, cls){
        dom.hasClass(element, cls) ? dom.removeClass(element, cls) : dom.addClass(element, cls);
    };


    /**
     * Wrapper for default querySelector method
     *
     * @param  {string} selector
     * @param  {Node}   [context]
     *
     * @return {Node}
     */
    dom.querySelector = function (selector, context) {
        context = context || document;

        return context.querySelector(selector);
    };


    /**
     *
     * @param  {string} selector
     * @param  {Node}   context
     * @return {NodeList}
     */
    dom.querySelectorAll = function (selector, context) {
        context = context || document;

        return context.querySelectorAll(selector);
    };

    /**
     *
     * @param  {Node}   element
     * @param  {string} selector
     * @param  {Node}   [context]
     *
     * @return {Boolean}
     */
    dom.matches = function (element, selector, context) {
        if (typeof element.matches == 'function') {
            return element.matches(selector);
        }
        else if (typeof element.webkitMatchesSelector == 'function') {
            return element.webkitMatchesSelector(selector);
        }
        else if (typeof element.mozMatchesSelector == 'function') {
            return element.mozMatchesSelector(selector);
        }
        else if (typeof element.mozMatchesSelector == 'function') {
            return element.mozMatchesSelector(selector);
        }
        else if (typeof element.msMatchesSelector == 'function') {
            return element.msMatchesSelector(selector);
        }
        else if (typeof element.oMatchesSelector == 'function') {
            return element.oMatchesSelector(selector);
        } else {
            context = context || document;

            var elements = dom.querySelectorAll(selector, context);
            for (var i = 0; i < elements.length; ++i) {
                if (elements[i] == element) {
                    return true;
                }
            }
        }

        return false;
    };

    /**
     *
     * @param {Node}            eventTarget
     * @param {string}          type
     * @param {Function|Object} callback
     */
    dom.addEventListener = function (eventTarget, type, callback) {
        eventTarget.addEventListener(type, callback, false);
    };

    /**
     *
     * @param {Node}            eventTarget
     * @param {string}          type
     * @param {Function|Object} callback
     */
    dom.removeEventListener = function (eventTarget, type, callback) {
        eventTarget.removeEventListener(type, callback, false);
    };

    var guid = 0;

    /**
     * brute and stupid implementation of jquery delegate method
     *
     * @param {Node}            context
     * @param {string}          selector
     * @param {string}          type
     * @param {Function|Object} callback
     */
    dom.delegateEventListener = function (context, selector, type, callback) {
        var wrapped = function (event) {
            if (dom.matches(event.target, selector, context)) {
                return !callback.handle ? callback(event) : callback.handle(event);
            }
            return null;
        };

        callback.__guid = callback.__guid || ++guid;
        var contextUID = getUID(selector, type);
        if (!context.__delegateHandlers) {
            context.__delegateHandlers = {};
        }

        if (!context.__delegateHandlers[contextUID]) {
            context.__delegateHandlers[contextUID] = {};
        }

        if (context.__delegateHandlers[contextUID][callback.__guid]) {
            delete context.__delegateHandlers[contextUID][callback.__guid];
        }

        context.__delegateHandlers[contextUID][callback.__guid] = wrapped;

        dom.addEventListener(context, type, wrapped);


    };

    /**
     *
     * @param {Node}            context
     * @param {string}          selector
     * @param {string}          type
     * @param {Function|Object} callback
     */
    dom.undelegateEventListener = function (context, selector, type, callback) {
        if (!callback.__guid) {
            return;
        }
        if (!context.__delegateHandlers) {
            return;
        }

        var contextUID = getUID(selector, type);
        if (!context.__delegateHandlers[contextUID]) {
            cleanUpDelegatedHandlers(context, contextUID);
        }

        if (context.__delegateHandlers[contextUID][callback.__guid]) {
            try {
                dom.removeEventListener(context, type, context.__delegateHandlers[contextUID][callback.__guid]);

                delete context.__delegateHandlers[contextUID][callback.__guid];
                cleanUpDelegatedHandlers(context, contextUID);
            } catch (error) {
            }
        }
    };


    /**
     *
     * @param  {string} selector
     * @param  {string} type
     * @return {string}
     */
    function getUID(selector, type) {
        return selector + '_' + type;
    }



    function cleanUpDelegatedHandlers(context, contextUID) {
        if (core.utils.isEmpty(context.__delegateHandlers)) {
            try {
                delete context.__delegateHandlers;
            } catch (error) {}

            return;
        }

        if (core.utils.isEmpty(context.__delegateHandlers[contextUID])) {
            try {
                delete context.__delegateHandlers[contextUID];
            } catch (error) {}

            cleanUpDelegatedHandlers(context, contextUID);
        }
    }


    dom.el = {};

    var tags = [
        "h1", "h2", "h3", "h4", "h5",
        "form", "fieldset", "input", "label", "textarea", "select", "option", "button",
        "ul", "ol", "li",
        "table", "tr", "td", "th",
        "pre","img", "a", "br", "div", "p", "span", "strong", "i", "b"

    ];
    for (var i = 0; i< tags.length; i++){
        dom.el[tags[i]] = core.utils.partialFunc(dom.create, tags[i])
    }


    core.dom = dom;

}(core, document) );

//https://developer.mozilla.org/en-US/docs/Web/API/EventTarget.addEventListener#Legacy_Internet_Explorer_and_attachEvent
(function() {
    if (!Event.prototype.preventDefault) {
        Event.prototype.preventDefault=function() {
            this.returnValue=false;
        };
    }
    if (!Event.prototype.stopPropagation) {
        Event.prototype.stopPropagation=function() {
            this.cancelBubble=true;
        };
    }
    if (!Element.prototype.addEventListener) {
        var eventListeners=[];

        var addEventListener=function(type,listener /*, useCapture (will be ignored) */) {
            var self=this;
            var wrapper=function(e) {
                e.target=e.srcElement;
                e.currentTarget=self;
                if (listener['handleEvent']) {
                    listener.handleEvent(e);
                } else {
                    listener.call(self,e);
                }
            };
            if (type=="DOMContentLoaded") {
                var wrapper2=function(e) {
                    if (document.readyState=="complete") {
                        wrapper(e);
                    }
                };
                document.attachEvent("onreadystatechange",wrapper2);
                eventListeners.push({object:this,type:type,listener:listener,wrapper:wrapper2});

                if (document.readyState=="complete") {
                    var e=new Event();
                    e.srcElement=window;
                    wrapper2(e);
                }
            } else {
                this.attachEvent("on"+type,wrapper);
                eventListeners.push({object:this,type:type,listener:listener,wrapper:wrapper});
            }
        };
        var removeEventListener=function(type,listener /*, useCapture (will be ignored) */) {
            var counter=0;
            while (counter<eventListeners.length) {
                var eventListener=eventListeners[counter];
                if (eventListener.object==this && eventListener.type==type && eventListener.listener==listener) {
                    if (type=="DOMContentLoaded") {
                        this.detachEvent("onreadystatechange",eventListener.wrapper);
                    } else {
                        this.detachEvent("on"+type,eventListener.wrapper);
                    }
                    break;
                }
                ++counter;
            }
        };
        Element.prototype.addEventListener=addEventListener;
        Element.prototype.removeEventListener=removeEventListener;
        if (HTMLDocument) {
            HTMLDocument.prototype.addEventListener=addEventListener;
            HTMLDocument.prototype.removeEventListener=removeEventListener;
        }
        if (Window) {
            Window.prototype.addEventListener=addEventListener;
            Window.prototype.removeEventListener=removeEventListener;
        }
    }
})();