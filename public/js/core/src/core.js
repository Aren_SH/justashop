var core = (function( document){

    "use strict";

    /** Some basic functions that might not exists **/

    if (typeof Array.isArray !== "function") {
        Array.isArray = function( obj ) {
            return Object.prototype.toString.call(obj) === "[object Array]";
        };
    }

    if( typeof Function.bind !== 'function' ){
        // From https://prototype.lighthouseapp.com/projects/8886/tickets/215-optimize-bind-bindaseventlistener#ticket-215-9
        Function.prototype.bind = (function(){

            var _slice = Array.prototype.slice;

            return function(context) {
                var fn = this,
                    args = _slice.call(arguments, 1);

                if (args.length) {
                    return function() {
                        return arguments.length
                            ? fn.apply(context, args.concat(_slice.call(arguments)))
                            : fn.apply(context, args);
                    }
                }
                return function() {
                    return arguments.length
                        ? fn.apply(context, arguments)
                        : fn.call(context);
                };
            }
        })();
    }

    // From https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/keys
    if (!Object.keys) {
        Object.keys = (function () {
            'use strict';
            var hasOwnProperty = Object.prototype.hasOwnProperty,
                hasDontEnumBug = !({toString: null}).propertyIsEnumerable('toString'),
                dontEnums = [
                    'toString',
                    'toLocaleString',
                    'valueOf',
                    'hasOwnProperty',
                    'isPrototypeOf',
                    'propertyIsEnumerable',
                    'constructor'
                ],
                dontEnumsLength = dontEnums.length;

            return function (obj) {
                if (typeof obj !== 'object' && (typeof obj !== 'function' || obj === null)) {
                    throw new TypeError('Object.keys called on non-object');
                }

                var result = [], prop, i;

                for (prop in obj) {
                    if (hasOwnProperty.call(obj, prop)) {
                        result.push(prop);
                    }
                }

                if (hasDontEnumBug) {
                    for (i = 0; i < dontEnumsLength; i++) {
                        if (hasOwnProperty.call(obj, dontEnums[i])) {
                            result.push(dontEnums[i]);
                        }
                    }
                }
                return result;
            };
        }());
    }


    /** Core object **/
    var core = {
        dom:     {},
        utils:   {},
        net:     {},
        events:  {},
        queues:  {},
        sources: {}
    };

    core.utils.isArray = function ( obj ){
        return Array.isArray( obj );
    };

    // From http://stackoverflow.com/questions/4994201/is-object-empty
    core.utils.isEmpty = function ( obj ){
        if (obj == null) return true;

        if (obj.length && obj.length > 0) return false;
        if (obj.length === 0) return true;

        try{
            var keys = Object.keys( obj );
            return keys.length < 1;
        } catch (e) {}

        return true;
    };

    core.utils.extend  = function( dst, src ){
        if( !src || !dst){
            return;
        }

        var objectProps = Object.keys( src );
        for( var prop in objectProps ){
            dst[objectProps[prop]] = src[objectProps[prop]];
        }
    };

    core.utils.prependPrototype = function( object, prototype ){
        var proto = object;
        while( typeof( proto.prototype.prototype ) != 'undefined' ) {
            proto = proto.prototype;
        }

        proto.prototype = prototype;
    };

    /**
     *
     * @param  {Function[]} functions
     * @return {Function}
     */
    core.utils.compose = function( functions ){
        return function(){
            var args = arguments;
            for( var i=0; i < functions.length; i++){
                args = [functions[i].apply(this, args)];
            }

            return args[0];
        }
    };


    /**
     *
     * @param  {String}   prop
     * @return {Function}
     */
    core.utils.getProp = function(prop){
        return function(object){
            return object[prop];
        }
    };


    /**
     *
     * @param  {String}   func
     * @return {Function}
     */
    core.utils.callFunc = function(func){
        return function(object){
            return object[func]();
        }
    };


    /**
     *
     * @param  {Function} func
     * @return {Function}
     */
    core.utils.partialFunc = function (func) {
        var args = Array.prototype.slice.call(arguments, 1);
        return function () {
            return func.apply(this, args.concat(Array.prototype.slice.call(arguments)));
        };
    };

    return core;

}( document ));