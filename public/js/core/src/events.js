;(function (core, document) {

    "use strict";

    var events = {};

    //unique ids for event listeners
    var uid = 1;

    /**
     * Events Dispatcher. Register event listeners and trigger events;
     */

    function Dispatcher() {
        this.listeners = {};
    }

    Dispatcher.prototype = {
        constructor: Dispatcher,

        listeners: {},


        /**
         *
         * @param  {string} eventType
         * @param  {function|object} callback
         * @return {number}
         */
        addListener: function (eventType, callback) {

            var registeredEventTypes = Object.keys(this.listeners);
            if (registeredEventTypes.indexOf(eventType) == -1) {
                this.listeners[eventType] = {};
            }

            var __dispatcherUID = uid++;

            this.listeners[eventType][__dispatcherUID] = callback;

            callback.__dispatcherUID = __dispatcherUID;

            return __dispatcherUID;
        },


        /**
         *
         * @param  {string} eventType
         * @param  {number|function|object} callback
         * @return {boolean}
         */
        removeListener: function (eventType, callback) {
            var __dispatcherUID = 0;
            if (typeof ( callback ) == "function") {
                __dispatcherUID = !core.utils.isEmpty(callback.__dispatcherUID) ? callback.__dispatcherUID : 0;
            } else {
                __dispatcherUID = callback;
            }

            if (!__dispatcherUID) {
                return false;
            }

            var registeredEventTypes = Object.keys(this.listeners);
            if (registeredEventTypes.indexOf(eventType) == -1) {
                return false;
            }

            delete this.listeners[eventType][__dispatcherUID];

            if (core.utils.isEmpty(this.listeners[eventType])) {
                delete this.listeners[eventType];
            }

            return true;
        },


        /**
         *
         * @param  {Event} event
         * @return {boolean}
         */
        trigger: function (event) {
            var result = null;

            if (!event.type) {
                return result;
            }

            var registeredEventTypes = Object.keys(this.listeners);
            if (registeredEventTypes.indexOf(event.type) == -1) {
                return result;
            }

            var callbacks = this.listeners[event.type];
            var callbackIds = Object.keys(callbacks);

            for (var i = 0; i < callbackIds.length; i++) {
                var callbackId = callbackIds[i];
                if (callbacks[callbackId] instanceof Object && callbacks[callbackId].hasOwnProperty('handleEvent')) {
                    result = callbacks[callbackId].handleEvent(event);
                }
                else if (callbacks[callbackId] instanceof Function) {
                    result = callbacks[callbackId](event);
                }

                if (event.isPropagationStopped()) {
                    break;
                }
            }

            return result;

        }

    };


    /**
     * Base Event Class.
     *
     * @param type
     * @param target
     * @param data
     * @constructor
     */
    function Event(type, target, data) {
        target = typeof( target ) == 'undefined' ? null : target;
        data   = typeof( data ) == 'undefined' ? {} : data;

        this.type   = type;
        this.target = target;
        this.data   = data;

        this._stopped          = false;
        this._defaultPrevented = false;
    }

    Event.prototype = {
        constructor: Event,

        /** @type {*} */
        data: {},

        /** @type {String} */
        type: null,

        /** @type {{}} */
        target: null,

        /** @type {boolean} */
        _stopped: false,

        /** @type {boolean} */
        _defaultPrevented: false,


        stopPropagation: function () {
            this._stopped = true;
        },


        isPropagationStopped: function () {
            return !!this._stopped;
        },


        preventDefault: function () {
            this._defaultPrevented = true;
        },


        isDefaultPrevented: function () {
            return !!this._defaultPrevented;
        }
    };

    events.Dispatcher = Dispatcher;
    events.Event      = Event;
    events.globalDispatcher = new Dispatcher();

    core.events = events;

}(core, document) );