(function(core, document){


    function L10n(){
        this._messages    = {};
        this._currentLang = 'en_EN';

    }

    L10n.prototype = {
        constructor: L10n,

        _messages: {},


        _delimiter: '.',

        _currentLang: 'en_EN',


        setLang: function(lang){
            this._currentLang = lang;
        },


        /**
         *
         * @param messages
         * @param path
         */
        addMessages: function(messages, path){
            if(!path){
                path = this._currentLang;
            }

            var dirs   = path.split(this._delimiter);
            var file   = dirs.pop();
            var messagesRef = this._messages;
            for(var i = 0; i < dirs.length; i++){
                if( !messagesRef.hasOwnProperty(dirs[i]) ) break;

                messagesRef = messagesRef[dirs[i]];
            }

            messagesRef[file] = messages;

        },


        /**
         *
         * @param  {string} path
         * @param  {string} message
         * @param  {string} [defVal]
         * @return {string}
         * @private
         */
        _: function(path, message, defVal){
            var dirs = null;
            if(!path){
                dirs = this._currentLang;
            } else {

                dirs = [this._currentLang].concat(path.split(this._delimiter));
            }

            if(!dirs) return null;

            if(!defVal) defVal = message;




            var messagesRef = this._messages;
            for(var i = 0; i < dirs.length; i++){
                if( !messagesRef.hasOwnProperty(dirs[i]) ) break;

                messagesRef = messagesRef[dirs[i]];
            }

            return messagesRef.hasOwnProperty(message) ? messagesRef[message] : defVal;
        }
    };


    core.l10n = new L10n();
    core.L10n = L10n;

})(core, document);