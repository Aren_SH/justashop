;(function( core, document ){

    "use strict";

    var queues = {
        queues:{}
        ,
        _dispatcher: new core.events.Dispatcher()
        ,
        /**
         * @param  {string}          queue
         * @param  {function|object} callback
         * @return {number}
         */
        track: function ( queue, callback ){
            var eventType = getEventType( queue );
            return this._dispatcher.addListener( eventType, callback );
        }
        ,
        /**
         * @param  {string}          queue
         * @param  {function|object|number} callback
         * @return {boolean}
         */
        untrack: function ( queue, callback ){
            var eventType = getEventType( queue );
            return this._dispatcher.removeListener( eventType, callback );
        }
        ,
        /**
         * @param {string} name
         * @param {*} data
         */
        enqueue: function ( name, data ){
            if( !this.queues[name] ){
                this.queues[name] = [];
            }

            var prevLength = this.queues[name].length;

            this.queues[name].push( data );

            var event = new core.events.Event( getEventType( name ), null );
            this._dispatcher.trigger( event );

            if( prevLength == this.queues[name].length ){
                this.dequeue( name );
            }
        }
        ,
        /**
         *
         * @param  {string} name
         * @return {*}
         */
        dequeue: function ( name ){
            var queue = this.getQueue( name );

            return queue && queue.length > 0 ? queue.pop() : null;
        }
        ,
        /**
         *
         * @param  {string} name
         * @return {*}
         */
        getLast: function ( name ){
            var queue = this.getQueue( name );

            return queue && queue.length > 0 ? queue[queue.length - 1] : null;
        }
        ,
        /**
         *
         * @param  {string} name
         * @return {*}
         */
        getQueue:function ( name ){
            if( !this.queues[name] || !this.queues[name].hasOwnProperty( 'length' ) ){
                return null;
            }

            return this.queues[name];
        }
    };

    function getEventType( queue ){
        return 'queues.' + queue + '.new';
    }

    core.queues = queues;
}( core, document ) );