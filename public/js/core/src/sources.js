;(function( core, document ){

    "use strict";

    var sources = {
        sources:{}
        ,
        /**
         * @param {string}          domain
         * @param {function|object} callback
         */
        register: function ( domain, callback ){
            this.sources[domain] = callback;
        }
        ,
        /**
         * @param {string} domain
         */
        deregister: function ( domain ){
            try{
                delete this.sources[domain];
            } catch ( error ){}
        }
        ,
        /**
         * @param  {string} method
         * @param  {{}}     [params]
         * @return {*}
         */
        request: function ( method, params ){
            var domain = null;
                params = typeof( params ) == 'undefined' ? {} : params;

            var dotIndex = method.indexOf( '.' );
            if( dotIndex == -1 ){
                domain = method;
                method = null;
            } else {
                domain = method.substr(0, dotIndex);
                method = method.substr(dotIndex);
            }

            if( !this.sources[domain] ){
                return null;
            }

            if( this.sources[domain] instanceof Object && this.sources[domain].hasOwnProperty( 'handleRequest' ) ){
                return this.sources[domain].handleRequest( method, params );
            }
            else if ( this.sources[domain] instanceof  Function ){
                return this.sources[domain]( method, params );
            }

            return null;


        }
    };

    core.sources = sources;
}( core, document ) );