;
(function (core, document) {
    core.ui = typeof( core.ui ) == 'undefined' ? {} : core.ui;


    /**
     *
     * @param       {{}} settings
     * @constructor
     */
    function Window(settings) {
        settings = typeof( settings ) == 'undefined' ? {} : settings;
        core.utils.extend(this.settings, settings);

        this.title = settings.title;
        this.id = 'window-' + Math.floor(Math.random() * 100);

        this.element = null;
        this.content = null;

        this._boundEvents = {};
        this._dispatcher = new core.events.Dispatcher();
    }

    Window.prototype = {
        constructor: Window,

        /** @type {{}} */
        settings: {
            title: '',
            draggable: false
        },

        /** @type {{}} */
        controlClasses: {
            btnClose: 'js-window-close'
        },

        /** @type {{}} */
        styleClasses: {
            btnClose: 'close'
        },

        /** @type {Node} */
        element: null,

        /** @type {String} */
        content: null,

        /** @type {{}} */
        _boundEvents: {},

        /** @type {Dispatcher} */
        _dispatcher: null,


        /**
         * @param {Node} context
         */
        open: function (context) {
            var openEvent     =  new core.events.Event('window.open', this);
            var triggerResult = this._dispatcher.trigger(openEvent);

            if( openEvent.isDefaultPrevented() || triggerResult === false ){
                return;
            }

            this.init();
            this.render(context);
        },


        init: function () {
            this.prepareHtml();
            this.bindEvents();
        },


        bindEvents: function () {
            this._bindBaseEvents();
            this._bindEvents();
        },


        _bindBaseEvents: function () {
            this._boundEvents['closeBtn-click'] = this.close.bind(this);

            var closeBtns = core.dom.querySelectorAll('.js-window-close', this.element);
            for (var i = 0; i < closeBtns.length; i++) {
                core.dom.addEventListener(closeBtns[i], 'click', this._boundEvents['closeBtn-click']);
            }
        },


        _bindEvents: function () {
        },


        unbindEvents: function () {
            this._unbindBaseEvents();
            this._unbindEvents();
        },


        _unbindBaseEvents: function () {
            var closeBtns = core.dom.querySelectorAll('.js-window-close', this.element);
            for (var i = 0; i < closeBtns.length; i++) {
                core.dom.removeEventListener(closeBtns[i], 'click', this._boundEvents['closeBtn-click']);
            }
        },


        _unbindEvents: function () {
        },


        prepareHtml: function () {
            throw new Error("Window.prepareHtml is abstract. Override it");
        },


        /**
         *
         * @param {Node} context
         */
        render: function (context) {
            context = typeof( context ) == 'undefined' ? core.dom.getBody() : context;
            context.appendChild(this.element);
        },


        close: function () {
            var closeEvent    =  new core.events.Event('window.close', this);
            var triggerResult = this._dispatcher.trigger(closeEvent);

            if( closeEvent.isDefaultPrevented() || triggerResult === false ){
                return;
            }

            this.unbindEvents();
            core.dom.remove(this.element);
        }
    };

    core.ui.Window = Window;

}(core, document));