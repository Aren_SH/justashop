(function(document, core){
    var flashMessages = new core.components.FlashMessages();
    flashMessages.init({
        containerSelector: '#js-message-container'
    });

    var account = new core.components.Account();
    account.init({
        urls: {
            currentUser: '/api/user/getCurrent',
            logIn:       '/api/user/logIn',
            logOut:      '/api/user/logOut',
            register:    '/api/user/register',
            activate:    '/api/user/activate'
        },
        selectors: {
            controls:            '#js-account-controls',
            authenticationModal: '#js-account-modal-auth',
            authenticationForm:  '#js-account-modal-auth-form',
            registrationModal:   '#js-account-modal-register',
            registrationForm:    '#js-account-modal-register-form',
            activationForm:      '#js-account-modal-register-activation-form'
        }
    });


    var cart = new core.components.Cart();
    cart.init({
        urls: {
            checkout: '/cart/checkout',
            add:      '/api/cart/add',
            set:      '/api/cart/set',
            list:     '/api/cart/list',
            clear:    '/api/cart/clear',
            remove:   '/api/cart/remove'
        },
        selectors: {
            controls:          '#js-cart-controls',
            dropdownElement:   '.js-cart-controls-element',
            scopeElement:      '#main-container',
            addModal:          '#js-cart-modal-add',
            addBtn:            '.js-cart-showCountModal-btn'
        }
    });


    core.components.ImageSlider.Factory( $('.js-image-slider').toArray() );


})(document, core);
