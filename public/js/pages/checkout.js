;(function (document, core, $) {
    var selectors = {
        deliverySelect: '#field-delivery_type_id',
        deliveryPrice:  '.js-cart-delivery-price',
        deliveryRow:    '.js-cart-delivery-row',
        cartItemRow:    '.js-cart-item-row',
        cartItemPrice:  '.js-cart-item-price',
        cartItemRemove: '.js-cart-item-remove',
        totalSum:       '.js-cart-total-sum'
    };


    var form = document.getElementById('checkout-form');
    var messages = {
        name: {
            required: core.l10n._('cart.checkout.validation', 'name required'),
            minlength: core.l10n._('cart.checkout.validation', 'name too short'),
            maxlength: core.l10n._('cart.checkout.validation', 'name too long')
        },

        email: {
            required: core.l10n._('cart.checkout.validation', 'email required'),
            email: core.l10n._('cart.checkout.validation', 'invalid email format')
        },

        phone: {
            required: core.l10n._('cart.checkout.validation', 'phone number required'),
            pattern: core.l10n._('cart.checkout.validation', 'invalid phone number format')
        },

        'zip_code': {
            required: core.l10n._('cart.checkout.validation', 'zip-code required')
        },

        'address': {
            required: core.l10n._('cart.checkout.validation', 'address required'),
            minlength: core.l10n._('cart.checkout.validation', 'address too short')
        }
    };


    if (form) {
        var formValidation = new core.components.FormValidation(form, messages);
    }

    var CartController = {

        _elements: {},


        init: function (){

            this._elements.$deliveryRow    = $(selectors.deliveryRow);
            this._elements.$deliveryPrice  = $(selectors.deliveryPrice);
            this._elements.$deliverySelect = $(selectors.deliverySelect);

            this._elements.$totalSum = $(selectors.totalSum);

            this._bindEvents();
            this._elements.$deliverySelect.trigger('change');
        },

        _bindEvents: function(){
            this._elements.$deliverySelect.on('change', this._onDeliveryOptionSelect.bind(this));
            $(selectors.cartItemRemove).click(this._onCartItemRemoveBtnClick.bind(this));
        },

        _onDeliveryOptionSelect: function(e){

            var deliveryPrice = 0;
            var select = e.target;
            var option = select.options[select.selectedIndex];
            if(option){
                deliveryPrice = Number(option.getAttribute('data-price'));
            }

            console.log(option, deliveryPrice);
            this._elements.$deliveryPrice.attr('data-value', deliveryPrice).text(core.utils.format.moneyFormat(deliveryPrice));

            if(deliveryPrice){
                this._elements.$deliveryRow.removeClass('hidden');
            } else {
                this._elements.$deliveryRow.addClass('hidden');
            }

            this._updateTotalPrice();
        },

        _onCartItemRemoveBtnClick: function(e){
            var totalRows =  $(selectors.cartItemRow).length;
            if( totalRows < 2 ) return false;
            var row = $(e.target).parents('tr');

            if( row ) row.remove();

            this._updateTotalPrice();

            if( totalRows - 1 < 2 ){
                $(selectors.cartItemRemove).hide();
            }
        },


        _updateTotalPrice: function(){
            this._elements.$totalSum.text(core.utils.format.moneyFormat(this._calculateTotalPrice()));
        },

        _calculateTotalPrice: function () {
            var total = 0;

            $(selectors.cartItemPrice).each(function () {
                total += Number(this.getAttribute('data-value'));
            });

            total += Number(this._elements.$deliveryPrice.attr('data-value'));
            console.log(total);
            return total;
        }
    };

    $(function () {
        CartController.init();
    });

})(document, core, $);