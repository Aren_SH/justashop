(function(document, core, $){

    var classes = {
        optionActive: "list-group-item-active"
    };

    $(function(){
        var $payBtn  = $('#js-pay-btn');
        var $modal   = $('#js-order-payment-modal');
        var $options = $('.js-payment-option');
        var $payText = $('#js-pay-text');

        var $proceedBtn     = $('#js-proceed-btn');
        var $adapterInputEl = $('#js-payment-adapter');

        console.log($payText);

        $payBtn.click(function(e){
            e.preventDefault();
            $modal.modal('show');
        });


        $options.click(function(e){
            $options.removeClass(classes.optionActive);
            $payText.css('opacity', '0');

            $proceedBtn.removeAttr('disabled');
            core.dom.addClass(e.delegateTarget, classes.optionActive);

            if(!!e.delegateTarget.getAttribute('data-show-pay-text')){
                $payText.css('opacity', '1');
            }

            $adapterInputEl.val(e.delegateTarget.getAttribute('data-id'));
        })
    });

})(document, core, $);