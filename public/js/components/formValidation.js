;(function(document, core, $){

    var defaultOptions = {
        selectors: {
            submitBtn:      '.js-submit-btn',
            fieldContainer: '.form-group',
            field:          '.form-control',
            error:          '.js-error'
        },

        classes: {
            error: 'bg-danger text-danger js-error',
            containerError: 'has-error',
            containerValid: 'has-success',
            fieldError: '',
            submitBtnError: 'btn-danger disabled',
            submitBtnValid: 'btn-success'
        }
    };


    /**
     *
     * @param {HTMLFormElement} form
     * @param {{}}              [messages]
     * @param {{}}              [options]
     * @constructor
     */
    function FormValidation( form, messages, options ){
        this._form      = form;
        this._validator = new core.utils.Validator(messages);

        this._messages  = messages || {};
        this._elements  = {};
        this._options   = {};
        core.utils.extend( core.utils.extend(this._options, defaultOptions), options || {} );

        this._init();
    }


    FormValidation.prototype = {
        constructor: FormValidation,

        _form: null,
        _validator: null,
        _messages: {},
        _elements: {},
        _options: {},

        _init: function(){
            this._elements.$form = $(this._form);
            this._elements.$submitBtn = this._elements.$form.find(this._options.selectors.submitBtn);

            console.log(this._options);
            this._bindEvents();
        },


        _bindEvents: function(){
            this._elements.$form.on('submit',  this._onFormSubmit.bind(this));
            this._elements.$form.on('invalid', this._onInvalid.bind(this));
            this._elements.$form.on('blur', 'input, select, textarea', this._onFieldChange.bind(this));
        },

        _onFormSubmit: function(e){
            e.preventDefault();
            var errors = this._validator.validateForm(this._form);
            if( errors ){
                this._setBtnError();
                this._setErrors(errors);
                return false;
            }

            this._form.submit();
            return true;
        },

        _onInvalid: function(e){
            e.preventDefault();
            var errors = this._validator.validateForm(this._form);
            if( errors ){
                this._setBtnError();
                this._setErrors(errors);
                return false;
            }

            return true;
        },

        _onFieldChange: function(e){
            var element = e.target;
            console.log(element);
            var errors  = this._validator.validateField(element);

            if( errors ){
                this._setFieldError(element, errors);
                this._setBtnError();
            } else {
                this._setFieldValid(element);
                this._setBtnValid();
            }
        },

        _setErrors: function(errors){
            errors = errors || {};

            for( var i = 0; i < this._form.elements.length; i++ ){
                if(!errors.hasOwnProperty(this._form.elements[i].name)){
                    this._setFieldValid(this._form.elements[i]);
                } else {
                    this._setFieldError(this._form.elements[i], errors[this._form.elements[i].name]);
                }
            }
        },


        _cleanField: function(element){
            var $element = $(element);
            var $parent  = $element.parents(this._options.selectors.fieldContainer);

            $parent.find(this._options.selectors.error).remove();
            $parent.removeClass(this._options.classes.containerError);
            $parent.removeClass(this._options.classes.containerValid);
        },


        _setFieldError: function(element, error){
            this._cleanField(element);

            var $element = $(element);
            var $parent  = $element.parents(this._options.selectors.fieldContainer);
            var errorEl  = document.createElement('p');

            errorEl.className = this._options.classes.error;
            errorEl.appendChild( document.createTextNode(error.join('<br/>')));

            $parent.addClass(this._options.classes.containerError);

            $(errorEl).insertAfter($element);
        },

        _setFieldValid: function(element){
            this._cleanField(element);

            var $element = $(element);
            var $parent  = $element.parents(this._options.selectors.fieldContainer);

            $parent.addClass(this._options.classes.containerValid);
        },


        _setBtnError: function(){
            this._elements.$submitBtn.removeClass(this._options.classes.submitBtnValid);
            this._elements.$submitBtn.addClass(this._options.classes.submitBtnError);
        },


        _setBtnValid: function(){
            this._elements.$submitBtn.removeClass(this._options.classes.submitBtnError);
            this._elements.$submitBtn.addClass(this._options.classes.submitBtnValid);
        }
    };



    core.components.FormValidation = FormValidation;

}(document, core, $));