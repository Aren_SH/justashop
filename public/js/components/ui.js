;(function(document, core, $){

   // $(function(){
        $(document).on('click', '.js-number-widget-inc', function(e){
            var input  = $(e.target).parents('.js-number-widget').find('.js-number-widget-input');
            var valNew = input.val()? Number(input.val()) + 1 : 1;
            var valMax = input.attr('data-max') ? Number(input.attr('data-max')) : null;
            if( valMax !== null && valMax < valNew ) valNew = valMax;

            input.val(valNew);
            input.trigger('change');
        });

        $(document).on('click', '.js-number-widget-dec', function(e){
            var input  = $(e.target).parents('.js-number-widget').find('.js-number-widget-input');
            var valNew = input.val()? Number(input.val())  - 1 : -1;
            var valMin = input.attr('data-min') ? Number(input.attr('data-min')) : null;
            if( valMin !== null && valMin > valNew ) valNew = valMin;

            input.val(valNew);
            input.trigger('change');
        });
   // })


}(document, core, $));