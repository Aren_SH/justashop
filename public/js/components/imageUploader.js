;(function(document, core, $, mOxie){

    "use strict";

    var DEFAULT_FIELD_NAME = 'images';
    var IMAGE_ATTRIBUTES   = ['url', 'fieldName', 'index', 'title', 'description', 'id', 'hash', 'is_main'];

    if( core.components == undefined ) core.components = {};


    function ProgressBar(){
        this._total = 0;
        this._current = 0;
        this._elements = {};
    }

    ProgressBar.prototype = {
        constructor: ProgressBar,
        _total: 0,
        _current: 0,
        _elements: {},

        init: function(element){
            this._elements.$bar = $(element);
        },

        start: function(total){
            this._total   = total;
            this._current = 0;

            this.addProgress(0);
            this._elements.$bar.parent().addClass('active');
        },


        addProgress: function(val){
            this._current += val;
            var progress = Math.round( (this._current / this._total) * 100 );
            this._updateBar(progress);
            if( progress == 100) {
                this.stop();
            }
        },

        setProgress: function(val){
            this._current = val;
            var progress = Math.round( (this._current / this._total) * 100 );

            this._updateBar(progress);
            if( progress == 100) {
                this.stop();
            }
        },

        stop: function(){
            this._elements.$bar.parent().removeClass('active');
        },

        _updateBar: function(progress){
            this._elements.$bar.css('width', progress + '%');
            this._elements.$bar.attr('aria-valuenow', progress);
            this._elements.$bar.text(progress + '%');

        }
    };




    function ImageUploader () {
        this._images = {};
        this._elements = {};
        this._selectors = {};
        this._urls = {};
        this._templates = {};
        this._fieldName = null;
        this._index = 1;
        this._progressBar = null;
        this._filesTotal = 0;
        this._filesLoaded = 0;
    }

    ImageUploader.Factory = function(elements, params){
        var res = [];

        if(!elements.length) return res;

        for( var i = 0; i < elements.length; i++){
            params.container = elements[i];
            var uploader = new ImageUploader();
                uploader.init(params);

            res.push(uploader);
        }

        return res;
    };

    ImageUploader.prototype = {
        constructor: ImageUploader,
        _images: {},
        _elements: {},
        _selectors: {},
        _urls: {},
        _templates: {},
        _fieldName: null,
        _index: 1,
        _progressBar: null,
        _filesTotal: 0,
        _filesLoaded: 0,


        init: function(params){
            this._selectors = params.selectors;
            this._urls      = params.urls;
            this._templates = params.templates;

            this._elements.$container  = $(params.container);
            this._elements.$uploadBtn  = this._elements.$container.find(params.selectors.uploadButton);
            this._elements.$imagesList = this._elements.$container.find(params.selectors.imagesList);
            this._elements.filePicker  = new mOxie.FileInput({ browse_button: this._elements.$uploadBtn.get(0), multiple: true });

            this._fieldName = this._elements.$container.attr('data-field-name');
            this._fieldName = this._fieldName || DEFAULT_FIELD_NAME;

            this._progressBar = new ProgressBar();
            this._progressBar.init(this._elements.$container.find(params.selectors.progressBar).get(0));

            this._setEventHandlers();
            this._processExistedImages();

            this._elements.filePicker.init();

            this._elements.$uploadBtn.text( core.l10n._('common', 'upload'));
        },


        _setEventHandlers: function(){
            this._elements.filePicker.bind('change', this.uploadSelectedFiles.bind(this));
            this._elements.$imagesList.on('click', this._selectors.removeBtn, this._removeImage.bind(this) );
        },


        _processExistedImages: function (){
            var $imageElements = this._elements.$imagesList.find(this._selectors.existedImage);

            if(!$imageElements.length) return;

            var data          = null;
            var $imageElement = null;
            for( var i = 0; i < $imageElements.length; i++){
                $imageElement = $imageElements.get(i);
                data = {};
                for( var j = 0; j < IMAGE_ATTRIBUTES.length; j++){
                    data[IMAGE_ATTRIBUTES[j]] = $imageElement.getAttribute('data-' + IMAGE_ATTRIBUTES[j]);
                }

                this.addImage(data);
            }
        },


        _removeImage: function(e){
            var $target = $(e.target);

            var $image = $target.parents(this._selectors.image);
            $image.remove();
        },


        uploadSelectedFiles: function(e){
            var el = e.target;
            var files = el.files;

            if( files && files.length){
                this._filesTotal = files.length;
                this._progressBar.start(this._filesTotal);
                for (var i = 0; i< files.length; i++){
                    this._upload(files[i]);
                }
            }

        },


        _upload:function(file){
            var xhr      = new mOxie.XMLHttpRequest();
            var formData = new mOxie.FormData();

            formData.append('images', file);

            xhr.open('POST', this._urls.upload, false);
            xhr.bind('readystatechange', this._uploadStateChange.bind(this));
            xhr.upload.onprogress = this._uploadProgressChange.bind(this);
            xhr.send(formData);

        },


        _uploadStateChange: function(e){
            var xhr = e.target;
            if(xhr.readyState == mOxie.XMLHttpRequest.DONE){
                this._uploadComplete(xhr);
            }
        },


        _uploadProgressChange: function(e){
            var progress = (e.total - e.loaded)/ e.total;
            this._progressBar.addProgress( progress );
        },


        _uploadComplete: function (xhr){
            this._progressBar.setProgress( ++this._filesLoaded );

            var data = _prepareResponseData(xhr.responseText);
            if( !data ) return;
            for( var i = 0; i < data.length; i++){
                if( Object.keys(this._images).length == 0){
                    data.is_main = 1;
                }
                this.addImage(data[i]);
            }

            if( this._filesLoaded == this._filesTotal ){
                this._filesLoaded = this._filesLoaded = 0;
            }
        },


        addImage: function(data){
            data.fieldName    = this._fieldName;
            data.index        = this._index++;

            data.label_title       = core.l10n._('imageLoader.image', 'title');
            data.label_description = core.l10n._('imageLoader.image', 'description');

            data.is_main = data.is_main && data.is_main == 1 ? 'checked="checked"' : '';

            var html  = this._templates.imageRow;

            for( var i = 0; i < IMAGE_ATTRIBUTES.length; i++){
                var regexp = new RegExp( '\:' + IMAGE_ATTRIBUTES[i] + '\:', 'g');
                html  = html.replace(regexp, data[IMAGE_ATTRIBUTES[i]] || '');
            }

            var labels = ['title', 'description', 'is_main' ];
            for( i = 0; i < labels.length; i++){
                var regexp = new RegExp( '\:label-' + labels[i] + '\:', 'g');
                html  = html.replace(regexp, core.l10n._('imageLoader.image', labels[i]));
            }

            this._elements.$imagesList.append(html);

            this._images[data.id] = data;
        }
    };

    /**
     *
     * @param  {*} data
     * @return {*}
     * @private
     */
    function _prepareResponseData(data){
        if( data) {
            try{
                data = $.parseJSON(data);
            } catch(e){
                data = null;
            }
        } else {
            data = null;
        }

        return data;
    }


    core.components.ImageUploader = ImageUploader;

}(document, core, $, mOxie));