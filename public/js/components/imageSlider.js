;(function(document, core, $){

    "use strict";

    var defaultOptions = {
        selectors: {
            container:  '.js-image-slider',
            viewport:   '.js-image-slider-view',
            thumbnails: '.js-image-slider-thumbnails',
            tape:       '.js-image-slider-tape',
            thumbnail:  '.js-image-slider-thumbnail'
        },

        classes: {
            thumbnailSelected: 'image-slider-thumbnail-selected'
        }
    };

    if( core.components == undefined ) core.components = {};

    function ImageSlider( element, options){
        this._inAnimation = false;
        this._images   = [];
        this._elements = {container: element};
        this._options  = {};
        this._tmpImage = null;
        core.utils.extend(this._options, defaultOptions);
        core.utils.extend( this._options, options || {});

        this.init();
    }


    ImageSlider.Factory = function(elements, params){
        var res = [];

        if(!elements.length) return res;

        for( var i = 0; i < elements.length; i++){
            var slider = new ImageSlider(elements[i], params);

            res.push(slider);
        }

        return res;
    };


    ImageSlider.prototype = {
        constructor: ImageSlider,

        _images: [],

        _elements: {},

        _options: {},

        _tmpImage: null,


        init: function(){
            this._tmpImage = new Image();

            this._elements.$container = $(this._elements.container);
            this._elements.$viewport  = this._elements.$container.find(this._options.selectors.viewport);

            this._bindEvents();
        },


        _bindEvents: function(){
            this._tmpImage.addEventListener('load', this._onImageLoad.bind(this));
            $(this._elements.container).on('click', this._options.selectors.thumbnail, this._onThumbnailClick.bind(this));
        },


        _onThumbnailClick: function(e){
            if( this._inAnimation ) return;

            var target = $(e.target);

            if( target.hasClass(this._options.classes.thumbnailSelected)) return;

            this._elements.$container.find(this._options.selectors.thumbnail)
                .removeClass(this._options.classes.thumbnailSelected);

            target.addClass(this._options.classes.thumbnailSelected);
            var bigImageSrc = e.target.getAttribute('data-big');
            if(bigImageSrc){
                this._inAnimation = true;
                var that = this;
                this._elements.$viewport.fadeTo(300, 0, function(){
                    that._tmpImage.src = bigImageSrc;
                });
            }
        },

        _onImageLoad: function(e){
            this._elements.$viewport.attr('src', this._tmpImage.src);
            this._elements.$viewport.fadeTo(300, 1);
            this._inAnimation  = false;
        }

    };


    core.components.ImageSlider = ImageSlider;

}(document, core, $));