;(function(document, core, $){

    "use strict";

    if( core.components == undefined ) core.components = {};

    var QUEUE_NAME     = 'cart';

    var STATUS_ERROR   = 'error';
    var STATUS_SUCCESS = 'success';

    var cartItems = {};

    /**
     * @constructor
     */
    function Cart(){
    }

    Cart.prototype = {
        constructor: Cart,

        /**
         * @param {{}}
         * @private
         */
        _elements: {},

        /**
         * @param {{}}
         * @private
         */
        _selectors: {},

        /**
         * @param {{}}
         * @private
         */
        _urls: {},

        /**
         * @param {{}}
         * @private
         */
        _errors: {},


        _modals: {},


        /**
         * @param {Config} params
         */
        init: function(params){
            this._elements.$scope    = params.selectors.scopeElement ? $(params.selectors.scopeElement) : $(document);
            this._elements.$controls = $(params.selectors.controls);

            this._urls  = params.urls;

            this.errors = params.errors;

            this._selectors = params.selectors;


            this._modals.add = new CartModal();
            this._modals.add.init(params.selectors.addModal);

            this._initControls();
            this._bindEvents();

            this._loadCart();

        },


        showAddModal: function(newData){
            var data = {id: '', title: '', price: '', thumbnail: ''};

            core.utils.extend(data, newData);
            this._modals.add.showAddDialog(data);


        },


        showChangeModal: function(data){
            console.log(data);
            this._modals.add.showChangeDialog(data);
        },

        add: function(id, count){

            var request = $.post(this._urls.add, {product_id: id, count: count});

            request.then(
                this._onAddRequestSuccess.bind(this),
                this._commonRequestFail.bind(this)
            );
        },

        set: function(id, count){

            var request = $.post(this._urls.set, {product_id: id, count: count});

            request.then(
                this._onAddRequestSuccess.bind(this),
                this._commonRequestFail.bind(this)
            );
        },

        remove: function(id, count){

            var request = $.post(this._urls.remove, {product_id: id, count: count});

            request.then(
                this._onRemoveRequestSuccess.bind(this),
                this._commonRequestFail.bind(this)
            );
        },

        clear: function(){

            var request = $.get(this._urls.clear);

            request.then(
                this._onClearRequestSuccess.bind(this),
                this._commonRequestFail.bind(this)
            );
        },


        updateControls: function(){
            //todo: "cart is empty"
            var count =  this._countItems();
            this._elements.controlButtonCount.firstChild.nodeValue = count;

            if( count ) {
                core.dom.removeClass(this._elements.controlButtonCount, 'hidden')
            } else {
                core.dom.addClass(this._elements.controlButtonCount, 'hidden')
            }

            core.dom.clear(this._elements.controlDropdown);
            var fragment = document.createDocumentFragment();
            if( count > 0 ){
                var table    = document.createElement('table');
                    table.className = 'table cart-dropdown-table';
                for( var id in cartItems ){
                    table.appendChild(this._createCartItemRow(cartItems[id]));
                }

                var total = core.dom.el.p(
                    {className: 'cart-dropdown-footer'},
                    [
                        core.dom.el.span({className:'cart-dropdown-total'}, this._getTotalSumString()),
                        core.dom.el.a(
                            {
                                className: 'js-cart-dropdown-check btn btn-default btn-sm pull-right',
                                href:      this._urls.checkout + '?referrer=' + encodeURIComponent(window.location.pathname)
                            },
                            core.l10n._('cart.controls', 'checkout')
                        )
                    ]
                );

                fragment.appendChild(table);
                fragment.appendChild(total);

            } else {
                var emptyMsg = core.dom.el.p({className:'text-center text-muted'}, core.l10n._('cart.controls', 'cart-empty'));
                fragment.appendChild(emptyMsg);
            }

            this._elements.controlDropdown.appendChild(fragment);

        },

        _getTotalSumString: function(){

            return core.l10n._('cart.controls', 'total')
                   + ': ' + _moneyFormat(this._calculateItemsTotalPrice())
                   + ' '  + core.l10n._('common', 'currency-sign');
        },


        _initControls: function(){
            this._createControls();

        },

        _createControls: function(){
            var button = core.dom.el.button(
                { className: 'btn btn-default navbar-btn js-cart-controls-btn'},
                [
                    core.dom.el.span({className: 'glyphicon glyphicon-shopping-cart'}, null ),
                    core.dom.el.span({className: 'hidden-xs'}, ' ' + core.l10n._('cart.controls', 'cart').toUpperCase() + ' ' ),
                    core.dom.el.span({className: 'js-cart-controls-count badge hidden'}, '')
                ]
            );

            var dropdown = core.dom.el.div({className: 'dropdown-menu cart-dropdown'}, null);

            this._elements.controlButton      = button;
            this._elements.controlButtonCount = button.childNodes[2];
            this._elements.controlDropdown    = dropdown;

            this._elements.$controls.append(button);
            this._elements.$controls.append(dropdown);
        },

        _loadCart: function(){

            var request = $.get(this._urls.list);

            request.then(
                this._onListRequestSuccess.bind(this),
                this._commonRequestFail.bind(this)
            );
        },


        /**
         * @private
         */
        _bindEvents: function(){
            //core.queues.track(QUEUE_NAME, this._addMessageFromQueue.bind(this));
            this._elements.$scope.on( 'click', this._selectors.addBtn, this._onAddBtnClick.bind(this));

            core.dom.addEventListener( this._elements.controlButton, 'click', this._onCartBtnClick.bind(this));

            this._elements.$controls.on( 'click', '.js-cart-dropdown-element-delete',   this._onElementDeleteClick.bind(this));
            this._elements.$controls.on( 'click', '.js-cart-dropdown-element-quantity', this._onElementQuantityClick.bind(this));

            this._modals.add.addEventListener('cartmodal.add', this._onModalAddBtnClick.bind(this));
            this._modals.add.addEventListener('cartmodal.change', this._onModalChangeBtnClick.bind(this));
            this._modals.add.addEventListener('cartmodal.remove', this._onModalRemoveBtnClick.bind(this));
        },


        _onCartBtnClick: function(e){
               this._toggleCartDropdown();
        },

        _onAddBtnClick: function(event){

            var data = this._getDataFromElement(event.target != event.currentTarget ? event.currentTarget : event.target);
            this.showAddModal(data);
        },

        _onDropdownElementClick: function(event){
            var id = Number(event.target.getAttribute('data-id'));
            this.showChangeModal(this._getItemById(id));
        },

        _onElementQuantityClick: function(event){
            console.log('click');
            var target = event.target != event.currentTarget ? event.currentTarget : event.target;
            var id = Number(target.getAttribute('data-id'));
            this.showChangeModal(this._getItemById(id));
        },

        _onElementDeleteClick: function(event){
            console.log('click');
            var target = event.target != event.currentTarget ? event.currentTarget : event.target;
            console.log(target, id);
            var id     = Number(target.getAttribute('data-id'));
            var item   = this._getItemById(id);

            if( !item ) return;
            this.remove(item.id, item.count);
        },

        _onModalAddBtnClick: function(event){
            var data = event.data;
            this.add(data.product_id, data.count);
        },

        _onModalChangeBtnClick: function(event){
            var count = event.data.count;
            var item  = this._getItemById(event.data.product_id);

            if( item.count > count ){
                this.remove(item.id, item.count - count);
            }
            if( item.count < count ){
                this.add(item.id, count - item.count);
            }
        },

        _onModalRemoveBtnClick: function(event){
            var item = this._getItemById(event.data.product_id);
            this.remove(item.id, item.count);
        },


        _getDataFromElement: function(element){
            var data = {};

            ['thumbnail', 'price', 'title', 'id', 'count'].map( function(prop){
                data[prop] = element.getAttribute( 'data-' + prop );
            });

            return data;
        },


        _onAddRequestSuccess: function(data, status, jqXHR){
            data = _prepareResponseData(data);

            if( !data ){
                //showError
                return;
            }

            if( data.status == STATUS_ERROR ){
                //show errors
            }

            if( data.status == STATUS_SUCCESS && data.items){
                this._addItems(data.items);
                this.updateControls();
            }
        },


        _onRemoveRequestSuccess: function(data, status, jqXHR){
            data = _prepareResponseData(data);

            if( !data ){
                //showError
                return;
            }

            if( data.status == STATUS_ERROR ){
                //show errors
            }

            if( data.status == STATUS_SUCCESS && data.items){
                this._resetItems(data.items);
                this.updateControls();
            }
        },


        _onListRequestSuccess: function(data, status, jqXHR){
            data = _prepareResponseData(data);

            if( !data ){
                //showError
                return;
            }

            if( data.status == STATUS_ERROR ){
                //show errors
            }

            if( data.status == STATUS_SUCCESS && data.items){
                this._addItems(data.items);
                this.updateControls();
            }
        },



        _onClearRequestSuccess: function(data, status, jqXHR){
            data = _prepareResponseData(data);

            if( !data ){
                //showError
                return;
            }

            if( data.status == STATUS_ERROR ){
                //show errors
            }

            if( data.status == STATUS_SUCCESS && data.items){
                this._resetItems(data.items);
                this.updateControls();
            }
        },


        _commonRequestFail: function( jqXHR, textStatus, errorThrown ){
            console.log('fail ' + textStatus + ' ' + errorThrown);

        },


        _toggleCartDropdown: function(){
            core.dom.toggleClass( this._elements.controlButton.parentNode, 'open');
        },


        _hideCartDropdown: function(){
            core.dom.removeClass( this._elements.controlButton.parentNode, 'open');
        },



        _getItemById: function(id){
            console.log(id);
            if(!cartItems.hasOwnProperty(id)) return null;

            return cartItems[id];
        },

        _addItems: function(items){
            if( !core.utils.isArray(items)) items = [items];

            for(var i = 0; i < items.length; i++ ){
                cartItems[items[i].id] = items[i];
            }

        },


        _resetItems: function(items){
            cartItems = {};
            this._addItems(items);
        },


        _countItems: function(){
            var count = 0;
            for(var id in cartItems){
                if(!cartItems.hasOwnProperty(id)) continue;
                count += Number(cartItems[id].count);
            }

            return count;
        },


        _calculateItemsTotalPrice: function(){
            var price = 0;
            for(var id in cartItems){
                if(!cartItems.hasOwnProperty(id)) continue;
                price += cartItems[id].totalPrice;
            }

            return price;
        },


        _createCartItemRow: function(item){
            var innerElements = [];
            if(item.thumbnail){
                innerElements.push(
                    core.dom.el.td(
                        {className: 'cart-dropdown-element-thumbnail'},
                        core.dom.el.img({src:item.thumbnail, className:'cart-dropdown-thumbnail'}, null )
                    )
                );
            }
            innerElements.push(
                core.dom.el.td(
                    {className: 'cart-dropdown-element-title'},
                    core.dom.el.a({'href': item.url || '#'}, item.title ) )
            );
            innerElements.push(
                core.dom.el.td(
                    {className: 'cart-dropdown-element-quantity'},
                    core.dom.el.a(
                        {
                            'data-id': item.id,
                            href:      '#',
                            title:     core.l10n._('common', 'change'),
                            className: 'js-cart-dropdown-element-quantity'
                        },
                        item.count
                    )
                )
            );

            innerElements.push(
                core.dom.el.td(
                    {className: 'cart-dropdown-element-actions'},
                    core.dom.el.a(
                        {
                            'data-id': item.id,
                            href:      '#',
                            title:     core.l10n._('common', 'delete'),
                            className: 'js-cart-dropdown-element-delete'
                        },
                        core.dom.el.b({className: 'close'}, '×') )
                )
            );

            return core.dom.el.tr(
                    {className: 'js-cart-controls-element', 'data-id': item.id},
                    innerElements
            );
        },


        _pushMessage: function(type, message){
            var data = {
                type:    type,
                message: message
            };

            core.queues.enqueue('flashMessages', data);
        }



    };



    function CartModal(){
        this._elements = {};
        this._dispatcher     =  new core.events.Dispatcher();
        this._currentProduct = null;

        this._selectors = {};
        core.utils.extend(this._selectors, this._defaultSelectors);
    }
    CartModal.prototype = {
        constructor: CartModal,

        _elements: {},

        _currentProduct: null,

        _dispatcher: null,

        _selectors: {},

        _defaultSelectors: {
            productId: '.js-cart-modal-add-product-id',
            price:     '.js-cart-modal-add-price',
            title:     '.js-cart-modal-add-title',
            count:     '.js-cart-modal-add-count',
            thumbnail: '.js-cart-modal-add-thumbnail',
            addBtn:    '.js-cart-modal-add-btn',
            saveBtn:   '.js-cart-modal-save-btn',
            removeBtn: '.js-cart-modal-remove-btn',
            newBtns:   '.js-cart-modal-new-btns',
            changeBtns:'.js-cart-modal-change-btns'
        },


        /**
         *
         * @param {string} element
         * @param {Object.<string>} [selectors]
         */
        init: function( element, selectors ){
            if( selectors ){
                core.utils.extend(this._selectors, selectors);
            }

            this._elements.$modal = $(element);

            this._elements.$productId  = this._elements.$modal.find(this._selectors.productId);
            this._elements.$thumbnail  = this._elements.$modal.find(this._selectors.thumbnail);
            this._elements.$title      = this._elements.$modal.find(this._selectors.title);
            this._elements.$price      = this._elements.$modal.find(this._selectors.price);
            this._elements.$count      = this._elements.$modal.find(this._selectors.count);
            this._elements.$addBtn     = this._elements.$modal.find(this._selectors.addBtn);
            this._elements.$saveBtn    = this._elements.$modal.find(this._selectors.saveBtn);
            this._elements.$removeBtn  = this._elements.$modal.find(this._selectors.removeBtn);
            this._elements.$newBtns    = this._elements.$modal.find(this._selectors.newBtns);
            this._elements.$changeBtns = this._elements.$modal.find(this._selectors.changeBtns);

            this._bindEvents();
        },


        showAddDialog: function(data){
            data.count = 1;
            this._currentProduct = data;
            this._setElementsFromData(data);

            this._elements.$newBtns.css('display', 'block');
            this._elements.$changeBtns.css('display', 'none');
            this._elements.$modal.modal('show');
        },

        showChangeDialog: function(data){
            this._currentProduct = data;
            this._setElementsFromData(data);

            this._elements.$newBtns.css('display', 'none');
            this._elements.$changeBtns.css('display', 'block');
            this._elements.$modal.modal('show');
        },

        hide: function(){
            this._elements.$modal.modal('hide');
        },


        _bindEvents: function(){
            this._elements.$count.on('change', this._onCountChange.bind(this));

            this._elements.$addBtn.on('click', this._onAddBtnClick.bind(this));
            this._elements.$saveBtn.on('click', this._onSaveBtnClick.bind(this));
            this._elements.$removeBtn.on('click', this._onRemoveBtnClick.bind(this));
        },



        _onCountChange: function(e){
            var count = Number(e.target.value);

            this._elements.$price.html( _moneyFormat(this._currentProduct.price * count) + ' руб.');
        },


        _onAddBtnClick: function(e){
            var data = {};

            data.product_id = this._elements.$productId.val();
            data.count      = this._elements.$count.val();

            var event = new core.events.Event( 'cartmodal.add', this._elements.$addBtn.get(0), data);

            this._dispatcher.trigger( event );
            this.hide();
        },


        _onSaveBtnClick: function(e){
            var data = {};

            data.product_id = this._currentProduct.id;
            data.count      = this._elements.$count.val();

            var event = new core.events.Event( 'cartmodal.change', this._elements.$saveBtn.get(0), data);

            this._dispatcher.trigger( event );
            this.hide();
        },


        _onRemoveBtnClick: function(e){
            var data = {};

            data.product_id = this._currentProduct.id;
            data.count      = 0;

            var event = new core.events.Event( 'cartmodal.remove', this._elements.$removeBtn.get(0), data);

            this._dispatcher.trigger( event );
            this.hide();
        },

        _setElementsFromData: function(data){

            this._elements.$thumbnail.attr('src', data.thumbnail);
            this._elements.$title.html(data.title);
            this._elements.$price.html( _moneyFormat(data.price * data.count) + ' руб.');
            this._elements.$count.val(data.count);
            this._elements.$productId.val(data.id);

        },

        addEventListener: function(type, callback){
            this._dispatcher.addListener(type, callback);
        }

    };


    /**
     *
     * @param  {*} data
     * @return {ResponseData}
     * @private
     */
    function _prepareResponseData(data){
        if( data) {
            try{
                data = JSON.parse(data);
            } catch(e){
                data = null;
            }
        } else {
            data = null;
        }

        return data;
    }


    function _moneyFormat(num){
        var str = num.toFixed(2).replace(/./g, function(c, i, a) {
            return i && c !== "." && !((a.length - i) % 3) ? ' ' + c : c;
        });

        return str.replace('.', ',');
    }

    core.components.Cart = Cart;

}(document, core, $));