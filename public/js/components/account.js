;(function(document, core, $){

    "use strict";

    var USER_STATUS_NOT_VERIFIED = 2;
    var USER_STATUS_ACTIVE       = 1;
    var USER_STATUS_DISABLED     = 0;

    var RESPONSE_STATUS_SUCCESS = 'success';
    var RESPONSE_STATUS_ERROR   = 'error';


    /**
     * Response object
     * @typedef {{status: String, user: User, errors: {}, error: String }} ResponseData
     */


    /**
     * User object
     * @typedef {{id: Number, login: String, email: String, createdAt: String, status: Number }} User
     */


    if( core.components == undefined ) core.components = {};


    /**
     * @constructor
     */
    function Account(){
        this._user     = null;
        this._urls     = {};
        this._elements = {};
        this._activeForm = null;
    }

    Account.prototype.constructor = Account;

    Account.prototype = {
        /** @type {User} */
        _user:     null,
        _urls:     {},
        _elements: {},
        _activeForm: null,

        init: function( params ){
            this.validator = new core.utils.Validator();
            this._elements.$controls  = $(params.selectors.controls);
            this._elements.$authModal = $(params.selectors.authenticationModal);
            this._elements.$authForm  = $(params.selectors.authenticationForm);
            this._elements.$regModal  = $(params.selectors.registrationModal);
            this._elements.$regForm   = $(params.selectors.registrationForm);
            this._elements.$actForm   = $(params.selectors.activationForm);

            this._urls = params.urls;

            this.errors = params.errors;

            this._setEventHandlers();
        },
        _setEventHandlers: function(){

            $(document).ready(this.loadUserByToken.bind(this));
            this._elements.$controls.on('click', '#js-account-show-auth-btn', this.showAuthenticationForm.bind(this) );
            this._elements.$controls.on('click', '#js-account-log-out-btn', this.logOut.bind(this) );

            this._elements.$authModal.on('click', '#js-account-log-in-btn', this.logIn.bind(this) );
            this._elements.$authModal.on('click', '#js-account-show-register-btn', this.showRegistrationForm.bind(this) );

            this._elements.$regModal.on('click', '#js-account-register-btn',   this.register.bind(this) );
            this._elements.$regModal.on('click', '#js-account-activate-btn',   this.activate.bind(this) );
            this._elements.$regModal.on('blur',  '.js-account-register-field', this.validateField.bind(this) );
        }
        ,
        getCurrentUser: function(){
            return this._user;
        }
        ,
        loadUserByToken: function(){
            var request = $.get(this._urls.currentUser);

            request.then(
                this._loadUserByTokenSuccess.bind(this),
                this._commonRequestFail.bind(this)
            );

        }
        ,
        _loadUserByTokenSuccess: function(data){
            data = _prepareResponseData(data);
            if( !data ) return;

            if(data.status && data.status == RESPONSE_STATUS_SUCCESS){
                this._user = data.user;
            }

            this.updateControls();

        },
        _commonRequestFail: function( jqXHR, textStatus, errorThrown ){
            console.log('fail ' + textStatus + ' ' + errorThrown);

            this.updateControls();
        },
        hideModals: function(){
            this._elements.$authModal.modal('hide');
            this._elements.$regModal.modal('hide');
            this._activeForm = null;
        },
        updateControls: function(){
            if(this._user){
                this._showLogOutControl();
            } else {
                this._showLogInControl();
            }
        },
        _showLogInControl: function(){
            this._elements.$controls.empty().append(
                '<button id="js-account-show-auth-btn" type="button" class="btn btn-primary navbar-btn">Log in</button>'
            );
        },
        _showLogOutControl: function(){
            this._elements.$controls.empty().append(
                '<p class="btn navbar-btn disabled">Signed in as :name:</p>'.replace(':name:', this._user.email) +
                '<button id="js-account-log-out-btn" type="button" class="btn btn-default navbar-btn">Log Out</button>'
            );
        }
        ,
        showAuthenticationForm: function(){
            this.hideModals();
            this._elements.$authModal.modal('show');
            this._activeForm = this._elements.$authForm;
        },
        showRegistrationForm: function(){
            this.hideModals();
            this._elements.$regModal.modal('show');
            this._activeForm = this._elements.$regForm;
        },
        showActivationForm: function(){
            this.hideModals();
            this._elements.$regForm.removeClass('show').addClass('hidden');
            this._elements.$actForm.removeClass('hidden').addClass('show');

            this._elements.$regModal.modal('show');
            this._activeForm = this._elements.$actForm;
        },
        logOut:function(){
            var request = $.get(this._urls.logOut);
            request.then(
                $.proxy(this._logOutSuccess, this),
                $.proxy(this._commonRequestFail, this)
            );
        }
        ,
        _logOutSuccess: function(data, status, jqXHR){
            this._user = null;
            this.updateControls();
        }
        ,
        logIn: function(){
            var $form = this._elements.$authModal.find('form');
            var data  = $form.serialize();

            var request = $.post(this._urls.logIn, data);

            request.then(
                this._logInSuccess.bind(this),
                function(){}
            );

        },
        _logInSuccess: function(data, status, jqXHR){
            data = _prepareResponseData(data);

            if( data.status == RESPONSE_STATUS_SUCCESS ){
                this._user = data.user;

                if( this._user.status != USER_STATUS_ACTIVE ){
                    this.showActivationForm();
                } else {
                    this._hideCommonError();
                    this.hideModals();
                }

                this._elements.$authForm.get(0).reset();

            } else {
                this._user = null;
                this._showCommonError(data.error);
                this._pushMessage('danger', data.error);
            }

            this.updateControls();

        },
        register: function(){
            var $form = this._elements.$regForm;

            if(!this.validateForm($form.get(0))) return;

            var data = $form.serialize();

            var request = $.post(this._urls.register, data);

            request.then(
                this._registerSuccess.bind(this),
                this._commonRequestFail.bind(this)
            );
        },
        _registerSuccess: function(data, status, jqXHR){
            data = _prepareResponseData(data);

            if(data.status == RESPONSE_STATUS_ERROR){
                this._setFormErrors(this._elements.$regForm.get(0), data.errors);
            }
            else {
                this._user = data._user;
                this.showActivationForm();
                this._elements.$regForm.get(0).reset();
            }
        },
        activate: function(){
            var $form = this._elements.$actForm;

            if(!this.validateForm($form.get(0))) return;

            var data = {
                token:  $form.get(0).elements.namedItem('token').value,
                userId: this._user.id
            };

            var request = $.post(this._urls.activate, data);

            request.then(
                this._activateSuccess.bind(this),
                this._commonRequestFail.bind(this)
            );
        },
        _activateSuccess: function(data, status, jqXHR){
            data = _prepareResponseData(data);

            if(data.status == RESPONSE_STATUS_ERROR){
                this._setFormErrors(this._elements.$actForm.get(0), data.errors);
            }
            else {
                this._user = data.user;
                this.updateControls();
                this.hideModals();
            }
        },
        validateForm: function(form){
            var errors = this.validator.validateForm(form);
            if(errors){
                this._setFormErrors(form, errors)
            }

            return $.isEmptyObject(errors);
        },
        _setFormErrors: function(form, errors){
            for( var field in errors){
                this._cleanFieldStatus(form.elements.namedItem(field));
                this._updateFieldErrors(form.elements.namedItem(field), errors[field]);
            }
        },
        validateField: function(e){
            this._cleanFieldStatus(e.target);
            var errors = this.validator.validateField(e.target);
            this._updateFieldErrors(e.target, errors);

        },
        _cleanFieldStatus: function(element){
            var $field  = $(element);
            var $parent = $field.parent();

            $parent.removeClass('has-error').removeClass('has-success');
            $parent.find('.js-error').remove();
            $parent.find('.js-icon').remove();


        },
        _updateFieldErrors: function(element, errors){
            var $field  = $(element);
            var $parent = $field.parent();

            var html   = null;

            if( !core.utils.isArray(errors) && errors) errors = [errors];

            if( errors ){
                html = '<span class="glyphicon glyphicon-remove form-control-feedback js-icon"></span>';

                errors.map(function(error){
                    html += '<p class="text-danger bg-danger js-error">:error:</p>'.replace(':error:', error);
                });

                $parent.addClass('has-error');
            } else {
                $parent.addClass('has-success');
                html = '<span class="glyphicon glyphicon-ok form-control-feedback js-icon"></span>';
            }

            $parent.append(html);
        },
        _showCommonError: function(message){
            var $error = this._activeForm ? this._activeForm.find('.js-account-common-error') : null;

            if( !$error || !$error.length) return;

            $error.empty().removeClass('hidden').append(message);

        },
        _hideCommonError: function(){
            var $error = this._activeForm ? this._activeForm.find('.js-account-common-error') : null;

            if( !$error || !$error.length) return;

            $error.empty().addClass('hidden');
        },
        _pushMessage: function(type, message){
            var data = {
                type:    type,
                message: message
            };

            core.queues.enqueue('flashMessages', data);
        }

    };


    /**
     *
     * @param  {*} data
     * @return {ResponseData}
     * @private
     */
    function _prepareResponseData(data){
        if( data) {
            try{
                data = JSON.parse(data);
            } catch(e){
                data = null;
            }
        } else {
            data = null;
        }

        return data;
    }


    core.components.Account = Account;


}(document, core, $));