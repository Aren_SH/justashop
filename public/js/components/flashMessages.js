;(function(document, core, $){

    var QUEUE_NAME       = 'flashMessages';
    var MESSAGE_LIFETIME = 6000;
    var TIMER_INTERVAL   = 3000;

    if( core.components == undefined ) core.components = {};

    /**
     * Config object for init function
     * @typedef {{containerSelector: String, timerInterval: Number, messageLifetime: Number }} Config
     */


    /**
     * @constructor
     */
    function FlashMessages(){
        this._containerElement = null;
        this._messageLifetime  = 0;
        this._messages         = [];
        this._timerStarted     = false;
        this._timerId          = 0;
        this._timerInterval    = 0;
    }

    FlashMessages.prototype = {
        constructor: FlashMessages,

        /**
         * @param {Object[]}
         * @private
         */
        _messages: [],

        /**
         * @param {Node}
         * @private
         */
        _containerElement: null,

        /**
         * @param {Boolean}
         * @private
         */
        _timerStarted: false,

        /**
         * @param {Number}
         * @private
         */
        _timerId: null,

        /**
         * @param {Number}
         * @private
         */
        _timerInterval: TIMER_INTERVAL,

        /**
         * @param {Number}
         * @private
         */
        _messageLifetime: MESSAGE_LIFETIME,


        /**
         * @param {Config} params
         */
        init: function(params){
            this._containerElement = $(params.containerSelector).get(0);
            this._timerInterval    = params.timerInterval   || TIMER_INTERVAL;
            this._messageLifetime  = params.messageLifetime || MESSAGE_LIFETIME;

            this._bindEvents();
        },


        /**
         * @private
         */
        _bindEvents: function(){
            core.queues.track(QUEUE_NAME, this._addMessageFromQueue.bind(this));

            $(this._containerElement).on('click', '.js-close', this._dismissMessage.bind(this));
        },


        /**
         * @param  {String} type
         * @param  {String} message
         */
        addMessage: function(type, message){

            var element = this._createMessageElement(type, message);
            var expires = getCurrentTimeStamp() + this._messageLifetime;

            this._messages.push({element:element, expires: expires});

            this._addMessageToContainer(element);

            if(!this._timerStarted){
                this._startTimer();
            }
        },


        /**
         * @param   {Event} e
         * @private
         */
        _addMessageFromQueue: function(e){
            var data    = core.queues.dequeue(QUEUE_NAME);
            var expires = getCurrentTimeStamp() + this._messageLifetime;

            var msgElement = this._createMessageElement(data.type, data.message);
            this._addMessageToContainer(msgElement);


            this._messages.push({element:msgElement, expires: expires});

            if(!this._timerStarted){
                this._startTimer();
            }
        },


        /**
         *
         * @param  {String} type
         * @param  {String} message
         * @return {Node}
         * @private
         */
        _createMessageElement: function(type, message){
            return core.dom.el.p(
                {className: 'alert alert-' + type},
                [
                    core.dom.el.button({className: 'close js-close', type: 'button'}, '×'),
                    message
                ]
            );
        },


        /**
         *
         * @param  {Node} element
         * @private
         */
        _addMessageToContainer: function(element){
            this._containerElement.appendChild(element);
            $(element).css('display', 'none').fadeIn();
        },


        /**
         *
         * @param  {Node} element
         * @private
         */
        _removeMessageFromContainer: function(element){
            var $element = $(element);
            $element.fadeOut( function(){$element.remove()});
        },


        /**
         *
         * @param  {Event} e
         * @private
         */
        _dismissMessage: function(e){
            var element = e.target;
            var parent  = element.parentNode;

            if(parent) this._removeMessageFromContainer(parent);
        },


        /**
         * @private
         */
        _removeMessages: function(){
            var currentTime = getCurrentTimeStamp();

            for( var i = 0; i < this._messages.length; i++){
                var message = this._messages[i];

                if(currentTime > message.expires){
                    this._removeMessageFromContainer(message.element);
                    this._messages[i] = null;
                }
            }

            this._messages = this._messages.filter(function(el){ return el != null} );

            if( !this._messages.length ){
                this._stopTimer();
            }

        },


        /**
         * @private
         */
        _startTimer: function(){
            this._timerId = setInterval(this._removeMessages.bind(this), this._timerInterval);
            this._timerStarted = true;
        },


        /**
         * @private
         */
        _stopTimer: function(){
            if(this._timerId){
                clearInterval(this._timerId);
            }
            this._timerStarted = false;
        }


    };


    /**
     * @return {number}
     */
    function getCurrentTimeStamp(){
        return (new Date()).getTime();
    }


    core.components.FlashMessages = FlashMessages;

}(document, core, $));