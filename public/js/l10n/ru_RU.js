(function(l10n){

    l10n.addMessages({
        common: {
            'currency-sign': 'руб.',
            'add':           'добавить',
            'change':       'изменить',
            'edit':          'редактировать',
            'delete':        'удалить',
            'save':          'сохранить',
            'upload':        'загрузить'
        },
        cart: {
            controls: {
                'checkout': 'Оформить',
                'total':    'Всего',
                'cart':      'Корзина',
                'cart-empty': 'Корзина пуста :('
            },
            checkout: {
                validation: {
                    'name required':  'Поле ФИО должно быть заполнено',
                    'name too long':  'Вы указали слишком длинное имя. Попробуйте сократить его до 128 символов',
                    'name too short': 'Вы указали слишком короткое имя. Введите полностью ваше имя, фамилию и отчество',

                    'email required':       'Укажите email',
                    'invalid email format': 'Указанный вами email имеет некорректный формат',

                    'phone number required':       'Укажите номер телефона',
                    'invalid phone number format': 'Указанный вами телефонный номер имеет некорректный формат. Используйте только цифры',

                    'zip-code required': 'Укажите почтовый индекс',

                    'address required':  'Укажите адрес доставки',
                    'address too short': 'Вы указали слишком короткий адрес. Укажите полностью область, город, улицу, номер дома и номер квартиры'
                }
            }
        },
        imageLoader: {
            image: {
                'title':       'Название',
                'description': 'Описание',
                'is_main'    : 'Главная'

            }
        }
    }, 'ru_RU');

})(core.l10n);