<?php

/** VEEERY useful */
define ("DS", DIRECTORY_SEPARATOR);

/** app directory path */
define("CLI_PATH", realpath(dirname(__FILE__)) . DS);

/** app directory path */
define("APP_PATH", realpath( CLI_PATH . '../app') . DS);


/** app directory path */
define("DOC_ROOT", realpath(APP_PATH . '../public' )  . DS);

/** config directory path */
define("CONF_PATH", APP_PATH . 'config' . DS);

/** config directory path */
define("CLI_CONF_PATH", APP_PATH . 'config' . DS);

use \Phalcon\CLI\Console;
use \Phalcon\DI\FactoryDefault\CLI;
use \Phalcon\Loader;

$loader = new Loader();
$loader->registerNamespaces([
    'Justashop' => APP_PATH . 'Justashop' . DS,
])->register();

$di = new CLI();
\Justashop\Helpers\DI::registerCLIServices($di);

$argParser = new \Justashop\Console\ArgParser('\\Justashop\\Console\\Tasks\\');
$argParser->parse($argv);

$app = new Console();
$app->setDI($di);

$di->setShared('arguments', $argParser);
$di->setShared('console', $app);

$app->handle(['task' => $argParser->getTask(), 'action' => $argParser->getAction()]);

