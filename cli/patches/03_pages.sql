CREATE TABLE IF NOT EXISTS "pages" (
  "id"          SERIAL       NOT NULL,
  "title"       VARCHAR(128) DEFAULT NULL,
  "alias"       VARCHAR(256) DEFAULT NULL,
  "content"     TEXT         DEFAULT NULL,
  "createdAt"   TIMESTAMP    NOT NULL DEFAULT NOW(),
  "status"      SMALLINT     DEFAULT 1,
  "meta_tag_id" INTEGER      DEFAULT NULL,

  PRIMARY KEY ("id")
);


DO $$
    BEGIN
        BEGIN
            CREATE INDEX "IX_publicationShows_alias" on "pages" using hash("alias");
        EXCEPTION
            WHEN duplicate_table THEN RAISE NOTICE 'index IX_pages_alias already exists.';
        END;
    END;
$$;



