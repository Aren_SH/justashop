--CREATE EXTENSION IF NOT EXISTS hstore;

CREATE TABLE IF NOT EXISTS "metaTags" (
  "id"          SERIAL NOT NULL,
  "title"       VARCHAR(128) DEFAULT NULL,
  "description" VARCHAR(512) DEFAULT NULL,
  "keywords"    VARCHAR(512) DEFAULT NULL,
  "robots"      VARCHAR(512) DEFAULT NULL,
  "props"       HSTORE DEFAULT NULL,

  PRIMARY KEY ("id")
);

DO $$
BEGIN
  BEGIN
    ALTER TABLE "products" ADD COLUMN "meta_tag_id" INTEGER DEFAULT NULL;
    EXCEPTION
    WHEN duplicate_column
      THEN RAISE NOTICE 'column "meta_tag_id" already exists in "products" table.';
  END;
END;
$$;

DO $$
BEGIN
  BEGIN
    ALTER TABLE "categories" ADD COLUMN "meta_tag_id" INTEGER DEFAULT NULL;
    EXCEPTION
    WHEN duplicate_column
      THEN RAISE NOTICE 'column "meta_tag_id" already exists in "categories" table.';
  END;
END;
$$;


DROP VIEW IF EXISTS "products_view";
CREATE VIEW "products_view" AS
  SELECT
    "products"."id"               AS "id",
    "products"."sku"              AS "sku",
    "products"."title"            AS "title",
    "products"."price"            AS "price",
    "products"."description"      AS "description",
    "products"."category_id"      AS "category_id",
    "products"."meta_tag_id"      AS "meta_tag_id",
    "products"."createdAt"        AS "createdAt",
    "products"."updatedAt"        AS "updatedAt",
    "products"."status"           AS "status",
    COUNT("order_products"."id")  AS "popularity",

    "product_image"."id"          AS "product_image.id",
    "product_image"."product_id"  AS "product_image.product_id",
    "product_image"."image_id"    AS "product_image.image_id",
    "product_image"."title"       AS "product_image.title",
    "product_image"."description" AS "product_image.description",
    "product_image"."is_main"     AS "product_image.is_main",
    "image"."id"                  AS "product_image.image.id",
    "image"."hash"                AS "product_image.image.hash",
    "image"."file"                AS "product_image.image.file",
    "image"."profile_id"          AS "product_image.image.profile_id",
    "image"."sizes"               AS "product_image.image.sizes",
    "image"."status"              AS "product_image.image.status"
  FROM
    "products"
    LEFT JOIN
    "product_images" AS "product_image"
      ON "product_image"."product_id" = "products"."id" AND "product_image"."is_main" = TRUE
    LEFT JOIN
    "images" AS "image" ON "product_image"."image_id" = "image"."id"
    LEFT JOIN
    "order_products" AS "order_products" ON "order_products"."product_id" = "products"."id"
  GROUP BY "products"."id", "product_image"."id", "image"."id";
