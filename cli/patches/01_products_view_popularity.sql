DROP VIEW "products_view";
CREATE VIEW "products_view" AS SELECT
  "products"."id"                as "id",
  "products"."sku"               as "sku",
  "products"."title"             as "title",
  "products"."price"             as "price",
  "products"."description"       as "description",
  "products"."category_id"       as "category_id",
  "products"."createdAt"         as "createdAt",
  "products"."updatedAt"         as "updatedAt",
  "products"."status"            as "status",
  COUNT("order_products"."id")   as "popularity",

  "product_image"."id"           as "product_image.id",
  "product_image"."product_id"   as "product_image.product_id",
  "product_image"."image_id"     as "product_image.image_id",
  "product_image"."title"        as "product_image.title",
  "product_image"."description"  as "product_image.description",
  "product_image"."is_main"      as "product_image.is_main",
  "image"."id"                   as "product_image.image.id",
  "image"."hash"                 as "product_image.image.hash",
  "image"."file"                 as "product_image.image.file",
  "image"."profile_id"           as "product_image.image.profile_id",
  "image"."sizes"                as "product_image.image.sizes",
  "image"."status"               as "product_image.image.status"
FROM
  "products"

LEFT JOIN "product_images"  as "product_image"  ON "product_image"."product_id" = "products"."id" AND "product_image"."is_main" = true
LEFT JOIN "images"          as "image"          ON "product_image"."image_id" = "image"."id"
LEFT JOIN  "order_products" as "order_products" ON "order_products"."product_id" = "products"."id"

GROUP BY "products"."id", "product_image"."id", "image"."id";

