<?php
$failed = false;
$reqExs = ['pgsql', 'memcache', 'phalcon', 'intl', 'gd'];

fwrite(STDOUT, "Check extensions:" . PHP_EOL);

$exts = get_loaded_extensions();
foreach( $reqExs as $ex){
    if( !in_array($ex, $exts) ){
        fwrite(STDOUT, sprintf("Extension %s not installed" . PHP_EOL, $ex));
    }
}

if( $failed ){
    fwrite(STDOUT, "[FAIL] Install required php extensions" . PHP_EOL);
    return;
} else {
    fwrite(STDOUT, "[OK] Required extensions installed" . PHP_EOL.PHP_EOL);
}


fwrite(STDOUT, "Check postgres" . PHP_EOL);

$command = DIRECTORY_SEPARATOR === '\\' ? 'where' : 'which';
$out = shell_exec($command . ' psql');
if( !trim($out) ){
    fwrite(STDOUT, "[FAIL] Postgres not installed" . PHP_EOL);
    return;
} else {
    fwrite(STDOUT, "[OK] Postgres installed" .  PHP_EOL.PHP_EOL);
}

$out = shell_exec($command . ' nginx');
if( !trim($out) ){
    $out = shell_exec($command . ' httpd');
}

if( !trim($out) ){
    fwrite(STDOUT, "[FAIL] Either Nginx or Apache needs to be installed" . PHP_EOL);
    return;
} else {
    fwrite(STDOUT, "[OK] Web server installed" .  PHP_EOL.PHP_EOL);
}



