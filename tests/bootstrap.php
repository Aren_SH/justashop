<?php

use Phalcon\DI;
use Phalcon\DI\FactoryDefault;

define( "DS", DIRECTORY_SEPARATOR );

/** tests directory path */
define( "TEST_ROOT", realpath( dirname(__FILE__) ) . DS );

/** app directory path */
define( "APP_PATH", realpath( TEST_ROOT . '..' . DS . 'app' ) . DS  );

/** config directory path */
define( "CONF_PATH", APP_PATH . 'config' . DS  );

$loader = new Phalcon\Loader();

$loader->registerNamespaces([
    'Justashop' => APP_PATH . 'justashop' . DS,
]);

$loader->register();
