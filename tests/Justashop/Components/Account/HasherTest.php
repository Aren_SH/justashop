<?php


namespace Justashop\Tests\Components\Account;


use Justashop\Components\Account\Hasher;

class HasherTest extends  \PHPUnit_Framework_TestCase{


    /**
     * Test that hasher use correct algorithm
     */
    public function testPasswordHashingAlgorithms(){

        $str = 'some string to hash';

        $config = [
            'hashAlgo' => 'sha256',
            'hashSalt' => 'someSalt',
        ];

        $shaHasher = new Hasher( $config );

        $config['hashAlgo'] = 'md5';
        $md5Hasher = new Hasher($config);

        $shaHash = $shaHasher->passwordHash( $str );
        $md5hash = $md5Hasher->passwordHash( $str );

        $this->assertEquals(64, strlen( $shaHash ), "sha356 must return 64 chars string");
        $this->assertEquals(32, strlen( $md5hash ), "sha356 must return 32 chars string");

    }


    /**
     * String get same hash each time
     */
    public function testPasswordHashIsPure(){
        $iterations    = 5;
        $hasher        = new Hasher(['hashSalt' => 'some salt']);
        $randomStrings = [];
        $hashes        = [];

        // generate random strings
        for($i = 0; $i < 1000; $i++){
            $str = $this->getRandomString(rand(10, 20));
            $randomStrings[] = $str;
            $hashes[0][]     = $hasher->passwordHash($str);
        }

        // generate more hashes for string
        for($i = 1; $i < $iterations; $i++){
            foreach($randomStrings as $str){
                $hashes[$i][] = $hasher->passwordHash($str);
            }
        }

        // check all hashes for string are equal
        foreach( $randomStrings as $j => &$str){
            for( $i = 1; $i < $iterations; $i++){
                $this->assertEquals( $hashes[$i][$j], $hashes[$i-1][$j], "Hashes for one string must be equal");
            }
        }

    }


    /**
     * Simply check that hasher return different hashes for almost equal strings
     */
    public function testPasswordHashCollisions(){
        $testString    = $this->getRandomString(rand(10, 20));
        $randomStrings = $this->randomizeString($testString);

        $hasher        = new Hasher(['hashSalt' => 'some salt']);
        $hashes        = [];

        // generate random strings
        foreach($randomStrings as $str){
            $hashes[$str] = $hasher->passwordHash($str);
        }

        $this->assertFalse( $this->hasEqualStrings( $hashes ), "Hashes for string must be unique");
    }

    public function testTokenIsUnique(){
        $iterations = 5;
        $testString = $this->getRandomString(rand(10, 20));
        $hasher     = new Hasher(['hashSalt' => 'some salt']);
        $tokens     = [];

        for($i = 0; $i < $iterations; $i++){
            $tokens[] = $hasher->tokenHash($testString);
        }

        $this->assertFalse( $this->hasEqualStrings( $tokens ), "Tokens must be unique");

    }


    /**
     * @param  string[] $strings
     * @return bool
     */
    private function hasEqualStrings($strings){

        foreach( $strings as $index1 => $str1){
            foreach( $strings as $index2 => $str2){
                if( $index1 == $index2) continue;
                if( $str1 == $str2 ) return true;
            }
        }

        return false;
    }

    /**
     * @param  int    $length
     * @return string
     */
    private function getRandomString( $length ){
        if( $length < 1) return '';

        $chars    = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$#!^&*';
        $charsLen = strlen($chars);
        $str = '';
        for($i = 0; $i < $length; $i++ ){
            $str .= $chars[rand(0, $charsLen -1 )];
        }

        return $str;

    }


    /**
     * @param  string   $str
     * @return string[]
     */
    private function randomizeString($str){
        $len = strlen($str);
        if( $len < 2) return [$str];

        $randomStrings = [$str];

        for($i = 1; $i < $len; $i++){
            $randomStrings[] = substr( $str, 0, $i) . $this->getRandomString($len - $i);
        }

        return $randomStrings;
    }

} 